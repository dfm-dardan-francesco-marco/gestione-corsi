export const environment = {
  production: true,
  host: 'http://localhost:4000/api/',
  endpoint: {
  'dashboard': 'dashboard/',
    'teachers': 'teachers/',
    'employees': 'employees/',
    'thirdParties': 'third-party/',
    'companies': 'companies/',
    'courses': 'courses/',
    'courseUsers': 'course-users/',
    'courseTeachers': 'course-teachers/',
    'marksUsers': 'marks-users/',
    'marksTeachers': 'marks-teachers/',
    'marksCourses': 'marks-courses/',
    'types': 'types/',
}
};
