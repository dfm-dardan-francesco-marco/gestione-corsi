// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  host: 'http://localhost:4000/api/',
  endpoint: {
    'dashboard': 'dashboard/',
    'teachers': 'teachers/',
    'employees': 'employees/',
    'thirdParties': 'third-party/',
    'companies': 'companies/',
    'courses': 'courses/',
    'courseUsers': 'course-users/',
    'courseTeachers': 'course-teachers/',
    'marksUsers': 'marks-users/',
    'marksTeachers': 'marks-teachers/',
    'marksCourses': 'marks-courses/',
    'types': 'types/',
  }
};
