import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent} from '../containers/advanced-table/advanced-table.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';
import {SidebarModule} from 'primeng/sidebar';
import {CardModule} from 'primeng/card';
import {PaginatorModule} from 'primeng/paginator';
import {DialogModule} from 'primeng/dialog';
import {ProgressBarModule} from 'primeng/progressbar';

@NgModule({
  declarations: [
    AdvancedTableComponent
  ],
  imports: [
    CommonModule,
    ProgressSpinnerModule,
    ConfirmDialogModule,
    ToastModule,
    FormsModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    TableModule,
    TabViewModule,
    SidebarModule,
    CardModule,
    PaginatorModule,
    DialogModule,
    ProgressBarModule
  ],
  exports: [
    AdvancedTableComponent,
    ProgressSpinnerModule,
    ConfirmDialogModule,
    ToastModule,
    FormsModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    TableModule,
    TabViewModule,
    SidebarModule
  ],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class SharedModule {
}
