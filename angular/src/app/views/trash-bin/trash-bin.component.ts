import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {FormBuilder} from '@angular/forms';
import {CoursesDictionariesDto, CoursesService} from '../../services/courses.service';
import {TeacherDto, TeachersService} from '../../services/teachers.service';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {PageableDto} from '../../interfaces/pageableDto';
import {ActionBtn, DynamicColumns, Search} from '../../containers/advanced-table/advanced-table.component';
import {OrderService} from '../../services/order.service';
import {DatePipe} from '@angular/common';
import {ServiceResponse} from '../../interfaces/serviceResponse';
import * as moment from 'moment';
import {CompaniesDto, CompaniesService} from '../../services/companies.service';

enum SelectedItem {
  COURSES,
  TEACHERS
}

@Component({
  selector: 'app-trash-bin',
  templateUrl: './trash-bin.component.html',
  styleUrls: ['./trash-bin.component.scss'],
  providers: [DatePipe]
})
export class TrashBinComponent implements OnInit {

  item: SelectedItem = SelectedItem.TEACHERS;
  rowId: String;
  visible: boolean = true;
  rows: any[] = null;
  teachers: TeacherDto[] = null;
  loading: boolean = true;
  totalRecords: number;
  resultsPerPage: number = null;
  errors: string[] = [];
  lastLazyLoad: LazyLoadEvent = null;
  request: any = null;
  dictionaries: CoursesDictionariesDto = null;
  companies: CompaniesDto[] = [];
  columns: DynamicColumns;
  search: Search;
  page: number = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private fb: FormBuilder,
    private coursesService: CoursesService,
    private teachersService: TeachersService,
    private orderService: OrderService,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService,
    private companiesService: CompaniesService
  ) {
  }


  actions: ActionBtn[] = [
    {
      id: 'restore',
      title: 'Ripristina',
      icon: 'pi-upload',
      class: ['btn-success', 'text-white']
    },
    {
      id: 'delete',
      title: 'Elimina definitivamente',
      icon: 'pi-trash',
      class: ['btn-danger', 'text-white']
    }
  ];

  ngOnInit(): void {
    this.route.params.subscribe(values => {
      this.route.data.subscribe(data => {
        if (data.title === 'Cestino corsi') {
          this.item = SelectedItem.COURSES;
          this.rowId = 'corsoId';
          this.columns = {
            enabled: true,
            columns: [
              {
                field: 'tipologia',
                header: 'Tipologia',
                visible: true
              },
              {
                field: 'tipo',
                header: 'Tipo',
                visible: true
              },
              {
                field: 'luogo',
                header: 'Luogo',
                visible: true
              },
              {
                field: 'ente',
                header: 'Ente',
                visible: true
              },
              {
                field: 'edizione',
                header: 'Edizione',
                visible: false
              },
              {
                field: 'dataErogazione',
                header: 'Data Erogazione',
                visible: true
              },
              {
                field: 'dataChiusura',
                header: 'Data Chiusura',
                visible: false
              },
              {
                field: 'dataCensimento',
                header: 'Data Censimento',
                visible: false
              },
              {
                field: 'trashed',
                header: 'Data cancellazione',
                visible: true
              }
            ]
          };
        } else if (data.title === 'Cestino docenti') {
          this.item = SelectedItem.TEACHERS;
          this.rowId = 'docenteId';
          this.columns = {
            enabled: true,
            columns: [
              {
                field: 'titolo',
                header: 'Titolo',
                visible: false
              },
              {
                field: 'ragioneSociale',
                header: 'Ragione sociale',
                visible: true
              },
              {
                field: 'partitaIva',
                header: 'Partita IVA',
                visible: true
              },
              {
                field: 'telefono',
                header: 'Telefono',
                visible: true
              },
              {
                field: 'email',
                header: 'Email',
                visible: true
              },
              {
                field: 'referente',
                header: 'Referente',
                visible: true
              },
              {
                field: 'localita',
                header: 'Località',
                visible: false
              },
              {
                field: 'provincia',
                header: 'Provincia',
                visible: false
              },
              {
                field: 'trashed',
                header: 'Data cancellazione',
                visible: true
              }
            ]
          };
        }
        this.search = {
          enabled: true,
          placeholder: 'Cerca nei ' + (this.item === 1 ? 'docenti' : 'corsi') + '...'
        };
        this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
          if (resp.status === 200 && resp.success) {
            this.dictionaries = resp.response.data;
          }
        });
        this.companiesService.list().subscribe((resp: BaseResponseDto<CompaniesDto[]>) => {
          if (resp.status === 200 && resp.success) {
            this.companies = resp.response;
          }
        });
      });
    });
  }

  searchFn(value: string): void {
    this.search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(1);
  }

  actionBtnClicked(pair: any): void {
    const id = pair.row;
    const action = pair.action;

    switch (action) {
      case 'restore':
        if (this.item === 1) {
          const teacher = this.rows.find(currentTeacher => currentTeacher.docenteId === id);
          this.confirmationService.confirm({
            message: 'Sei sicuro di voler recuperare il docente <b>' + (teacher.ragioneSociale) + '</b>?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              teacher.trashed = null;
              this.teachersService.setTrashed(teacher.docenteId, teacher, false).subscribe((resp: BaseResponseDto<boolean | string>) => {
                if (resp.status === 200) {
                  if (resp.success) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Rimozione riuscita',
                      detail: 'Il docente ' + teacher.ragioneSociale + ' è stato ripristinato'
                    });
                    this.lazyLoad();
                  }
                  if (resp.error) {
                    this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il docente');
                  }
                } else {
                  this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il docente');
                }
              }, () => {
                this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il docente');
              });
            }
          });
        } else if (this.item === 0) {
          const course = this.rows.find(currentCourse => currentCourse.corsoId === id);
          this.confirmationService.confirm({
            message: 'Sei sicuro di voler recuperare il corso <b>' + (course.tipo) + ' - ' + (course.tipologia) + '</b>?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              course.trashed = null;
              course.tipo = this.dictionaries.tipi.find(type => type.descrizione === course.tipo).tipoId;
              course.tipologia = this.dictionaries.tipologie.find(typology => typology.descrizione === course.tipologia).tipologiaId;
              course.dataErogazione === 'Non impostata' ? course.dataErogazione = null : course.dataErogazione = moment(this.coursesService.convertDate(course.dataErogazione)).format('YYYY-MM-DD');
              course.dataChiusura === 'Non impostata' ? course.dataChiusura = null : course.dataChiusura = moment(this.coursesService.convertDate(course.dataChiusura)).format('YYYY-MM-DD');
              course.dataCensimento === 'Non impostata' ? course.dataCensimento = null : course.dataCensimento = moment(this.coursesService.convertDate(course.dataCensimento)).format('YYYY-MM-DD');
              // course.societa = this.companies.find(societa => societa.ragioneSociale === course.societa).societaId;
              this.coursesService.setTrashed(course.corsoId, course, false).subscribe((resp: BaseResponseDto<boolean | string>) => {
                if (resp.status === 200) {
                  if (resp.success) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Rimozione riuscita',
                      detail: 'Il corso ' + (this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione) + ' - ' + (this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione) + ' è stato ripristinato'
                    });
                    this.lazyLoad();
                  }
                  if (resp.error) {
                    this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il corso');
                  }
                } else {
                  this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il corso');
                }
              }, () => {
                this.generateError('Errore rimozione', true, 'Non sono riuscito a ripristinare il corso');
              });
            }
          });
        }
        break;
      case 'delete':
        if (this.item === 1) {
          const teacher = this.rows.find(currentTeacher => currentTeacher.docenteId === id);

          this.confirmationService.confirm({
            message: 'Sei sicuro che vuoi eliminare <b> definitivamente </b> il docente <b>' + (teacher.ragioneSociale) + '</b>?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              this.teachersService.deleteById(teacher.docenteId).subscribe((resp: BaseResponseDto<boolean | string>) => {
                if (resp.status === 200) {
                  if (resp.success) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Rimozione riuscita',
                      detail: 'Il docente ' + teacher.ragioneSociale + ' è stato eliminato definitivamente'
                    });
                    this.lazyLoad();
                  }
                  if (resp.error) {
                    if (resp.response === 'DATA_INTEGRITY_ERROR') {
                      this.generateError('Impossibile eliminare', true, 'Il docente che vuoi eliminare sta svolgendo un corso');
                    }
                    if (resp.response === 'DELETE_FAILED') {
                      this.generateError('Impossibile eliminare', true, 'Non sono riuscito a rimuovere il docente');
                    }
                    if (resp.response === 'EMPTY_ID') {
                      this.generateError('Impossibile eliminare', true, 'Nessun docente selezionato');
                    }
                    if (resp.response === 'EMPTY_DTO') {
                      this.generateError('Impossibile eliminare', true, 'Il docente da elimanare non è stato passato al DTO');
                    }
                    if (resp.response === 'UPDATE_FAILED') {
                      this.generateError('Impossibile modificare', true, 'Questo docente non può essere modificato:(');
                    }
                  }
                } else {
                  this.generateError('Errore rimozione', true, 'Non sono riuscito a rimuovere il docente');
                }
              }, () => {
                this.generateError('Errore rimozione', true, 'Non sono riuscito a rimuovere il docente');
              });
            }
          });
          break;
        } else if (this.item === 0) {
          const course = this.rows.find(corso => corso.corsoId === id);
          this.confirmationService.confirm({
            message: 'Sei sicuro che vuoi eliminare <b> definitivamente </b> il <b>' + (course.tipo) + ' - ' + (course.tipologia) + '</b>?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              this.lazyLoad();

              this.coursesService.deleteById(course.corsoId).subscribe((resp: BaseResponseDto<boolean | string>) => {
                if (resp.status === 200) {
                  if (resp.success) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Rimozione riuscita',
                      detail: 'Il ' + course.tipo + ' - ' + course.tipologia + ' è stato rimosso definitivamente'
                    });
                    this.lazyLoad();
                  }
                  if (resp.error) {
                    if (resp.response === 'DATA_INTEGRITY_ERROR') {
                      this.generateError('Impossibile eliminare', true, 'Per eliminare un corso, devi prima eliminare il docente e gli studenti');
                    }
                    if (resp.response === 'DELETE_FAILED') {
                      this.generateError('Impossibile eliminare', true, 'Non sono riuscito a rimuovere il corso');
                    }
                    if (resp.response === 'EMPTY_ID') {
                      this.generateError('Impossibile eliminare', true, 'Nessun corso selezionato');
                    }
                    if (resp.response === 'EMPTY_DTO') {
                      this.generateError('Impossibile eliminare', true, 'Il corso da eliminare non è stato passato al DTO');
                    }
                    if (resp.response === 'UPDATE_FAILED') {
                      this.generateError('Impossibile modificare', true, 'Questo corso non può essere modificato:(');
                    }
                  }
                }
              }, () => {
                this.generateError('Errore rimozione', true, 'Non sono riuscito a rimuovere il corso');
              });
            }
          });
          break;
        }
    }
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.lastLazyLoad;
    }

    this.loading = true;
    this.lastLazyLoad = event;

    if (this.request) {
      this.request.unsubscribe();
    }

    const multiSortMeta = [];
    if (event.multiSortMeta !== null && event.multiSortMeta !== undefined) {
      event.multiSortMeta.forEach(item => {
        const temp = {...item};
        temp.field = this.mapSortField(temp.field);
        multiSortMeta.push(temp);
      });
    }

    let fn;
    if (this.search.defaultValue !== undefined && this.search.defaultValue !== null) {
      if (this.item === 0) {
        fn = this.coursesService.searchByField(
          this.search.defaultValue,
          this.page,
          this.resultsPerPage,
          this.orderService.parse(multiSortMeta)
        );
        this.search.defaultValue = event.globalFilter;
      } else {
        fn = this.teachersService.searchByField(
          this.search.defaultValue,
          this.page,
          this.resultsPerPage,
          this.orderService.parse(multiSortMeta),
          true
        );
      }
    } else {
      this.search.defaultValue = null;
      if (this.item === 0) {
        fn = this.coursesService.list(
          this.page,
          this.resultsPerPage,
          this.orderService.parse(multiSortMeta),
          true
        );
      } else {
        fn = this.teachersService.list(
          this.page,
          this.resultsPerPage,
          this.orderService.parse(multiSortMeta),
          true
        );
      }
    }

    if (fn) {
      this.request = fn.subscribe((resp: BaseResponseDto<PageableDto<any>>) => {
        this.loading = false;

        if (resp.status === 200 && resp.success) {
          resp.response.data.forEach(item => {
            item.trashed = item.trashed !== null ? this.datePipe.transform(item.trashed, 'dd/MM/yyyy') : 'Non caricata';
            if (this.item === 0) {
              item.dataErogazione = item.dataErogazione !== null ? this.datePipe.transform(item.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
              item.dataCensimento = item.dataCensimento !== null ? this.datePipe.transform(item.dataCensimento, 'dd/MM/yyyy') : 'Non impostata';
              item.dataChiusura = item.dataChiusura !== null ? this.datePipe.transform(item.dataChiusura, 'dd/MM/yyyy') : 'Non impostata';
              item.tipo = this.dictionaries.tipi.find(type => type.tipoId === item.tipo).descrizione;
              item.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === item.tipologia).descrizione;
            }
          });

          this.rows = resp.response.data;
          this.totalRecords = resp.response.totalCount;
          this.resultsPerPage = resp.response.resultsPerPage;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        } else {
          this.generateError(null, true);
        }
      }, () => this.generateError(null, true));
    }
  }

  private mapSortField(field: string): string {
    switch (field) {
      case 'tipo':
        return 'typesDao.descrizione';
      case 'tipologia':
        return 'typologiesDao.descrizione';
      default:
        return field;
    }
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }

  rowClicked(row: string) {
    if (this.item === 0) {
      this.router.navigate(['courses', row]);
    } else {
      this.router.navigate(['teachers', row]);
    }

  }
}
