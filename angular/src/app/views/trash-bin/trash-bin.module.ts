import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../modules/shared.module';
import {TrashBinRoutingModule} from './trash-bin-routing.module';
import {TrashBinComponent} from './trash-bin.component';

@NgModule({
  imports: [
    FormsModule,
    TrashBinRoutingModule,
    CommonModule,
    SharedModule
  ],
  declarations: [
    TrashBinComponent
  ]
})
export class TrashBinModule {
}
