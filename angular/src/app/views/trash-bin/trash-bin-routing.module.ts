import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {TrashBinComponent} from './trash-bin.component';

const routes: Routes = [
  {
    path: 'teachers',
    component: TrashBinComponent,
    data: {
      title: 'Cestino docenti'
    }
  },
  {
    path: 'courses',
    component: TrashBinComponent,
    data: {
      title: 'Cestino corsi'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrashBinRoutingModule {
}
