import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

  public readonly moment = moment;
  public readonly currentDate: moment.Moment = moment();

  @Input()
  public id: number;

  @Input()
  public tipologia: string;

  @Input()
  public tipo: string;

  @Input()
  public dataErogazione: number = null;

  @Input()
  public dataChiusura: number = null;

  public progress: number = 0;

  constructor() {
  }

  ngOnInit(): void {
    if (this.dataErogazione && this.dataChiusura
      && this.currentDate > moment(this.dataErogazione)
      && this.currentDate < moment(this.dataChiusura)) {

      const dates = {
        erogazione: moment(this.dataErogazione),
        chiusura: moment(this.dataChiusura)
      };
      const days = {
        total: Math.abs(dates.chiusura.diff(dates.erogazione, 'days')),
        current: Math.abs(dates.erogazione.diff(moment(), 'days'))
      };

      this.progress = (100 * days.current) / days.total;
    }
  }

}
