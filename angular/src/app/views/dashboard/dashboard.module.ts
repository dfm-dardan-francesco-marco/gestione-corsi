import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {CourseListComponent} from './course-list/course-list.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../modules/shared.module';
import {ProgressBarModule} from 'primeng/progressbar';

@NgModule({
  imports: [
    FormsModule,
    DashboardRoutingModule,
    CommonModule,
    SharedModule,
    ProgressBarModule
  ],
  declarations: [DashboardComponent, CourseListComponent]
})
export class DashboardModule {
}
