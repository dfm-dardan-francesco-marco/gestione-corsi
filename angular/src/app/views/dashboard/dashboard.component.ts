import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../services/dashboard.service';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {MessageService} from 'primeng/api';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../services/courses.service';
import {ServiceResponse} from '../../interfaces/serviceResponse';

interface Corsi {
  incoming: CoursesDto[];
  ongoing: CoursesDto[];
  finished: CoursesDto[];
}

interface DashboardDto {
  nteachers: number;
  nemployees: number;
  ncourses: number;
  nmarks: number;
}

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  nTeachers: number = null;
  nEmployees: number = null;
  nCourses: number = null;
  nMarks: number = null;
  errors: string[] = [];
  coursesDictionaries: CoursesDictionariesDto = null;

  courses: Corsi = {
    incoming: null,
    ongoing: null,
    finished: null
  };

  constructor(
    private dashboardService: DashboardService,
    private coursesService: CoursesService,
    private messageService: MessageService
  ) {
  }

  translateTypologyAndType(course: CoursesDto): void {
    course.tipologia = this.coursesDictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
    course.tipo = this.coursesDictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
  }

  ngOnInit(): void {
    this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.coursesDictionaries = resp.response.data;

        this.coursesService.getIncoming().subscribe((respI: BaseResponseDto<CoursesDto[]>) => {
          if (respI.status === 200 && respI.success) {
            respI.response.forEach(incoming => {
              this.translateTypologyAndType(incoming);
            });
            this.courses.incoming = respI.response;
          } else {
            this.generateError('Corsi in avvio non caricati', true);
          }
        });

        this.coursesService.getOngoing().subscribe((respO: BaseResponseDto<CoursesDto[]>) => {
          if (respO.status === 200 && respO.success) {
            respO.response.forEach(ongoing => {
              this.translateTypologyAndType(ongoing);
            });
            this.courses.ongoing = respO.response;
          } else {
            this.generateError('Corsi in svolgimento non caricati', true);
          }
        });

        this.coursesService.getFinished().subscribe((respF: BaseResponseDto<CoursesDto[]>) => {
          if (respF.status === 200 && respF.success) {
            respF.response.forEach(finished => {
              this.translateTypologyAndType(finished);
            });
            this.courses.finished = respF.response;
          } else {
            this.generateError('Corsi conclusi non caricati', true);
          }
        });
      }
    });

    this.dashboardService.counters().subscribe((resp: BaseResponseDto<ServiceResponse<DashboardDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.nTeachers = resp.response.data.nteachers;
        this.nEmployees = resp.response.data.nemployees;
        this.nCourses = resp.response.data.ncourses;
        this.nMarks = resp.response.data.nmarks;
      } else {
        this.generateError('Contatori non caricati', true);
      }
    });
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }
}
