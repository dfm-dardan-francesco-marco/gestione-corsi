import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {EmployeesRoutingModule} from './employees-routing.module';
import {EmployeesComponent} from './employees.component';
import {SharedModule} from '../../modules/shared.module';
import {DetailsEmployeeComponent} from './details-employee/details-employee.component';

@NgModule({
  imports: [
    FormsModule,
    EmployeesRoutingModule,
    CommonModule,
    SharedModule
  ],
  declarations: [
    EmployeesComponent,
    DetailsEmployeeComponent
  ]
})
export class EmployeesModule {
}
