import {Component, OnInit} from '@angular/core';
import {EmployeesDto, EmployeesService} from '../../services/employees.service';
import {ActionBtn, DynamicColumns, Search} from '../../containers/advanced-table/advanced-table.component';
import {Router} from '@angular/router';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {LazyLoadEvent, MessageService} from 'primeng/api';
import {PageableDto} from '../../interfaces/pageableDto';
import {DatePipe} from '@angular/common';
import {OrderService} from '../../services/order.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
  providers: [DatePipe]
})
export class EmployeesComponent implements OnInit {

  loading: boolean = true;
  employees: EmployeesDto[] = null;
  errors: string[] = [];
  request: any = null;
  totalRecords: number;
  resultsPerPage: number = null;
  lastLazyLoad: LazyLoadEvent = null;
  page: number = 1;

  columns: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'matricola',
        header: 'Matricola',
        visible: true
      },
      {
        field: 'cognome',
        header: 'Cognome',
        visible: true
      },
      {
        field: 'nome',
        header: 'Nome',
        visible: true
      },
      {
        field: 'tipoDipendente',
        header: 'Tipo dipendente',
        visible: true
      },
      {
        field: 'qualifica',
        header: 'Qualifica',
        visible: false
      },
      {
        field: 'responsabileRisorsa',
        header: 'Responsabile risorsa',
        visible: true
      },
      {
        field: 'sesso',
        header: 'Sesso',
        visible: false
      },
      {
        field: 'dataNascita',
        header: 'Data di nascita',
        visible: false
      },
      {
        field: 'luogoNascita',
        header: 'Luogo di nascita',
        visible: false
      },
      {
        field: 'statoCivile',
        header: 'Stato civile',
        visible: false
      },
      {
        field: 'titoloStudio',
        header: 'Titolo di studio',
        visible: false
      },
      {
        field: 'conseguitoPresso',
        header: 'Titolo conseguito presso',
        visible: false
      },
      {
        field: 'annoConseguimento',
        header: 'Titolo conseguito il',
        visible: false
      },
      {
        field: 'livello',
        header: 'Livello',
        visible: false
      },
      {
        field: 'responsabileArea',
        header: 'Responsabile di area',
        visible: false
      },
      {
        field: 'dataAssunzione',
        header: 'Data di assunzione',
        visible: false
      },
      {
        field: 'dataFineRapporto',
        header: 'Data di fine rapporto',
        visible: false
      },
      {
        field: 'dataScadenzaContratto',
        header: 'Data di scadenza contratto',
        visible: false
      },
    ]
  };

  actions: ActionBtn[] = [
    {
      id: 'open',
      title: 'Visualizza',
      icon: 'pi-eye'
    }
  ];

  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei dipendenti...'
  };

  constructor(
    private employeesService: EmployeesService,
    private router: Router,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private orderService: OrderService
  ) {
  }

  ngOnInit(): void {
  }

  searchFn(value: string): void {
    this.search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(1);
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.lastLazyLoad;
    }

    this.loading = true;
    this.lastLazyLoad = event;

    if (this.request) {
      this.request.unsubscribe();
    }

    let fn;
    if (this.search.defaultValue !== undefined && this.search.defaultValue !== null) {
      fn = this.employeesService.searchByMatricola(
        this.search.defaultValue,
        this.page,
        this.resultsPerPage,
        this.orderService.parse(event.multiSortMeta)
      );
      this.search.defaultValue = event.globalFilter;
    } else {
      fn = this.employeesService.list(
        this.page,
        this.resultsPerPage,
        this.orderService.parse(event.multiSortMeta)
      );
      this.search.defaultValue = null;
    }

    if (fn) {
      this.request = fn.subscribe((resp: BaseResponseDto<PageableDto<EmployeesDto[]>>) => {
        this.loading = false;

        if (resp.status === 200 && resp.success) {
          resp.response.data.forEach(employee => {
            employee.dataAssunzione = employee.dataAssunzione !== null ? this.datePipe.transform(employee.dataAssunzione, 'dd/MM/yyyy') : 'Non impostata';
            employee.dataFineRapporto = employee.dataFineRapporto !== null ? this.datePipe.transform(employee.dataFineRapporto, 'dd/MM/yyyy') : 'Non impostata';
            employee.dataNascita = employee.dataNascita !== null ? this.datePipe.transform(employee.dataNascita, 'dd/MM/yyyy') : 'Non impostata';
            employee.dataScadenzaContratto = employee.dataScadenzaContratto !== null ? this.datePipe.transform(employee.dataScadenzaContratto, 'dd/MM/yyyy') : 'Non impostata';
          });
          this.employees = resp.response.data;
          this.totalRecords = resp.response.totalCount;
          this.resultsPerPage = resp.response.resultsPerPage;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        }
        if (resp.error) {
          this.errors.push('Si &egrave; verificato un errore');
        }
      });
    }
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  rowClicked(row: string): void {
    this.router.navigate(['employees', row]);
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  actionBtnClicked(pair: any): void {
    const row = pair.row;
    const action = pair.action;

    switch (action) {
      case 'open':
        this.router.navigate(['employees', row]);
        break;
      case 'edit':
        this.router.navigate(['employees', 'edit', row]);
        break;
    }
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }

}
