import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EmployeesDto, EmployeesService} from '../../../services/employees.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseResponseDto} from '../../../interfaces/baseResponseDto';
import {CompaniesDto, CompaniesService} from '../../../services/companies.service';
import {LazyLoadEvent, MessageService} from 'primeng/api';
import {SearchListType, SearchService, SearchType} from '../../../services/search.service';
import {DatePipe} from '@angular/common';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../../services/courses.service';
import {Search} from '../../../containers/advanced-table/advanced-table.component';
import {ServiceResponse} from '../../../interfaces/serviceResponse';
import {PageableDto} from '../../../interfaces/pageableDto';
import {OrderService} from '../../../services/order.service';

@Component({
  selector: 'app-details-employee',
  templateUrl: './details-employee.component.html',
  styleUrls: ['./details-employee.component.scss'],
  providers: [DatePipe]
})
export class DetailsEmployeeComponent implements OnInit {

  form: FormGroup;
  request: any = null;
  id: number = null;
  employee: EmployeesDto = null;

  totalRecordsTenuti: number;
  totalRecordsSeguiti: number;
  resultsPerPageTenuti: number = null;
  resultsPerPageSeguiti: number = null;
  lastLazyLoad: LazyLoadEvent = null;
  loadingCoursesTenuti: boolean = true;
  loadingCoursesSeguiti: boolean = true;
  dictionaries: CoursesDictionariesDto = null;
  listTenuti: CoursesDto[] = null;
  listSeguiti: CoursesDto[] = null;
  requestCoursesTenuti: any = null;
  requestCoursesSeguiti: any = null;
  page = {
    'tenuti': 1,
    'seguiti': 1
  };

  columns: any[] = [
    {
      field: 'tipologia',
      header: 'Tipologia'
    },
    {
      field: 'tipo',
      header: 'Tipo'
    },
    {
      field: 'descrizione',
      header: 'Descrizione'
    },
    {
      field: 'luogo',
      header: 'Luogo'
    },
    {
      field: 'ente',
      header: 'Ente'
    },
    {
      field: 'dataErogazione',
      header: 'Data Erogazione'
    },
  ];

  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei corsi...'
  };

  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeesService,
    private route: ActivatedRoute,
    private companiesService: CompaniesService,
    private messageService: MessageService,
    private searchService: SearchService,
    private coursesService: CoursesService,
    private datePipe: DatePipe,
    private router: Router,
    private orderService: OrderService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(values => {
      this.form = this.fb.group({
        matricola: [{value: {value: '', disabled: true}, disabled: true}],
        cognome: [{value: '', disabled: true}],
        nome: [{value: '', disabled: true}],
        dataNascita: [{value: '', disabled: true}],
        luogoNascita: [{value: '', disabled: true}],
        statoCivile: [{value: '', disabled: true}],
        titoloStudio: [{value: '', disabled: true}],
        conseguitoPresso: [{value: '', disabled: true}],
        annoConseguimento: [{value: '', disabled: true}],
        tipoDipendente: [{value: '', disabled: true}],
        qualifica: [{value: '', disabled: true}],
        livello: [{value: '', disabled: true}],
        dataAssunzione: [{value: '', disabled: true}],
        responsabileRisorsa: [{value: '', disabled: true}],
        responsabileArea: [{value: '', disabled: true}],
        dataFineRapporto: [{value: '', disabled: true}],
        dataScadenzaContratto: [{value: '', disabled: true}],
        societa: [{value: '', disabled: true}]
      });

      this.id = values.id_r;
      this.lazyLoad('tenuti');
      this.lazyLoad('seguiti');

      this.employeesService.getById(this.id).subscribe((resp: BaseResponseDto<EmployeesDto>) => {
        if (resp.status === 200 && resp.success && resp.response !== null) {
          this.employee = resp.response;

          this.form.patchValue({
            matricola: resp.response.matricola,
            cognome: resp.response.cognome,
            nome: resp.response.nome,
            sesso: resp.response.sesso,
            dataNascita: this.datePipe.transform(resp.response.dataNascita, 'dd/MM/yyyy'),
            luogoNascita: resp.response.luogoNascita,
            statoCivile: resp.response.statoCivile,
            titoloStudio: resp.response.titoloStudio,
            conseguitoPresso: resp.response.conseguitoPresso,
            annoConseguimento: resp.response.annoConseguimento,
            tipoDipendente: resp.response.tipoDipendente,
            qualifica: resp.response.qualifica,
            livello: resp.response.livello,
            dataAssunzione: this.datePipe.transform(resp.response.dataAssunzione, 'dd/MM/yyyy'),
            responsabileRisorsa: resp.response.responsabileRisorsa,
            responsabileArea: resp.response.responsabileArea,
            dataFineRapporto: this.datePipe.transform(resp.response.dataFineRapporto, 'dd/MM/yyyy'),
            dataScadenzaContratto: this.datePipe.transform(resp.response.dataScadenzaContratto, 'dd/MM/yyyy'),
            societa: resp.response.societa
          });

          this.searchService.push(SearchListType.QUICK_ACCESS, {
            id: this.id,
            type: SearchType.EMPLOYEE,
            firstRow: resp.response.cognome + ' ' + resp.response.nome
          });

          this.companiesService.getById(resp.response.societa).subscribe((res: BaseResponseDto<CompaniesDto>) => {
            if (res.status === 200 && res.success) {
              this.form.patchValue({
                societa: res.response.ragioneSociale
              });
            } else {
              this.generateError(null, true, 'Impossibile ottenere la società');
            }
          });
        } else {
          this.employee = null;
          this.generateError(null, true, 'Impossibile caricare il dipendente');
        }
      });
    });

  }

  lazyLoad(table: string, event: LazyLoadEvent = null): void {
    if (this.dictionaries === null) {
      this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
        if (resp.status === 200 && resp.success) {
          this.dictionaries = resp.response.data;
        }
      });
    }

    if (event === null) {
      event = this.lastLazyLoad;
    }

    if (event !== null) {
      let request = null;
      let list = null;
      let resultsPerPage = null;
      if (table === 'tenuti') {
        request = this.requestCoursesTenuti;
        list = this.listTenuti;
        resultsPerPage = this.resultsPerPageTenuti;
        this.loadingCoursesTenuti = true;
      } else if (table === 'seguiti') {
        request = this.requestCoursesSeguiti;
        list = this.listSeguiti;
        resultsPerPage = this.resultsPerPageSeguiti;
        this.loadingCoursesSeguiti = true;
      }
      if (request) {
        request.unsubscribe();
      }
      this.lastLazyLoad = event;

      request = this.employeesService.listCourses(
        table,
        this.id,
        this.page[table],
        resultsPerPage,
        this.orderService.parse(event.multiSortMeta)
      ).subscribe((resp: BaseResponseDto<ServiceResponse<PageableDto<CoursesDto[]>>>) => {
        if (table === 'tenuti') {
          this.loadingCoursesTenuti = false;
        } else if (table === 'seguiti') {
          this.loadingCoursesSeguiti = false;
        }

        if (resp.status === 200 && resp.success) {
          if (this.dictionaries !== null) {
            resp.response.data.data.forEach(course => {
              course.dataErogazione = course.dataErogazione !== null ? this.datePipe.transform(course.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
              course.tipo = this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
              course.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
            });
          }

          if (table === 'tenuti') {
            this.totalRecordsTenuti = resp.response.data.totalCount;
            this.resultsPerPageTenuti = resp.response.data.resultsPerPage;
            this.listTenuti = resp.response.data.data;
          } else if (table === 'seguiti') {
            this.totalRecordsSeguiti = resp.response.data.totalCount;
            this.resultsPerPageSeguiti = resp.response.data.resultsPerPage;
            this.listSeguiti = resp.response.data.data;
          }

          list = resp.response.data.data;
          event.rows = resp.response.data.resultsPerPage;
          event.first = 0;
        } else {

        }
      });
    }
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si è verificato un errore',
        detail
      });
    } else {

    }
  }

  rowClicked(row: string): void {
    this.router.navigate(['courses', row]);
  }

  changeResultsPerPage(table: string, number: number): void {
    if (table === 'tenuti') {
      this.resultsPerPageTenuti = number;
      this.pageChanged('tenuti', 1);
    } else if (table === 'seguiti') {
      this.resultsPerPageSeguiti = number;
      this.pageChanged('seguiti', 1);
    }
    this.lazyLoad(table);
  }

  pageChanged(table: string, page: number) {
    this.page[table] = page;
    this.lazyLoad(table);
  }
}
