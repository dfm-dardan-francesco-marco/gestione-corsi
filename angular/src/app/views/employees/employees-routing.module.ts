import {EmployeesComponent} from './employees.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailsEmployeeComponent} from './details-employee/details-employee.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeesComponent,
    data: {
      title: 'Lista dipendenti'
    }
  },
  {
    path: ':id_r',
    component: DetailsEmployeeComponent,
    data: {
      title: 'Visualizza dipendenti',
      read: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule {
}
