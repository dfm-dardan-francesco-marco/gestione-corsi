import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TeachersComponent} from './teachers.component';
import {NewEditTeacherComponent} from './new-edit-teacher/new-edit-teacher.component';

const routes: Routes = [
  {
    path: '',
    component: TeachersComponent,
    data: {
      title: 'Lista docenti'
    }
  },
  {
    path: 'insert',
    component: NewEditTeacherComponent,
    data: {
      title: 'Inserimento docente',
      create: true
    }
  },
  {
    path: ':id_r',
    component: NewEditTeacherComponent,
    data: {
      title: 'Visualizza docente',
      read: true
    }
  },
  {
    path: 'edit/:id_u',
    component: NewEditTeacherComponent,
    data: {
      title: 'Modifica docente',
      edit: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeachersRoutingModule {
}
