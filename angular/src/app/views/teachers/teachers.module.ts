import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {TeachersComponent} from './teachers.component';
import {CommonModule} from '@angular/common';
import {TeachersRoutingModule} from './teachers-routing.module';
import {SharedModule} from '../../modules/shared.module';
import {NewEditTeacherComponent} from './new-edit-teacher/new-edit-teacher.component';
import {TooltipModule} from 'primeng/tooltip';
import {KeyFilterModule} from 'primeng/keyfilter';
import {DialogModule} from 'primeng/dialog';
import {MessageModule} from 'primeng/message';

@NgModule({
  imports: [
    FormsModule,
    TeachersRoutingModule,
    CommonModule,
    SharedModule,
    TooltipModule,
    KeyFilterModule,
    DialogModule,
    MessageModule
  ],
  declarations: [
    TeachersComponent,
    NewEditTeacherComponent
  ]
})
export class TeachersModule {
}
