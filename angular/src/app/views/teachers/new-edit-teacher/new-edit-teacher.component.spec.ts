import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEditTeacherComponent} from './new-edit-teacher.component';

describe('NeweditteacherComponent', () => {
  let component: NewEditTeacherComponent;
  let fixture: ComponentFixture<NewEditTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEditTeacherComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
