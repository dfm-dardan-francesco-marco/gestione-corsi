import {Component, ElementRef, OnInit, ViewChild,} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BaseResponseDto} from '../../../interfaces/baseResponseDto';
import {TeacherDto, TeachersService} from '../../../services/teachers.service';
import {LazyLoadEvent, MessageService} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {CountryDto} from '../../../interfaces/third-parties/CountryDto';
import {ThirdPartiesService} from '../../../services/third-parties.service';
import {SearchListType, SearchService, SearchType} from '../../../services/search.service';
import * as moment from 'moment';
import {ServiceResponse} from '../../../interfaces/serviceResponse';
import {PageableDto} from '../../../interfaces/pageableDto';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../../services/courses.service';
import {Search} from '../../../containers/advanced-table/advanced-table.component';
import {DatePipe} from '@angular/common';

enum Operation {
  CREATE,
  READ,
  UPDATE
}

@Component({
  selector: 'app-new-edit-teacher',
  templateUrl: './new-edit-teacher.component.html',
  styleUrls: ['./new-edit-teacher.component.scss'],
  providers: [DatePipe]
})
export class NewEditTeacherComponent implements OnInit {

  readonly Operation = Operation;
  readonly allTitles: string[] = ['Dr', 'Dr.ssa', 'Prof', 'Prof.ssa', 'Ing'];
  allCountries: CountryDto[] = [];

  form: FormGroup;
  request: any = null;
  requestCourses: any = null;
  loading: boolean = false;
  id: number = null;
  operation: Operation = Operation.CREATE;
  teacher: TeacherDto = null;
  list: CoursesDto[] = null;
  errors: string[] = [];
  totalRecords: number;
  page: number = 1;
  resultsPerPage: number = null;
  lastLazyLoad: LazyLoadEvent = null;
  loadingCourses: boolean = true;
  visible: boolean = true;
  submitted = false;
  displayModal: boolean = false;
  deleteDef: boolean = false;
  restoreTeacher: boolean = false;
  dictionaries: CoursesDictionariesDto = null;
  deleted: any;

  title: string;
  titles: string[];

  country: CountryDto;
  countries: CountryDto[];

  columns: any[] = [
    {
      field: 'tipologia',
      header: 'Tipologia'
    },
    {
      field: 'tipo',
      header: 'Tipo'
    },
    {
      field: 'descrizione',
      header: 'Descrizione'
    },
    {
      field: 'luogo',
      header: 'Luogo'
    },
    {
      field: 'ente',
      header: 'Ente'
    },
    {
      field: 'dataErogazione',
      header: 'Data Erogazione'
    },
  ];

  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei corsi...'
  };

  blockSpace: RegExp = /[^\s]/;

  @ViewChild('ragioneSociale') ragioneSociale: ElementRef;

  constructor(
    private fb: FormBuilder,
    private teachersService: TeachersService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private thirdPartiesService: ThirdPartiesService,
    private searchService: SearchService,
    private router: Router,
    private coursesService: CoursesService,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(values => {
      this.form = this.fb.group({
        titolo: [null, [
          Validators.maxLength(100),
        ]],
        ragioneSociale: [null, [
          Validators.required,
          Validators.maxLength(100),
        ]],
        indirizzo: [null, [
          Validators.required,
          Validators.maxLength(100),
        ]],
        localita: [null, [
          Validators.required,
          Validators.maxLength(100),
        ]],
        provincia: [null, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(2),
        ]],
        nazione: [null, [
          Validators.required,
          Validators.maxLength(100),
        ]],
        telefono: [null, [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(100),
          Validators.pattern('[0-9]+$')
        ]],
        fax: [null, [
          Validators.maxLength(100),
          Validators.pattern('[0-9]+$')
        ]],
        email: [null, [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(100),
          Validators.email
        ]],
        noteGeneriche: [null, [
          Validators.maxLength(200)
        ]],
        partitaIva: [null, [
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(11),
          Validators.pattern('^[0-9]{11}$')
        ]],
        referente: [null, [
          Validators.required,
          Validators.maxLength(100),
        ]],
      });

      this.route.data.subscribe(data => {
        if (data.read) {
          this.operation = Operation.READ;
          this.id = values.id_r;
        } else if (data.edit) {
          this.operation = Operation.UPDATE;
          this.id = values.id_u;
        } else if (data.create) {
          this.operation = Operation.CREATE;
        }

        if (this.operation === Operation.CREATE) {
          this.thirdPartiesService.countries().subscribe((respCountries: BaseResponseDto<CountryDto[]>) => {
            if (respCountries.status === 200 && respCountries.success) {
              this.allCountries = respCountries.response;

            }
          });
        }

        if (this.operation === Operation.READ || this.operation === Operation.UPDATE) {
          this.lazyLoad();

          this.thirdPartiesService.countries().subscribe((respCountries: BaseResponseDto<CountryDto[]>) => {
            if (respCountries.status === 200 && respCountries.success) {
              this.allCountries = respCountries.response;

              this.teachersService.getById(this.id).subscribe((resp: BaseResponseDto<TeacherDto>) => {
                if (resp.status === 200 && resp.success && resp.response !== null) {
                  this.teacher = resp.response;
                  this.deleted = this.teacher.trashed;

                  this.form.patchValue({
                    titolo: resp.response.titolo,
                    ragioneSociale: resp.response.ragioneSociale,
                    indirizzo: resp.response.indirizzo,
                    localita: resp.response.localita,
                    provincia: resp.response.provincia,
                    telefono: resp.response.telefono,
                    nazione: resp.response.nazione,
                    fax: resp.response.fax,
                    email: resp.response.email,
                    noteGeneriche: resp.response.noteGeneriche,
                    partitaIva: resp.response.partitaIva,
                    referente: resp.response.referente
                  });
                  this.country = this.allCountries.find(c => c.name === resp.response.nazione);

                  this.searchService.push(SearchListType.QUICK_ACCESS, {
                    id: this.id,
                    type: SearchType.TEACHER,
                    firstRow: resp.response.ragioneSociale
                  });
                } else {
                  this.teacher = null;
                  this.generateError('Impossibile caricare il docente', 'Il docente scelto non esiste');
                }
              });
            } else {
              this.missingCountries();
            }
          }, () => this.missingCountries());
        }
        if (this.operation === Operation.UPDATE) {
          this.teachersService.getById(this.id).subscribe((resp: BaseResponseDto<TeacherDto>) => {
            if (resp.status === 200 && resp.success) {
              this.teacher = resp.response;

              this.form.patchValue({
                titolo: resp.response.titolo,
                ragioneSociale: resp.response.ragioneSociale,
                indirizzo: resp.response.indirizzo,
                localita: resp.response.localita,
                provincia: resp.response.provincia,
                nazione: {name: resp.response.nazione},
                telefono: resp.response.telefono,
                fax: resp.response.fax,
                email: resp.response.email,
                noteGeneriche: resp.response.noteGeneriche,
                partitaIva: resp.response.partitaIva,
                referente: resp.response.referente
              });
            }
          });
        }
      });
    });
  }

  generateError(text: string = null, detail: string = null) {
    this.messageService.add({
      severity: 'error',
      summary: text || 'Si &egrave; verificato un errore',
      detail
    });
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (this.operation === Operation.READ) {
      if (this.dictionaries === null) {
        this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
          if (resp.status === 200 && resp.success) {
            this.dictionaries = resp.response.data;
          }
        });
      }

      if (event === null) {
        event = this.lastLazyLoad;
      }

      if (event !== null) {
        this.loadingCourses = true;
        this.lastLazyLoad = event;

        if (this.requestCourses) {
          this.requestCourses.unsubscribe();
        }

        this.requestCourses = this.teachersService.listCourses(
          this.id,
          this.page,
          this.resultsPerPage,
          event.sortField,
          event.sortOrder
        ).subscribe((resp: BaseResponseDto<ServiceResponse<PageableDto<CoursesDto[]>>>) => {
          this.loadingCourses = false;

          if (resp.status === 200 && resp.success) {
            if (this.dictionaries !== null) {
              resp.response.data.data.forEach(course => {
                course.dataErogazione = course.dataErogazione !== null ? this.datePipe.transform(course.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
                course.tipo = this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
                course.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
              });
            }

            this.list = resp.response.data.data;
            this.totalRecords = resp.response.data.totalCount;
            this.resultsPerPage = resp.response.data.resultsPerPage;
            event.rows = resp.response.data.resultsPerPage;
            event.first = 0;
          } else {

          }
        });
      }
    }
  }

  private missingCountries(): void {
    this.messageService.add({
      severity: 'warn',
      summary: 'Nazioni',
      detail: 'Non sono riuscito a caricare le nazioni'
    });
  }

  filterTitles(event: any): void {
    this.titles = this.allTitles.filter(title =>
      title.trim().toLowerCase().includes(event.query.trim().toLowerCase())
    );
  }

  filterCountries(event: any): void {
    this.countries = this.allCountries.filter(country =>
      country.name.trim().toLowerCase().includes(event.query.trim().toLowerCase())
    );
  }

  takeAction(): void {
    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid) {
      this.messageService.add({
        severity: 'error',
        summary: 'Errori nel modulo',
        detail: 'Controlla e correggi gli errori segnalati'
      });
    } else {
      if (this.request) {
        this.request.unsubscribe();
      }

      let fn = null;
      if (this.operation === Operation.CREATE) {
        fn = this.teachersService.add(this.form.value);
      } else if (this.operation === Operation.UPDATE) {
        fn = this.teachersService.update(this.id, this.form.value);
      }

      if (fn) {
        this.form.value.nazione = this.form.value.nazione.name;

        this.request = fn.subscribe((resp: BaseResponseDto<boolean | string>) => {
          if (resp.status === 200 && resp.success) {
            this.messageService.add({
              severity: 'success',
              summary: 'Operazione riuscita!',
              detail: 'Docente ' + (this.operation === Operation.CREATE ? 'aggiunto' : 'modificato')
            });
            if (this.operation === Operation.CREATE) {
              this.form.reset();
              this.submitted = false;
            }
          } else {
            let text = null;
            if (resp.response === 'DATA_INTEGRITY_ERROR') {
              text = 'Partita IVA o e-mail esistono già';
            } else {
              text = 'Nazione non corretta';
            }
            this.messageService.add({
              severity: 'error',
              summary: 'Errore',
              detail: text
            });
          }
        });
      }
    }
  }

  get f() {
    return this.form.controls;
  }

  rowClicked(row: string): void {
    this.router.navigate(['courses', row]);
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  showDialog() {
    this.displayModal = true;
  }

  deleteTeacher() {
    this.teachersService.setTrashed(this.id, this.teacher, true).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['teachers']);
        }
        if (resp.error) {
          if (resp.response === 'DATA_INTEGRITY_ERROR') {
            this.generateError('Impossibile eliminare', 'Il docente che vuoi cestinare sta svolgendo un corso');
          }
          if (resp.response === 'DELETE_FAILED') {
            this.generateError('Impossibile eliminare', 'Non sono riuscito a cestinare il docente');
          }
          if (resp.response === 'EMPTY_ID') {
            this.generateError('Impossibile eliminare', 'Nessun docente selezionato');
          }
          if (resp.response === 'EMPTY_DTO') {
            this.generateError('Impossibile eliminare', 'Il docente da cestinare non è stato passato al DTO');
          }
          if (resp.response === 'UPDATE_FAILED') {
            this.generateError('Impossibile modificare', 'Questo docente non può essere modificato:(');
          }
        }
      } else {
        this.generateError('Errore rimozione', 'Non sono riuscito a cestinare il docente');
      }
    }, () => {
      this.generateError('Errore rimozione', 'Non sono riuscito a cestinare il docente');
    });
  }

  deleteDefinitively() {
    this.deleteDef = true;
  }

  yesDeleteDefinitively() {
    this.teachersService.deleteById(this.id).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['', 'trashbin', 'teachers']);
        }
        if (resp.error) {
          if (resp.response === 'DATA_INTEGRITY_ERROR') {
            this.generateError('Impossibile eliminare', 'Il docente che vuoi eliminare sta svolgendo un corso');
          }
          if (resp.response === 'DELETE_FAILED') {
            this.generateError('Impossibile eliminare', 'Non sono riuscito a rimuovere il docente');
          }
          if (resp.response === 'EMPTY_ID') {
            this.generateError('Impossibile eliminare', 'Nessun docente selezionato');
          }
          if (resp.response === 'EMPTY_DTO') {
            this.generateError('Impossibile eliminare', 'Il docente da elimanare non è stato passato al DTO');
          }
          if (resp.response === 'UPDATE_FAILED') {
            this.generateError('Impossibile modificare', 'Questo docente non può essere modificato:(');
          }
        }
      } else {
        this.generateError('Errore rimozione', 'Non sono riuscito a rimuovere il docente');
      }
      this.deleteDef = false;
    }, () => {
      this.deleteDef = false;
      this.generateError('Errore rimozione', 'Non sono riuscito a rimuovere il docente');
    });
  }

  restore() {
    this.restoreTeacher = true;
  }

  yesRestore() {
    this.teachersService.setTrashed(this.id, this.teacher, false).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['', 'trashbin', 'teachers']);
        }
        if (resp.error) {
          this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il docente');
        }
      } else {
        this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il docente');
      }
    }, () => {
      this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il docente');
    });
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }
}
