import {Component, OnInit} from '@angular/core';
import {TeacherDto, TeachersService} from '../../services/teachers.service';
import {ActionBtn, DynamicColumns, HeaderBtn, Position, Search} from '../../containers/advanced-table/advanced-table.component';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {Router} from '@angular/router';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {PageableDto} from '../../interfaces/pageableDto';
import {OrderService} from '../../services/order.service';
import {EmployeesDto, EmployeesService} from '../../services/employees.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-docenti',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss'],
  providers: [DatePipe]
})
export class TeachersComponent implements OnInit {

  loading: boolean = true;
  loadingInterno: boolean = true;
  list: TeacherDto[] = null;
  errors: string[] = [];
  requestEsterno: any = null;
  requestInterno: any = null;
  totalRecords: number;
  totalRecordsInterno: number;
  resultsPerPage: number = null;
  resultsPerPageInterno: number = null;
  visible: boolean = true;
  lastLazyLoadEsterno: LazyLoadEvent = null;
  lastLazyLoadInterno: LazyLoadEvent = null;
  page: number = 1;
  pageInterno: number = 1;
  employees: EmployeesDto[] = null;


  columns: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'titolo',
        header: 'Titolo',
        visible: false
      },
      {
        field: 'ragioneSociale',
        header: 'Ragione sociale',
        visible: true
      },
      {
        field: 'partitaIva',
        header: 'Partita IVA',
        visible: true
      },
      {
        field: 'telefono',
        header: 'Telefono',
        visible: true
      },
      {
        field: 'email',
        header: 'Email',
        visible: true
      },
      {
        field: 'referente',
        header: 'Referente',
        visible: true
      },
      {
        field: 'localita',
        header: 'Località',
        visible: false
      },
      {
        field: 'provincia',
        header: 'Provincia',
        visible: true
      },
    ],
  };

  columnsInterno: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'matricola',
        header: 'Matricola',
        visible: true
      },
      {
        field: 'cognome',
        header: 'Cognome',
        visible: true
      },
      {
        field: 'nome',
        header: 'Nome',
        visible: true
      },
      {
        field: 'tipoDipendente',
        header: 'Tipo dipendente',
        visible: true
      },
      {
        field: 'qualifica',
        header: 'Qualifica',
        visible: false
      },
      {
        field: 'responsabileRisorsa',
        header: 'Responsabile risorsa',
        visible: true
      },
      {
        field: 'sesso',
        header: 'Sesso',
        visible: false
      },
      {
        field: 'dataNascita',
        header: 'Data di nascita',
        visible: false
      },
      {
        field: 'luogoNascita',
        header: 'Luogo di nascita',
        visible: false
      },
      {
        field: 'statoCivile',
        header: 'Stato civile',
        visible: false
      },
      {
        field: 'titoloStudio',
        header: 'Titolo di studio',
        visible: false
      },
      {
        field: 'conseguitoPresso',
        header: 'Titolo conseguito presso',
        visible: false
      },
      {
        field: 'annoConseguimento',
        header: 'Titolo conseguito il',
        visible: false
      },
      {
        field: 'livello',
        header: 'Livello',
        visible: false
      },
      {
        field: 'responsabileArea',
        header: 'Responsabile di area',
        visible: false
      },
      {
        field: 'dataAssunzione',
        header: 'Data di assunzione',
        visible: false
      },
      {
        field: 'dataFineRapporto',
        header: 'Data di fine rapporto',
        visible: false
      },
      {
        field: 'dataScadenzaContratto',
        header: 'Data di scadenza contratto',
        visible: false
      },
    ]
  };


  headersBtns: HeaderBtn[] = [
    {
      id: 'insert',
      title: 'Inserisci',
      icon: 'pi-plus',
      class: ['btn-success', 'text-white', 'float-left'],
      position: Position.LEFT
    }
  ];

  actions: ActionBtn[] = [
    {
      id: 'open',
      title: 'Visualizza',
      icon: 'pi-eye'
    },
    {
      id: 'edit',
      title: 'Modifica',
      icon: 'pi-pencil',
      class: ['btn-warning', 'text-white']
    },
    {
      id: 'delete',
      title: 'Elimina',
      icon: 'pi-trash',
      class: ['btn-danger', 'text-white']
    }
  ];

  actionsInterno: ActionBtn[] = [
    {
      id: 'open',
      title: 'Visualizza',
      icon: 'pi-eye'
    }
  ];

  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei docenti...'
  };

  searchInterno: Search = {
    enabled: false,
    placeholder: 'Cerca nei dipendenti...'
  };

  constructor(
    private employeesService: EmployeesService,
    private teachersService: TeachersService,
    private router: Router,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private orderService: OrderService
  ) {
  }

  ngOnInit(): void {
  }

  searchFn(type: string, value: string): void {
    if (type === 'esterno') {
      this.search.defaultValue = value.trim().length === 0 ? null : value.trim();
      this.pageChanged('esterno', 1);
    } else if (type === 'interno') {
      this.searchInterno.defaultValue = value.trim().length === 0 ? null : value.trim();
      this.pageChanged('interno', 1);
    }
  }

  lazyLoad(type: string, event: LazyLoadEvent = null): void {
    if (type === 'esterno') {
      if (event === null) {
        event = this.lastLazyLoadEsterno;
      }

      this.loading = true;
      this.lastLazyLoadEsterno = event;

      if (this.requestEsterno) {
        this.requestEsterno.unsubscribe();
      }

      let fn;
      if (this.search.defaultValue !== undefined && this.search.defaultValue !== null) {
        fn = this.teachersService.searchByField(
          this.search.defaultValue,
          this.page,
          this.resultsPerPage,
          this.orderService.parse(event.multiSortMeta)
        );
      } else {
        fn = this.teachersService.list(
          this.page,
          this.resultsPerPage,
          this.orderService.parse(event.multiSortMeta)
        );
        this.search.defaultValue = null;
      }

      if (fn) {
        this.requestEsterno = fn.subscribe((resp: BaseResponseDto<PageableDto<TeacherDto[]>>) => {
          this.loading = false;

          if (resp.status === 200 && resp.success) {
            this.list = resp.response.data;
            this.totalRecords = resp.response.totalCount;
            this.resultsPerPage = resp.response.resultsPerPage;
            event.rows = resp.response.resultsPerPage;
            event.first = 0;
          }
          if (resp.error) {
            this.errors.push('Si &egrave; verificato un errore');
          }
        });
      }
    } else if (type === 'interno') {
      if (event === null) {
        event = this.lastLazyLoadInterno;
      }

      this.loadingInterno = true;
      this.lastLazyLoadInterno = event;

      if (this.requestInterno) {
        this.requestInterno.unsubscribe();
      }

      let fn;
      if (this.searchInterno.defaultValue !== undefined && this.searchInterno.defaultValue !== null) {
        fn = this.employeesService.searchByMatricolaEmployeesTeachers(
          this.searchInterno.defaultValue,
          this.pageInterno,
          this.resultsPerPageInterno,
          this.orderService.parse(event.multiSortMeta)
        );
        this.search.defaultValue = event.globalFilter;
      } else {
        fn = this.employeesService.employeesTeacherToo(
          this.pageInterno,
          this.resultsPerPageInterno,
          this.orderService.parse(event.multiSortMeta)
        );
        this.searchInterno.defaultValue = null;
      }

      if (fn) {
        this.requestInterno = fn.subscribe((resp: BaseResponseDto<PageableDto<EmployeesDto[]>>) => {
          this.loadingInterno = false;

          if (resp.status === 200 && resp.success) {
            resp.response.data.forEach(employee => {
              employee.dataAssunzione = employee.dataAssunzione !== null ? this.datePipe.transform(employee.dataAssunzione, 'dd/MM/yyyy') : 'Non impostata';
              employee.dataFineRapporto = employee.dataFineRapporto !== null ? this.datePipe.transform(employee.dataFineRapporto, 'dd/MM/yyyy') : 'Non impostata';
              employee.dataNascita = employee.dataNascita !== null ? this.datePipe.transform(employee.dataNascita, 'dd/MM/yyyy') : 'Non impostata';
              employee.dataScadenzaContratto = employee.dataScadenzaContratto !== null ? this.datePipe.transform(employee.dataScadenzaContratto, 'dd/MM/yyyy') : 'Non impostata';
            });
            this.employees = resp.response.data;
            this.totalRecordsInterno = resp.response.totalCount;
            this.resultsPerPageInterno = resp.response.resultsPerPage;
            event.rows = resp.response.resultsPerPage;
            event.first = 0;
          }
          if (resp.error) {
            this.errors.push('Si &egrave; verificato un errore');
          }
        });
      }
    }

  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  rowClicked(type: string, row: string): void {
    if (type === 'esterno') {
      this.router.navigate(['teachers', row]);
    } else if (type === 'interno') {
      this.router.navigate(['employees', row]);
    }

  }

  headerBtnClicked(action: string) {
    switch (action) {
      case 'insert':
        this.router.navigate(['teachers', 'insert']);
        break;
    }
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged('esterno', 1);
  }

  actionBtnClicked(type: string, pair: any): void {
    if (type === 'esterno') {
      const docenteId = pair.row;
      const action = pair.action;

      switch (action) {
        case 'open':
          this.router.navigate(['teachers', docenteId]);
          break;
        case 'edit':
          this.router.navigate(['teachers', 'edit', docenteId]);
          break;
        case 'delete':
          const teacher = this.list.find(currentTeacher => currentTeacher.docenteId === docenteId);

          this.confirmationService.confirm({
            message: 'Sei sicuro che vuoi cestinare il docente <b>' + (teacher.ragioneSociale) + '</b>?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              this.teachersService.setTrashed(teacher.docenteId, teacher, true).subscribe((resp: BaseResponseDto<boolean | string>) => {
                if (resp.status === 200) {
                  if (resp.success) {
                    this.messageService.add({
                      severity: 'success',
                      summary: 'Rimozione riuscita',
                      detail: 'Il docente ' + teacher.ragioneSociale + ' è stato spostato nel cestino'
                    });
                    this.lazyLoad('esterno');
                  }
                  if (resp.error) {
                    if (resp.response === 'DATA_INTEGRITY_ERROR') {
                      this.generateError('Impossibile eliminare', true, 'Il docente che vuoi cestinare sta svolgendo un corso');
                    }
                    if (resp.response === 'DELETE_FAILED') {
                      this.generateError('Impossibile eliminare', true, 'Non sono riuscito a cestinare il docente');
                    }
                    if (resp.response === 'EMPTY_ID') {
                      this.generateError('Impossibile eliminare', true, 'Nessun docente selezionato');
                    }
                    if (resp.response === 'EMPTY_DTO') {
                      this.generateError('Impossibile eliminare', true, 'Il docente da cestinare non è stato passato al DTO');
                    }
                    if (resp.response === 'UPDATE_FAILED') {
                      this.generateError('Impossibile modificare', true, 'Questo docente non può essere modificato:(');
                    }
                  }
                } else {
                  this.generateError('Errore rimozione', true, 'Non sono riuscito a cestinare il docente');
                }
              }, () => {
                this.generateError('Errore rimozione', true, 'Non sono riuscito a cestinare il docente');
              });
            }
          });
          break;
      }
    } else if (type === 'interno') {
      const row = pair.row;
      const action = pair.action;

      switch (action) {
        case 'open':
          this.router.navigate(['employees', row]);
          break;
        case 'edit':
          this.router.navigate(['employees', 'edit', row]);
          break;
      }
    }
  }

  pageChanged(type: string, page: number) {
    if (type === 'esterno') {
      this.page = page;
      this.lazyLoad('esterno');
    } else if (type === 'interno') {
      this.pageInterno = page;
      this.lazyLoad('interno');
    }

  }
}
