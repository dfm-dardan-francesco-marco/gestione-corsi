import {Component, OnInit} from '@angular/core';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../../services/courses.service';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {ActionBtn, DynamicColumns} from '../../../containers/advanced-table/advanced-table.component';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseResponseDto} from '../../../interfaces/baseResponseDto';
import {PageableDto} from '../../../interfaces/pageableDto';
import {ServiceResponse} from '../../../interfaces/serviceResponse';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-type',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss'],
  providers: [DatePipe]
})
export class TypeComponent implements OnInit {

  loading: boolean = true;
  courses: CoursesDto[] = null;
  errors: string[] = [];
  request: any = null;
  totalRecords: number;
  resultsPerPage: number = null;
  visible: boolean = true;
  lastLazyLoad: LazyLoadEvent = null;
  id: number;
  descrizione: string;
  dictionaries: CoursesDictionariesDto = null;
  page: number = 1;

  columns: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'tipologia',
        header: 'Tipologia',
        visible: true
      },
      {
        field: 'tipo',
        header: 'Tipo',
        visible: true
      },
      {
        field: 'luogo',
        header: 'Luogo',
        visible: true
      },
      {
        field: 'ente',
        header: 'Ente',
        visible: true
      },
      {
        field: 'edizione',
        header: 'Edizione',
        visible: false
      },
      {
        field: 'dataErogazione',
        header: 'Data Erogazione',
        visible: true
      },
      {
        field: 'dataChiusura',
        header: 'Data Chiusura',
        visible: false
      },
      {
        field: 'dataCensimento',
        header: 'Data Censimento',
        visible: false
      },
    ]
  };

  actions: ActionBtn[] = [
    {
      id: 'open',
      title: 'Visualizza',
      icon: 'pi-eye'
    },
    {
      id: 'edit',
      title: 'Modifica',
      icon: 'pi-pencil',
      class: ['btn-warning', 'text-white']
    },
    {
      id: 'delete',
      title: 'Elimina',
      icon: 'pi-trash',
      class: ['btn-danger', 'text-white']
    }
  ];


  constructor(
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private coursesService: CoursesService,
    private route: ActivatedRoute,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params.id;
      this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
        if (resp.status === 200 && resp.success) {
          this.dictionaries = resp.response.data;
          this.descrizione = this.dictionaries.tipi.find(z => z.tipoId === Number(this.id)).descrizione;
        }
      });
    });
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  rowClicked(row: string): void {
    this.router.navigate(['courses', row]);
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.lastLazyLoad;
    }

    this.loading = true;
    this.lastLazyLoad = event;

    if (this.request) {
      this.request.unsubscribe();
    }

    let fn;
    if (event.globalFilter !== null) {
      /*fn = this.coursesService.searchByField(
        event.globalFilter,
        this.page,
        this.resultsPerPage,
        event.sortField,
        event.sortOrder
      );*/
    } else {
      fn = this.coursesService.getByType(
        this.id,
        this.page,
        this.resultsPerPage,
        event.sortField,
        event.sortOrder
      );
    }

    if (fn) {
      this.request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesDto[]>>) => {
        this.loading = false;

        if (resp.status === 200 && resp.success) {
          if (this.dictionaries !== null) {
            resp.response.data.forEach(course => {
              course.dataErogazione = course.dataErogazione !== null ? this.datePipe.transform(course.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
              course.dataChiusura = course.dataChiusura !== null ? this.datePipe.transform(course.dataChiusura, 'dd/MM/yyyy') : 'Non impostata';
              course.dataCensimento = course.dataCensimento !== null ? this.datePipe.transform(course.dataCensimento, 'dd/MM/yyyy') : 'Non impostata';
              course.tipo = this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
              course.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
            });
          }
          this.courses = resp.response.data;
          this.totalRecords = resp.response.totalCount;
          this.resultsPerPage = resp.response.resultsPerPage;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        } else {
          this.generateError('Impossibile caricare la lista dei corsi', true);
        }
      });
    }
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  actionBtnClicked(pair: any): void {
    const corso_id = pair.row;
    const action = pair.action;
    switch (action) {
      case 'open':
        this.router.navigate(['courses', corso_id]);
        break;
      case 'edit':
        this.router.navigate(['courses', 'edit', corso_id]);
        break;
      case 'delete':
        const course = this.courses.find(corso => corso.corsoId === corso_id);
        this.confirmationService.confirm({
          message: 'Sei sicuro che vuoi eliminare il <b>' + (course.tipo) + ' - ' + (course.tipologia) + '</b>?',
          acceptLabel: 'Conferma',
          rejectLabel: 'Annulla',
          defaultFocus: 'reject',
          accept: () => {
            this.lazyLoad();

            this.coursesService.deleteById(course.corsoId).subscribe((resp: BaseResponseDto<boolean | string>) => {
              if (resp.status === 200) {
                if (resp.success) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Rimozione riuscita',
                    detail: 'Il ' + course.tipo + ' - ' + course.tipologia + ' è stato rimosso'
                  });
                  this.lazyLoad();
                }
                if (resp.error) {
                  if (resp.response === 'DATA_INTEGRITY_ERROR') {
                    this.generateError('Impossibile eliminare', true, 'Per eliminare un corso, devi prima eliminare il docente e gli studenti');
                  }
                  if (resp.response === 'DELETE_FAILED') {
                    this.generateError('Impossibile eliminare', true, 'Non sono riuscito a rimuovere il corso');
                  }
                  if (resp.response === 'EMPTY_ID') {
                    this.generateError('Impossibile eliminare', true, 'Nessun corso selezionato');
                  }
                  if (resp.response === 'EMPTY_DTO') {
                    this.generateError('Impossibile eliminare', true, 'Il corso da eliminare non è stato passato al DTO');
                  }
                  if (resp.response === 'UPDATE_FAILED') {
                    this.generateError('Impossibile modificare', true, 'Questo corso non può essere modificato:(');
                  }
                }
              }
            }, () => {
              this.generateError('Errore rimozione', true, 'Non sono riuscito a rimuovere il corso');
            });
          }

        });
        break;
    }
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }

}
