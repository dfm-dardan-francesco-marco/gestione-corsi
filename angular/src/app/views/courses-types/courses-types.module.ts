import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoursesTypesComponent} from './courses-types.component';
import {CommonModule} from '@angular/common';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {ColorPickerModule} from 'primeng/colorpicker';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {CardModule} from 'primeng/card';
import {TypeComponent} from './types/types.component';
import {CoursesTypesRoutingModule} from './courses-types-routing.module';
import {ToastModule} from 'primeng/toast';
import {PaginatorModule} from 'primeng/paginator';
import {SharedModule} from '../../modules/shared.module';

@NgModule({
  imports: [
    FormsModule,
    CoursesTypesRoutingModule,
    CommonModule,
    ButtonModule,
    DialogModule,
    ReactiveFormsModule,
    ColorPickerModule,
    ConfirmDialogModule,
    DynamicDialogModule,
    ToastModule,
    SharedModule,
    PaginatorModule,
    CardModule
  ],
  declarations: [
    CoursesTypesComponent,
    TypeComponent,
  ]
})
export class CoursesTypesModule {
}
