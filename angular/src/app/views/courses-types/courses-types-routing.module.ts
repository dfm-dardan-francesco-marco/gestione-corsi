import {RouterModule, Routes} from '@angular/router';
import {CoursesTypesComponent} from './courses-types.component';
import {NgModule} from '@angular/core';
import {TypeComponent} from './types/types.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesTypesComponent,
    data: {
      title: 'Tipi di corso'
    }
  },
  {
    path: ':id',
    component: TypeComponent,
    data: {
      title: 'Elenco corsi per tipo'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesTypesRoutingModule {
}
