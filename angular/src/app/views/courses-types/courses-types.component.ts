import {Component, OnInit} from '@angular/core';
import {TypesDto, TypesService} from '../../services/types.service';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {PageableDto} from '../../interfaces/pageableDto';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ConfirmationService, MessageService} from 'primeng/api';
import {CoursesDto} from '../../services/courses.service';

@Component({
  selector: 'app-courses-types',
  templateUrl: './courses-types.component.html',
  styleUrls: ['./courses-types.component.scss']
})
export class CoursesTypesComponent implements OnInit {

  columns: any = null;
  types: any[];
  totalRecords: number;
  resultsPerPage: number = 20;
  pages: number;
  displayModal: boolean;
  form: FormGroup;
  form2: FormGroup;
  request: any = null;
  errors: string[] = [];
  type: TypesDto = null;
  page: number = 1;

  constructor(
    private typesService: TypesService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
  ) {
  }

  ngOnInit(): void {
    this.refresh();
    this.form = this.fb.group({
      descrizione: null
    });
    this.form2 = this.fb.group({
      descrizione: null
    });
  }

  changeResultsPerPage(number: any): void {
    this.resultsPerPage = number.target.value;
    this.refresh();
  }

  refresh(): void {
    this.typesService.list(this.page, this.resultsPerPage).subscribe((resp: BaseResponseDto<PageableDto<TypesDto[]>>) => {
      if (resp.status === 200 && resp.success) {
        this.types = resp.response.data;
        this.totalRecords = resp.response.totalCount;
        this.resultsPerPage = resp.response.resultsPerPage;
        this.pages = resp.response.pages;
      }
    });
  }

  paginate(event: any): void {
    this.page = event.page + 1;
    this.typesService.list(
      this.page,
      this.resultsPerPage,
      event.sortField
    ).subscribe((resp: BaseResponseDto<PageableDto<TypesDto[]>>) => {
        if (resp.status === 200 && resp.success) {
          this.types = resp.response.data;
          this.totalRecords = resp.response.totalCount;
          this.resultsPerPage = resp.response.resultsPerPage;
          this.pages = resp.response.pages;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        }
      }
    );
  }

  showModalDialog(tipo: TypesDto) {
    this.displayModal = true;
    this.type = tipo;
    this.typesService.getById(this.type.tipoId).subscribe((resp: BaseResponseDto<CoursesDto>) => {
      this.form2 = this.fb.group({
        descrizione: resp.response.descrizione
      });
    });
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  add(): void {
    this.request = this.typesService.add(this.form.value).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200 && resp.success) {
        this.messageService.add({
          severity: 'success',
          summary: 'Operazione riuscita!',
          detail: 'Tipo corso aggiunto'
        });
        this.refresh();
        this.form.patchValue({
          descrizione: null
        });
      } else {
        let text = null;
        if (resp.response === 'DATA_INTEGRITY_ERROR') {
          text = 'Devi inserire la descrizione';
        }
        this.messageService.add({
          severity: 'error',
          summary: 'Errore',
          detail: text
        });
      }
    });
  }

  update() {
    this.typesService.update(this.type.tipoId, this.form2.value).subscribe((resp: BaseResponseDto<never>) => {
      if (resp.status === 200 && resp.success) {
        this.messageService.add({
          severity: 'success',
          summary: 'Operazione riuscita!',
          detail: 'Tipo corso modificato'
        });
        this.refresh();
      }
    });
    this.displayModal = false;
  }

  delete(tipo: TypesDto) {
    this.confirmationService.confirm({
      message: 'Sei sicuro che vuoi eliminare <b>' + (tipo.descrizione) + '</b>?',
      acceptLabel: 'Conferma',
      rejectLabel: 'Annulla',
      defaultFocus: 'reject',
      accept: () => {
        this.typesService.deleteById(tipo.tipoId).subscribe((resp: BaseResponseDto<boolean | string>) => {
          if (resp.status === 200) {
            if (resp.success) {
              this.messageService.add({
                severity: 'success',
                summary: 'Rimozione riuscita',
                detail: 'Il tipo di corso ' + tipo.descrizione + ' è stato rimosso'
              });
              this.refresh();
            }
            if (resp.error) {
              if (resp.response === 'DATA_INTEGRITY_ERROR') {
                this.generateError('Impossibile eliminare', true, 'Per eliminare un tipo, devi prima eliminare i corsi collegati');
              }
              if (resp.response === 'DELETE_FAILED') {
                this.generateError('Impossibile eliminare', true, 'Non sono riuscito a rimuovere il tipo');
              }
              if (resp.response === 'EMPTY_ID') {
                this.generateError('Impossibile eliminare', true, 'Nessun tipo selezionato');
              }
              if (resp.response === 'EMPTY_DTO') {
                this.generateError('Impossibile eliminare', true, 'Il tipo da eliminare non è stato passato al DTO');
              }
              if (resp.response === 'UPDATE_FAILED') {
                this.generateError('Impossibile modificare', true, 'Questo tipo non può essere modificato:(');
              }
            }
          }
        });
      }
    });
  }
}
