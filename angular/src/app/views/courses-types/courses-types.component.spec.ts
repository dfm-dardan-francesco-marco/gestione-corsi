import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CoursesTypesComponent} from './courses-types.component';

describe('CoursesTypesComponent', () => {
  let component: CoursesTypesComponent;
  let fixture: ComponentFixture<CoursesTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CoursesTypesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
