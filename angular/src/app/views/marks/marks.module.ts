import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MarksComponent} from './marks.component';
import {MarksRoutingModule} from './marks-routing.module';
import {SharedModule} from '../../modules/shared.module';
import {TabViewModule} from 'primeng/tabview';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {NewEditMarkComponent} from './new-edit-mark/new-edit-mark.component';
import {RatingModule} from 'primeng/rating';
import {AccordionModule} from 'primeng/accordion';
import {MessageModule} from 'primeng/message';

@NgModule({
  imports: [
    CommonModule,
    MarksRoutingModule,
    SharedModule,
    TabViewModule,
    ButtonModule,
    DialogModule,
    RatingModule,
    AccordionModule,
    MessageModule,
  ],
  declarations: [
    MarksComponent,
    NewEditMarkComponent
  ]
})
export class MarksModule {
}
