import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {ActionBtn, ColType, DynamicColumns, Search} from '../../containers/advanced-table/advanced-table.component';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {PageableDto} from '../../interfaces/pageableDto';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../services/courses.service';
import {ServiceResponse} from '../../interfaces/serviceResponse';
import {DatePipe} from '@angular/common';
import {OrderService} from '../../services/order.service';
import {MarksCoursesService} from '../../services/marks-courses.service';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.scss'],
  providers: [DatePipe]
})
export class MarksComponent implements OnInit {

  visible: boolean = true;
  courses: CoursesDto[] = null;
  loading: boolean = true;
  totalRecords: number;
  resultsPerPage: number = null;
  errors: string[] = [];
  lastLazyLoad: LazyLoadEvent = null;
  request: any = null;
  dictionaries: CoursesDictionariesDto = null;
  page: number = 1;

  columns: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'tipologia',
        header: 'Tipologia',
        visible: true
      },
      {
        field: 'tipo',
        header: 'Tipo',
        visible: true
      },
      {
        field: 'luogo',
        header: 'Luogo',
        visible: true
      },
      {
        field: 'ente',
        header: 'Ente',
        visible: true
      },
      {
        field: 'edizione',
        header: 'Edizione',
        visible: false
      },
      {
        field: 'dataErogazione',
        header: 'Data Erogazione',
        visible: true
      },
      {
        field: 'dataChiusura',
        header: 'Data Chiusura',
        visible: true
      },
      {
        field: 'dataCensimento',
        header: 'Data Censimento',
        visible: false
      },
      {
        field: 'valutazioni',
        header: 'Valutazioni',
        visible: true,
        type: ColType.PROGRESS_BAR
      }
    ]
  };
  actions: ActionBtn[] = [
    {
      id: 'open',
      title: 'Dettagli corso',
      icon: 'pi-eye'
    },
    {
      id: 'rate',
      title: 'Valuta',
      icon: 'pi-check',
      class: ['btn-success', 'text-white']
    },
  ];
  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei corsi...'
  };

  constructor(
    private coursesService: CoursesService,
    private marksCoursesService: MarksCoursesService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private orderService: OrderService
  ) {
  }

  ngOnInit(): void {
    this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.dictionaries = resp.response.data;
      }
    });
  }

  searchFn(value: string): void {
    this.search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(1);
  }

  actionBtnClicked(pair: any): void {
    const corsoId = pair.row;
    const action = pair.action;
    switch (action) {
      case 'open':
        this.router.navigate(['courses', corsoId]);
        break;
      case 'rate':
        this.router.navigate(['marks', corsoId]);
        break;
    }
  }

  rowClicked(row: string): void {
    this.router.navigate(['marks', row]);
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.lastLazyLoad;
    }

    this.loading = true;
    this.lastLazyLoad = event;

    if (this.request) {
      this.request.unsubscribe();
    }

    const multiSortMeta = [];
    if (event.multiSortMeta !== null && event.multiSortMeta !== undefined) {
      event.multiSortMeta.forEach(item => {
        const temp = {...item};
        temp.field = this.mapSortField(temp.field);
        multiSortMeta.push(temp);
      });
    }

    let fn;
    if (this.search.defaultValue !== undefined && this.search.defaultValue !== null) {
      fn = this.coursesService.searchByField(
        this.search.defaultValue,
        this.page,
        this.resultsPerPage,
        this.orderService.parse(multiSortMeta)
      );
    } else {
      fn = this.coursesService.list(
        this.page,
        this.resultsPerPage,
        this.orderService.parse(multiSortMeta)
      );
      this.search.defaultValue = null;
    }

    this.request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesDto[]>>) => {
      this.loading = false;

      if (resp.status === 200 && resp.success) {
        if (this.dictionaries !== null) {
          resp.response.data.forEach(course => {
            course.dataErogazione = course.dataErogazione !== null ? this.datePipe.transform(course.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
            course.dataChiusura = course.dataChiusura !== null ? this.datePipe.transform(course.dataChiusura, 'dd/MM/yyyy') : 'Non impostata';
            course.dataCensimento = course.dataCensimento !== null ? this.datePipe.transform(course.dataCensimento, 'dd/MM/yyyy') : 'Non impostata';
            course.tipo = this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
            course.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
          });
        }
        this.courses = resp.response.data;
        this.totalRecords = resp.response.totalCount;
        this.resultsPerPage = resp.response.resultsPerPage;
        event.rows = resp.response.resultsPerPage;
        event.first = 0;
      } else {
        this.generateError(null, true);
      }
    });
  }

  private mapSortField(field: string): string {
    switch (field) {
      case 'tipo':
        return 'typesDao.descrizione';
      case 'tipologia':
        return 'typologiesDao.descrizione';
      default:
        return field;
    }
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }

}
