import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MarksComponent} from './marks.component';
import {NewEditMarkComponent} from './new-edit-mark/new-edit-mark.component';

const routes: Routes = [
  {
    path: '',
    component: MarksComponent,
    data: {
      title: 'Lista valutazioni mancanti'
    }
  },
  {
    path: ':id_r',
    component: NewEditMarkComponent,
    data: {
      title: 'Visualizza valutazioni',
      read: true
    }
  },
  {
    path: ':edit/:id_u',
    component: NewEditMarkComponent,
    data: {
      title: 'Inserimento valutazione',
      edit: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarksRoutingModule {
}
