import {Component, OnInit} from '@angular/core';
import {CoursesTeachersDto, CoursesTeachersService} from '../../../services/courses-teachers.service';
import {ActivatedRoute} from '@angular/router';
import {BaseResponseDto} from '../../../interfaces/baseResponseDto';
import {PageableDto} from '../../../interfaces/pageableDto';
import {MarksCoursesDto, MarksCoursesService, ParametersMarksDto, ParamsMarksDto} from '../../../services/marks-courses.service';
import {CoursesUsersDto, CoursesUsersService} from '../../../services/courses-users.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {LazyLoadEvent, MessageService} from 'primeng/api';
import {TableTypes} from '../../courses/new-edit-course/new-edit-course.component';
import {MarksTeachersDto, MarksTeachersService} from '../../../services/marks-teachers.service';
import {MarksUsersDto, MarksUsersService} from '../../../services/marks-users.service';
import {ServiceResponse} from '../../../interfaces/serviceResponse';
import {CoursesDictionariesDto, CoursesDto, CoursesService, TypologiesDto} from '../../../services/courses.service';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';

enum Operation {
  CREATE,
  READ,
  UPDATE
}

@Component({
  selector: 'app-new-edit-mark',
  templateUrl: './new-edit-mark.component.html',
  styleUrls: ['./new-edit-mark.component.scss'],
  providers: [DatePipe]
})
export class NewEditMarkComponent implements OnInit {

  readonly Operation = Operation;
  readonly moment = moment;

  id: number = null;
  request: any = null;
  operation: Operation = Operation.READ;
  operationCourse: Operation = Operation.READ;
  operationTeacher: Operation = Operation.READ;
  operationUser: Operation = Operation.READ;
  course: CoursesDto = null;
  courseMarksOperation: Operation = Operation.READ;
  teacherMarksOperation: Operation = Operation.READ;
  userMarksOperation: Operation = Operation.READ;
  paramMarksDto: ParametersMarksDto[] = null;
  paramCourse: ParamsMarksDto[] = [];
  paramTeacher: ParamsMarksDto[] = [];
  paramUser: ParamsMarksDto[] = [];
  rateTeacher: boolean;
  rateUser: boolean;
  courseForm: FormGroup = null;
  teacherForm: FormGroup = null;
  userForm: FormGroup = null;
  idCourseTeacher: number = null;
  idCourseUser: number = null;
  courseTeacher: CoursesTeachersDto;
  teacher: string;
  courseUser: CoursesUsersDto;
  user: string;
  index: number = 0;
  userNew: boolean;
  markUserId: number[] = [];
  courseAverages: number[] = [];
  alertMaxUsers: boolean[] = [];
  teacherAverages: number[] = [];
  courseAverage: number = null;
  teacherAverage: number = null;
  userAverage: number = null;
  dictionaries: CoursesDictionariesDto = null;
  tipologia: TypologiesDto = null;
  tipo: string = null;
  visibleSidebar: boolean = false;
  dataProva: string;
  counterUsers: number = 0;
  sendCoursesWithoutAlert: boolean;

  readonly tables: { [k: string]: TableTypes } = {
    'teachers': {
      rowID: 'corsiDocentiId',
      columns: [
        {
          field: 'showedText',
          header: 'Docente'
        },
        {
          field: 'type',
          header: 'Tipo'
        },
        {
          field: 'media',
          header: 'Media'
        }
      ],
      rows: [],
      actions: [
        {
          id: 'rate',
          title: 'Valuta docente',
          icon: 'pi-star',
          class: ['btn-warning', 'text-white']
        }
      ],
      rowClickable: false,
      search: {
        enabled: false,
        placeholder: 'Cerca nei docenti'
      },
      loading: true,
      totalRecords: 0,
      resultsPerPage: null,
      exportAsCSV: true,
      enableChangeResultsPerPage: false,
      sortable: false,
      suggestions: null,
      selected: [],
      lastLazyLoad: null,
      request: null,
      page: 1
    },
    'employees': {
      rowID: 'corsiUtentiId',
      columns: [
        {
          field: 'showedText',
          header: 'Dipendente'
        },
        {
          field: 'durata',
          header: 'Durata'
        },
        {
          field: 'media',
          header: 'Media'
        }
      ],
      rows: [],
      actions: [
        {
          id: 'rate',
          title: 'Valuta studente',
          icon: 'pi-star',
          class: ['btn-warning', 'text-white']
        },
      ],
      rowClickable: false,
      search: {
        enabled: true,
        placeholder: 'Cerca nei dipendenti'
      },
      loading: true,
      totalRecords: 0,
      resultsPerPage: null,
      exportAsCSV: true,
      enableChangeResultsPerPage: false,
      sortable: false,
      suggestions: null,
      selected: [],
      lastLazyLoad: null,
      request: null,
      page: 1
    }
  };

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private coursesService: CoursesService,
    private coursesTeachersService: CoursesTeachersService,
    private coursesUsersService: CoursesUsersService,
    private markCourseService: MarksCoursesService,
    private marksTeachersService: MarksTeachersService,
    private marksUsersService: MarksUsersService,
    private fb: FormBuilder,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(values => {
      this.route.data.subscribe(data => {
        if (data.read) {
          this.operation = Operation.READ;
          this.id = values.id_r;
        } else if (data.edit) {
          this.operation = Operation.UPDATE;
          this.id = values.id_u;
        }
      });


      this.coursesUsersService.getByCourseId(this.id).subscribe((respA: BaseResponseDto<PageableDto<CoursesUsersDto>>) => {
        this.counterUsers = respA.response.totalCount;
      });

      this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
        if (resp.status === 200 && resp.success) {
          this.dictionaries = resp.response.data;
        }
        this.coursesService.getById(this.id).subscribe((respCourse: BaseResponseDto<CoursesDto>) => {
          if (respCourse.status === 200 && respCourse.success && respCourse.response !== null) {
            typeof respCourse.response.dataErogazione !== 'string' ? respCourse.response.dataErogazione = 'Non impostata' : respCourse.response.dataErogazione = moment(respCourse.response.dataErogazione);
            typeof respCourse.response.dataChiusura !== 'string' ? respCourse.response.dataChiusura = 'Non impostata' : respCourse.response.dataChiusura = moment(respCourse.response.dataChiusura);
            this.course = respCourse.response;
            this.dataProva = moment(this.course.dataChiusura).format('DD/MM/YYYY');
            this.tipologia = this.dictionaries.tipologie.find(item => item.tipologiaId === respCourse.response.tipologia);
            this.tipo = this.dictionaries.tipi.find(item => item.tipoId === respCourse.response.tipo).descrizione;
          } else {
            this.course = null;
            this.generateError('Impossibile caricare il corso', 'Il corso scelto non esiste');
          }
        });
      });

      this.request = this.markCourseService.getParametersMarks().subscribe((resp: BaseResponseDto<ParametersMarksDto[]>) => {
        if (resp.status === 200 && resp.success) {
          this.paramMarksDto = resp.response;
          this.paramCourse = [];
          this.paramTeacher = [];
          this.paramUser = [];

          this.paramMarksDto.forEach(value => {
            if (value.tipo === 'corso') {
              this.paramCourse.push({
                idCriterio: value.idCriterio,
                descrizione: value.descrizione,
              });
            } else if (value.tipo === 'docente') {
              this.paramTeacher.push({
                idCriterio: value.idCriterio,
                descrizione: value.descrizione,
              });
            } else if (value.tipo === 'dipendente') {
              this.paramUser.push({
                idCriterio: value.idCriterio,
                descrizione: value.descrizione,
              });
            }
          });

          const formGroup = {};
          this.paramCourse.forEach(param => {
            formGroup['course-1-' + param.idCriterio] = 0;
            formGroup['course-2-' + param.idCriterio] = 0;
            formGroup['course-3-' + param.idCriterio] = 0;
            formGroup['course-4-' + param.idCriterio] = 0;
            formGroup['course-na-' + param.idCriterio] = 0;
          });
          this.courseForm = this.fb.group(formGroup);


          this.markCourseService.getCourseMarks(this.id).subscribe((respC: BaseResponseDto<PageableDto<MarksCoursesDto[]>>) => {
            if (respC.status === 200 && respC.success) {
              if (respC.response.totalCount === 0) {
                this.courseMarksOperation = Operation.CREATE;
              } else {
                this.courseMarksOperation = Operation.UPDATE;
                respC.response.data.forEach((item: MarksCoursesDto) => {
                  const formItem = {};
                  Object.keys(item.valore).forEach((key: string) => {
                    formItem['course-' + key + '-' + item.criterio] = item.valore[key];
                  });
                  this.courseForm.patchValue(formItem);
                });
              }
              this.calcAvgs('course');
            } else {
              this.generateError('Errore', 'Impossibile trovare il corso!');
            }

            this.courseForm.valueChanges.subscribe(() => this.calcAvgs('course'));
          });
        }
      });
    });
  }

  searchFn(table: string, value: string): void {
    this.tables[table].search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(table, 1);
  }

  actionBtnClicked(table: string, pair: any): void {
    const row = pair.row;
    if (table === 'teachers') {
      this.showModalTeacher(row);
    } else if (table === 'employees') {
      this.showModalUser(row);
    }
  }

  lazyLoad(table: string, event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.tables[table].lastLazyLoad;
    }
    this.tables[table].loading = true;
    this.tables[table].lastLazyLoad = event;
    this.tables[table].request?.unsubscribe();

    const fn = this.getRestFun(table, event);

    if (fn !== null) {
      if (table === 'teachers') {
        this.tables[table].request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesTeachersDto[]>>) => {
          this.tables[table].loading = false;
          const valuesTable = [];
          this.tables[table].request = this.marksTeachersService.getByCourse(this.id).subscribe((result: BaseResponseDto<PageableDto<MarksTeachersDto[]>>) => {
            if (result.response !== null) {
              const arrayDocenti = [];
              const arrayInterno = [];
              result.response.data.forEach(item => {
                let sum = 0;
                let count = 0;
                Object.keys(item.valore).forEach((key: string) => {
                  if (key !== 'na') {
                    count = count + Number(item.valore[key]);
                    sum = sum + (Number(key) * Number(item.valore[key]));
                  }
                });
                const array = item.docente != null ? arrayDocenti : arrayInterno;
                const itemID = item.docente || item.interno;
                if (count !== 0 && sum !== 0) {
                  if (array[itemID] === undefined) {
                    array[itemID] = [];
                  }
                  array[itemID].push(Number((sum / count).toFixed(2)));
                }
              });

              const forEach = (array) => {
                array.forEach(x => {
                  let somma = 0;
                  const n = x.length;
                  x.forEach(y => {
                    somma = somma + y;
                  });
                  array[array.indexOf(x)] = (somma / n).toFixed(2);
                });
              };

              forEach(arrayDocenti);
              forEach(arrayInterno);

              if (resp.status === 200 && resp.success && resp.response !== null) {
                if (resp.response.data !== null) {
                  resp.response.data.forEach(item => {
                    const avg = item.type === 'TEACHER' ? arrayDocenti.find(valore => valore === arrayDocenti[item.docenteOInternoId]) : arrayInterno.find(valore => valore === arrayInterno[item.docenteOInternoId]);
                    valuesTable.push({
                      corsiDocentiId: item.corsiDocentiId,
                      corso: item.corso,
                      docenteOInternoId: item.docenteOInternoId,
                      showedText: item.showedText,
                      type: item.type === 'TEACHER' ? 'Docente' : 'Dipendente',
                      media: avg || 'Non valutato'
                    });
                  });
                }
                this.tables[table].rows = valuesTable;
                this.tables[table].totalRecords = resp.response.totalCount;
                this.tables[table].resultsPerPage = resp.response.resultsPerPage;
                event.rows = resp.response.resultsPerPage;
                event.first = 0;
              } else {
                this.generateError('Impossibile caricare i ' + (table === 'teachers' ? 'docenti' : 'dipendenti'));
              }
            }
          });

        });
      } else {
        this.tables[table].request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesUsersDto[]>>) => {
          this.tables[table].loading = false;
          const valuesTable = [];
          if (resp.status === 200 && resp.success && resp.response !== null) {
            if (resp.response.data !== null) {
              this.tables[table].request = this.marksUsersService.getByCourse(this.id).subscribe((result: BaseResponseDto<PageableDto<MarksUsersDto[]>>) => {

                if (result.status === 200 && result.success && result.response !== null) {

                  resp.response.data.forEach(item => {
                    const valuesAvg = result.response.data.filter(itemeOne => itemeOne.dipendente === item.dipendente);

                    let sum = 0;
                    let count = 0;
                    valuesAvg.forEach(valore => {
                      if (valore.valore !== 0) {
                        sum = sum + valore.valore;
                        count = count + 1;
                      }

                    });

                    valuesTable.push({
                      corsiUtentiId: item.corsiUtentiId,
                      corso: item.corso,
                      dipendente: item.dipendente,
                      showedText: item.showedText,
                      durata: item.durata,
                      media: sum === 0 ? 'Non valutato' : (sum / count).toFixed(2)
                    });

                  });

                } else {
                  resp.response.data.forEach(item => {

                    valuesTable.push({
                      corsiUtentiId: item.corsiUtentiId,
                      corso: item.corso,
                      dipendente: item.dipendente,
                      showedText: item.showedText,
                      durata: item.durata,
                      media: 'Non valutato'
                    });


                  });
                }
              });
            }
            this.tables[table].rows = valuesTable;
            this.tables[table].totalRecords = resp.response.totalCount;
            this.tables[table].resultsPerPage = resp.response.resultsPerPage;
            event.rows = resp.response.resultsPerPage;
            event.first = 0;
          } else {
            this.generateError('Impossibile caricare i ' + (table === 'teachers' ? 'docenti' : 'dipendenti'));
          }
        });
      }
    } else {
      this.tables[table].loading = false;
    }
  }

  private getRestFun(table: string, event: LazyLoadEvent): any {
    let search = null;
    let page = this.tables[table].page;
    if (this.tables[table].search.defaultValue !== undefined && this.tables[table].search.defaultValue !== null) {
      search = this.tables[table].search.defaultValue;
      page = 1;
    }
    if (table === 'teachers') {
      return this.coursesTeachersService.getByCourseId(
        this.id,
        page,
        this.tables[table].resultsPerPage,
        event.sortField,
        event.sortOrder,
        search
      );
    } else if (table === 'employees') {
      return this.coursesUsersService.getByCourseId(
        this.id,
        page,
        this.tables[table].resultsPerPage,
        event.sortField,
        event.sortOrder,
        search
      );
    }
    return null;
  }

  pageChanged(table: string, page: number) {
    this.tables[table].page = page;
    this.lazyLoad(table);
  }

  changeResultsPerPage(table: string, number: number): void {
    this.tables[table].resultsPerPage = number;
    this.pageChanged(table, 1);
  }

  generateError(text: string = null, detail: string = null) {
    this.messageService.add({
      severity: 'error',
      summary: text || 'Si &egrave; verificato un errore',
      detail
    });
  }

  sendCourseMarks(): void {
    if (this.request) {
      this.request.unsubscribe();
    }

    const transformMarksCourse = [];
    const markCoursesDto: MarksCoursesDto[] = [];
    let empty = true;
    Object.keys(this.courseForm.value).forEach(key => {
      const pair = key.replace('course-', '').split('-');
      if (transformMarksCourse[pair[1]] === undefined) {
        transformMarksCourse[pair[1]] = {};
      }
      const value = this.courseForm.get(key).value;
      if (empty === true && value !== 0) {
        empty = false;
      }
      transformMarksCourse[pair[1]][pair[0]] = value;
    });

    if (!empty) {
      transformMarksCourse.forEach((item, index: number) => {
        markCoursesDto.push({
          valutazioniCorsiId: null,
          corso: Number(this.id),
          criterio: index,
          valore: item
        });
      });

      if (this.sendCoursesWithoutAlert === false) {
        this.request = this.markCourseService.insert(markCoursesDto).subscribe((resp: BaseResponseDto<string>) => {
          if (resp.status === 200 && resp.success) {
            this.messageService.add({
              severity: 'success',
              summary: 'Operazione riuscita!',
              detail: 'Valutazioni ' + (resp.response === 'OK' ? 'aggiunte' : 'modificate') + ' con successo'
            });
            if (resp.response === 'OK') {
              typeof this.course.dataErogazione._i !== 'string' ? this.course.dataErogazione = null : this.course.dataErogazione = this.datePipe.transform(this.course.dataErogazione._i, 'yyyy-MM-dd');
              typeof this.course.dataChiusura._i !== 'string' ? this.course.dataChiusura = null : this.course.dataChiusura = this.datePipe.transform(this.course.dataChiusura._i, 'yyyy-MM-dd');
              this.coursesService.setValutazioni(this.id, this.course).subscribe((res: BaseResponseDto<String>) => {
              });
              this.operationCourse = Operation.READ;
              this.courseMarksOperation = Operation.UPDATE;
              this.course.dataChiusura = moment(this.course.dataChiusura);
              this.course.dataErogazione = moment(this.course.dataErogazione);
            }
            this.operationCourse = Operation.READ;
          } else {
            this.generateError('Erorre', 'Impossibile aggiungere la valutazione');
          }

        });
      } else {
        this.generateError('Valutazione errata', 'Correggi gli errori');
      }

    } else {
      this.generateError('Valutazione vuota', 'Non puoi salvare una valutazione vuota');
    }
  }

  showModalTeacher(id: number) {
    this.rateTeacher = true;
    this.idCourseTeacher = id;
    this.teacherMarksOperation = Operation.CREATE;

    const teacherFormGroup = {};
    this.paramTeacher.forEach(param => {
      teacherFormGroup['course-1-' + param.idCriterio] = 0;
      teacherFormGroup['course-2-' + param.idCriterio] = 0;
      teacherFormGroup['course-3-' + param.idCriterio] = 0;
      teacherFormGroup['course-4-' + param.idCriterio] = 0;
      teacherFormGroup['course-na-' + param.idCriterio] = 0;
    });
    this.teacherForm = this.fb.group(teacherFormGroup);

    this.request = this.coursesTeachersService.getById(this.idCourseTeacher).subscribe((resp: BaseResponseDto<CoursesTeachersDto>) => {
      if (resp.status === 200 && resp.success) {
        this.teacher = resp.response.showedText;
        this.courseTeacher = resp.response;
      } else {
        this.generateError('Errore', 'Impossibile rececuperare il docente.');
      }

      let fn = null;
      if (resp.response.type === 'TEACHER') {
        fn = this.marksTeachersService.getByDocente(this.id, this.courseTeacher.docenteOInternoId);
      } else if (resp.response.type === 'EMPLOYEE') {
        fn = this.marksTeachersService.getByInterno(this.id, this.courseTeacher.docenteOInternoId);
      }
      if (fn !== null) {
        this.request = fn.subscribe((result: BaseResponseDto<PageableDto<MarksTeachersDto[]>>) => {
          if (result.response !== null) {
            if (result.response.totalCount !== 0) {
              this.teacherMarksOperation = Operation.UPDATE;
            }
            result.response.data.forEach((item: MarksTeachersDto) => {
              const formItem = {};
              Object.keys(item.valore).forEach((key: string) => {
                formItem['course-' + key + '-' + item.criterio] = item.valore[key];
              });
              this.teacherForm.patchValue(formItem);
            });
          }
          this.teacherForm.valueChanges.subscribe(() => this.calcAvgs('teacher'));
          this.calcAvgs('teacher');
        });
      }
    });
  }

  sendTeacherMarks() {
    if (this.request) {
      this.request.unsubscribe();
    }
    const transformMarksTeacher = [];
    const marksTeachersDto: MarksTeachersDto[] = [];
    let empty = true;
    Object.keys(this.teacherForm.value).forEach(key => {
      const pair = key.replace('course-', '').split('-');
      if (transformMarksTeacher[pair[1]] === undefined) {
        transformMarksTeacher[pair[1]] = {};
      }
      const value = this.teacherForm.get(key).value;
      if (empty === true && value !== 0) {
        empty = false;
      }
      transformMarksTeacher[pair[1]][pair[0]] = this.teacherForm.get(key).value;
    });

    if (!empty) {
      if (this.courseTeacher.type === 'EMPLOYEE') {
        transformMarksTeacher.forEach((item, index: number) => {
          marksTeachersDto.push({
            valutazioniDocentiId: null,
            corso: this.id,
            docente: null,
            interno: this.courseTeacher.docenteOInternoId,
            criterio: index,
            valore: item
          });
        });
      } else {
        transformMarksTeacher.forEach((item, index: number) => {
          marksTeachersDto.push({
            valutazioniDocentiId: null,
            corso: this.id,
            docente: this.courseTeacher.docenteOInternoId,
            interno: null,
            criterio: index,
            valore: item
          });
        });
      }

      if (this.sendCoursesWithoutAlert === false) {
        this.request = this.marksTeachersService.insert(marksTeachersDto).subscribe((result: BaseResponseDto<string>) => {
          if (result.status === 200 && result.success) {
            if (this.teacherMarksOperation === Operation.CREATE) {
              this.messageService.add({
                severity: 'success',
                summary: 'Operazione riuscita!',
                detail: 'Valutazione aggiunta con successo'
              });
              this.lazyLoad('teachers');
              // isNaN(this.course.dataErogazione._i) ? this.course.dataErogazione = null : this.course.dataErogazione = moment(this.coursesService.convertDate(this.course.dataErogazione._i)).format('YYYY-MM-DD');
              // this.course.dataChiusura === 'Non impostata' ? this.course.dataChiusura = null : this.course.dataChiusura = moment(this.coursesService.convertDate(this.course.dataChiusura._i)).format('YYYY-MM-DD');
              typeof this.course.dataErogazione._i !== 'string' ? this.course.dataErogazione = null : this.course.dataErogazione = this.datePipe.transform(this.course.dataErogazione._i, 'yyyy-MM-dd');
              typeof this.course.dataChiusura._i !== 'string' ? this.course.dataChiusura = null : this.course.dataChiusura = this.datePipe.transform(this.course.dataChiusura._i, 'yyyy-MM-dd');
              // this.course.dataErogazione = this.course.dataErogazione._i;
              // this.course.dataChiusura = this.course.dataChiusura._i;
              this.coursesService.setValutazioni(this.id, this.course).subscribe((resp: BaseResponseDto<String>) => {
              });
              this.course.dataChiusura = moment(this.course.dataChiusura);
              this.course.dataErogazione = moment(this.course.dataErogazione);
            } else if (this.teacherMarksOperation === Operation.UPDATE) {
              this.messageService.add({
                severity: 'success',
                summary: 'Operazione riuscita!',
                detail: 'Valutazione modificata con successo'
              });
              this.lazyLoad('teachers');
            }
          } else {
            this.generateError();
          }

          this.teacherForm.reset();
        });
        this.rateTeacher = false;
      } else {
        this.generateError('Valutazione errata', 'Correggi gli errori');
      }

    } else {
      this.generateError('Valutazione vuota', 'Non puoi salvare una valutazione vuota');
    }
  }

  showModalUser(id: number) {
    this.userMarksOperation = Operation.CREATE;
    this.rateUser = true;
    this.idCourseUser = id;
    this.request = this.coursesUsersService.getById(this.idCourseUser).subscribe((resp: BaseResponseDto<CoursesUsersDto>) => {
      if (resp.status === 200 && resp.success) {
        this.user = resp.response.showedText;
        this.courseUser = resp.response;
      } else {
        this.generateError();
      }
      this.request = this.marksUsersService.getByCourseAndEmployee(this.id, resp.response.dipendente).subscribe((result: BaseResponseDto<PageableDto<MarksUsersDto[]>>) => {
        if (result.status === 200 && result.success) {
          const userFormGroup = {};
          this.paramUser.forEach(param => {
            userFormGroup['course-' + param.idCriterio] = 0;
          });
          this.userForm = this.fb.group(userFormGroup);
          this.calcFinalAvgs('user');
          this.userForm.valueChanges.subscribe(() => {
            this.calcFinalAvgs('user');
          });
          this.userNew = true;

          if (result.response.totalCount > 0) {
            this.userMarksOperation = Operation.UPDATE;
            const results = result.response.data;
            results.forEach(param => {
              this.markUserId.push(param.valutazioniUtentiId);
            });

            const patchUserForm = {};
            let index = 0;
            this.paramUser.forEach(param => {
              patchUserForm['course-' + param.idCriterio] = results[index].valore;
              index = index + 1;
            });
            this.userForm.patchValue(patchUserForm);
            this.userNew = false;
          }
        } else {
          this.generateError();
        }
      });
    });
  }

  sendUserMarks() {
    const marksUsersDto: MarksUsersDto[] = [];
    let error: boolean = false;

    let index: number = 0;
    let empty: boolean = true;

    Object.keys(this.userForm.controls).forEach(key => {
      const value = this.userForm.get(key).value;
      if (empty === true && value !== 0) {
        empty = false;
      }
      marksUsersDto.push({
        valutazioniUtentiId: this.userNew ? 0 : this.markUserId[index],
        corso: Number(this.id),
        dipendente: this.courseUser.dipendente,
        criterio: Number(key.replace('course-', '')),
        valore: value
      });
      if (!this.userNew) {
        index = index + 1;
      }
    });

    if (!empty) {
      if (this.userMarksOperation === Operation.CREATE) {
        if (marksUsersDto.filter(item => item.valore === 0).length === marksUsersDto.length) {
          this.messageService.add({
            severity: 'error',
            summary: 'Valutazione vuota',
            detail: 'Devi esprimere almeno una valutazione'
          });
          error = true;
        } else {
          this.request = this.marksUsersService.insert(marksUsersDto).subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Operazione riuscita!',
              detail: 'Valutazione aggiunta con successo'
            });
            typeof this.course.dataErogazione._i !== 'string' ? this.course.dataErogazione = null : this.course.dataErogazione = this.datePipe.transform(this.course.dataErogazione._i, 'yyyy-MM-dd');
            typeof this.course.dataChiusura._i !== 'string' ? this.course.dataChiusura = null : this.course.dataChiusura = this.datePipe.transform(this.course.dataChiusura._i, 'yyyy-MM-dd');
            this.coursesService.setValutazioni(this.id, this.course).subscribe((resp: BaseResponseDto<String>) => {
            });
            this.course.dataChiusura = moment(this.course.dataChiusura);
            this.course.dataErogazione = moment(this.course.dataErogazione);
            this.userForm.reset();
            this.lazyLoad('employees');
          });
        }
      } else if (this.userMarksOperation === Operation.UPDATE) {
        this.request = this.marksUsersService.update(marksUsersDto).subscribe(() => {
          this.messageService.add({
            severity: 'success',
            summary: 'Operazione riuscita!',
            detail: 'Valutazione modificata con successo'
          });
          this.lazyLoad('employees');
        });
      }

      if (!error) {
        this.markUserId = [];
        this.userNew = false;
        this.rateUser = false;
      }
    } else {
      this.generateError('Valutazione vuota', 'Non puoi salvare una valutazione vuota');
    }

  }

  closeModalUser() {
    this.userNew = false;
    this.rateUser = false;
    this.userForm.reset();
  }

  calcAvgs(table: string): void {
    this.alertMaxUsers = [];
    let sums: number[] = [];
    let counters: number[] = [];
    let countersMax: number[] = [];
    const form: FormGroup = table === 'course' ? this.courseForm : this.teacherForm;

    const reset = () => { // Reset all averages to zero
      const temp = [];
      const param = table === 'course' ? this.paramCourse : this.paramTeacher;
      for (let i = 0; i < param.length; i++) {
        temp.push(0);
      }
      if (table === 'course') {
        this.courseAverages = temp;
      } else if (table === 'teacher') {
        this.teacherAverages = temp;
      }
    };

    Object.keys(form.value).forEach(key => {
      const pair = key.replace('course-', '').split('-');
      const index = Number(pair[1]) - 1;


      if (pair[0] !== 'na') { // skip 'na' values from average
        if (sums[index] === undefined) { // initialize arrays slot
          sums[index] = 0;
          counters[index] = 0;
          countersMax[index] = 0;
        }

        if (form.value[key] !== 0) {
          sums[index] += Number(pair[0]) * form.value[key];
          counters[index] += 1 * form.value[key];
          countersMax[index] += 1 * form.value[key];
        }
      } else {
        if (sums[index] === undefined) {
          countersMax[index] = 0;
        }
        if (form.value[key] !== 0) {
          countersMax[index] += 1 * form.value[key];
        }
      }


    });

    sums = sums.filter(e => e != null);
    counters = counters.filter(e => e != null);
    countersMax = countersMax.filter(e => e != null);

    if (sums.length === 0) { // If empty, initialize averages to zero
      reset();
    } else {
      for (let i = 0; i < sums.length; i++) {
        let value: number = 0;

        if (sums[i] !== 0) {
          value = sums[i] / counters[i];
          if (isNaN(value)) {
            value = 0;
          } else {
            value = +(value.toFixed(2));
          }

        }

        if (table === 'course') {
          this.courseAverages[i] = value;
        } else if (table === 'teacher') {
          this.teacherAverages[i] = value;
        }
      }

      countersMax.forEach(element => {
        if (element > this.counterUsers) {
          this.alertMaxUsers.push(false);
        } else {
          this.alertMaxUsers.push(true);
        }
      });

      this.sendCoursesWithoutAlert = false;
      this.alertMaxUsers.forEach(item => {
        if (item === false) {
          this.sendCoursesWithoutAlert = true;
        }
      });
    }

    this.calcFinalAvgs(table);
  }

  calcFinalAvgs(table): void {
    let array = [];
    if (table === 'course') {
      array = this.courseAverages;
    } else if (table === 'teacher') {
      array = this.teacherAverages;
    } else if (table === 'user') {
      Object.keys(this.userForm.value).forEach(key => {
        array.push(this.userForm.value[key]);
      });
    }

    if (array !== null) {
      const sum: number = array.reduce((sumR, avg) => sumR + avg);
      const count: number = array.filter(avg => avg !== 0).length;
      const avgFinal = count > 0 ? +((sum / count).toFixed(2)) : 0;

      if (table === 'course') {
        this.courseAverage = avgFinal;
      } else if (table === 'teacher') {
        this.teacherAverage = avgFinal;
      } else if (table === 'user') {
        this.userAverage = avgFinal;
      }
    }
  }

  resultRate(value: number) {
    switch (value) {
      case 1: return 'Totalmente insufficente';
      case 2: return 'Insufficiente';
      case 3: return 'Sufficiente';
      case 4: return 'Dicreto';
      case 5: return 'Buono';
      case 6: return 'Ottimo';
    }
  }
}
