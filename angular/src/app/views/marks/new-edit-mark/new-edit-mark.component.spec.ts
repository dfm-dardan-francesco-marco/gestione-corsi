import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEditMarkComponent} from './new-edit-mark.component';

describe('NewEditMarkComponent', () => {
  let component: NewEditMarkComponent;
  let fixture: ComponentFixture<NewEditMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEditMarkComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
