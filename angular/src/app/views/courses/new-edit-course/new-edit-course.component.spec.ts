import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEditCourseComponent} from './new-edit-course.component';

describe('NewEditCourseComponent', () => {
  let component: NewEditCourseComponent;
  let fixture: ComponentFixture<NewEditCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEditCourseComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
