import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CoursesDictionariesDto, CoursesDto, CoursesService, MeasuresDto, TypologiesDto} from '../../../services/courses.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseResponseDto} from '../../../interfaces/baseResponseDto';
import {DatePipe} from '@angular/common';
import {SearchListType, SearchService, SearchType} from '../../../services/search.service';
import {ActionBtn, Search} from '../../../containers/advanced-table/advanced-table.component';
import {CoursesTeachersDto, CoursesTeachersService, CourseTeachersSuggestionDto} from '../../../services/courses-teachers.service';
import {PageableDto} from '../../../interfaces/pageableDto';
import * as moment from 'moment';
import {CoursesUsersDto, CoursesUsersService, CourseUsersSuggestionDto} from '../../../services/courses-users.service';
import {EServiceResponse} from '../../../enums/EServiceResponse';
import {ServiceResponse} from '../../../interfaces/serviceResponse';
import {CompaniesDto, CompaniesService} from '../../../services/companies.service';
import {ErrorsDto} from '../../../interfaces/errorsDto';
import {EmployeesService} from '../../../services/employees.service';
import {TeachersService} from '../../../services/teachers.service';
import {MarksTeachersService} from '../../../services/marks-teachers.service';
import {MarksUsersService} from '../../../services/marks-users.service';

enum Operation {
  CREATE,
  READ,
  UPDATE
}

export interface TableTypes {
  rowID: string;
  columns: any[];
  rows: any[];
  actions: ActionBtn[];
  rowClickable: boolean;
  search: Search;
  loading: boolean;
  totalRecords: number;
  resultsPerPage: number;
  exportAsCSV: boolean;
  enableChangeResultsPerPage: boolean;
  sortable: boolean;
  suggestions: any;
  selected: any;
  lastLazyLoad: LazyLoadEvent;
  request: any;
  page: number;
}

@Component({
  selector: 'app-new-edit-course',
  templateUrl: './new-edit-course.component.html',
  styleUrls: ['./new-edit-course.component.scss'],
  providers: [DatePipe]
})

export class NewEditCourseComponent implements OnInit {

  readonly Operation = Operation;
  readonly today: Date = new Date();

  request: any = null;
  loading: boolean = false;
  operation: Operation = Operation.CREATE;
  course: CoursesDto = null;
  form: FormGroup;
  id: number = null;
  displaySidebar: boolean = false;
  editLengthForm: FormGroup;
  item: string;

  dictionaries: CoursesDictionariesDto = null;
  tipologia: TypologiesDto = null;
  misura: MeasuresDto = null;

  previsto: boolean = true;
  erogato: boolean = false;
  dataEro: Date;
  dataChi: Date;
  dataCen: Date = new Date();
  societa: string;
  tipo: string;
  marks: number = 0;

  allCompanies: CompaniesDto[] = null;
  company: CompaniesDto;
  companies: CompaniesDto[];

  editEmployee: CoursesUsersDto = null;

  submitted = false;
  deleted: any;
  displayModal: boolean = false;
  deleteDef: boolean = false;
  restoreCourse: boolean = false;

  readonly tables: { [k: string]: TableTypes } = {
    'teachers': {
      rowID: 'corsiDocentiId',
      columns: [
        {
          field: 'showedText',
          header: 'Docente'
        },
        {
          field: 'type',
          header: 'Tipo'
        }
      ],
      rows: [],
      actions: [
        {
          id: 'delete',
          title: 'Rimuovi',
          icon: 'pi-trash',
          class: ['btn-danger', 'text-white']
        }
      ],
      rowClickable: true,
      search: {
        enabled: false,
        placeholder: 'Cerca nei docenti'
      },
      loading: true,
      totalRecords: 0,
      resultsPerPage: null,
      exportAsCSV: true,
      enableChangeResultsPerPage: false,
      sortable: false,
      suggestions: null,
      selected: [],
      lastLazyLoad: null,
      request: null,
      page: 1
    },
    'employees': {
      rowID: 'corsiUtentiId',
      columns: [
        {
          field: 'showedText',
          header: 'Dipendente'
        },
        {
          field: 'durata',
          header: 'Durata'
        }
      ],
      rows: [],
      actions: [
        {
          id: 'edit',
          title: 'Modifica durata',
          icon: 'pi-pencil',
          class: ['btn-warning', 'text-white']
        },
        {
          id: 'delete',
          title: 'Rimuovi',
          icon: 'pi-trash',
          class: ['btn-danger', 'text-white']
        }
      ],
      rowClickable: true,
      search: {
        enabled: true,
        placeholder: 'Cerca nei dipendenti'
      },
      loading: true,
      totalRecords: 0,
      resultsPerPage: null,
      exportAsCSV: true,
      enableChangeResultsPerPage: false,
      sortable: false,
      suggestions: null,
      selected: [],
      lastLazyLoad: null,
      request: null,
      page: 1
    }
  };

  @ViewChild('teachers') teachersRef: ElementRef;
  @ViewChild('employees') employeesRef: ElementRef;
  @ViewChild('durata') durataRef: ElementRef;

  constructor(
    private fb: FormBuilder,
    private coursesService: CoursesService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private searchService: SearchService,
    private coursesTeachersService: CoursesTeachersService,
    private coursesUsersService: CoursesUsersService,
    private confirmationService: ConfirmationService,
    private companiesService: CompaniesService,
    private router: Router,
    private employeesService: EmployeesService,
    private teachersService: TeachersService,
    private marksTeachersService: MarksTeachersService,
    private marksUsersService: MarksUsersService
  ) {
  }

  get f() {
    return this.form.controls;
  }

  ngOnInit() {
    this.form = this.fb.group({
      tipo: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      tipologia: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      descrizione: [null, [
        Validators.maxLength(200),
      ]],
      edizione: [null, [
        Validators.maxLength(100),
      ]],
      previsto: [this.previsto, [
        Validators.maxLength(100),
        Validators.required
      ]],
      erogato: [this.erogato, [
        Validators.maxLength(100),
        Validators.required
      ]],
      durata: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      misura: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      note: [null, [
        Validators.maxLength(100),
      ]],
      luogo: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      ente: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      societa: [null, [
        Validators.maxLength(100),
        Validators.required
      ]],
      dataErogazione: [this.dataEro, [
        Validators.maxLength(100),
      ]],
      dataChiusura: [this.dataChi, [
        Validators.maxLength(100),
      ]],
      dataCensimento: [this.dataCen, [
        Validators.maxLength(100),
      ]],
      interno: [0, [
        Validators.maxLength(100),
        Validators.required
      ]],
      valutazioni: [
        0
      ]
    });

    this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.dictionaries = resp.response.data;
        if (this.tipologia === null) {
          this.tipologia = this.dictionaries.tipologie[0];
        }
        if (this.misura === null) {
          this.misura = this.dictionaries.misure[0];
        }
        this.route.params.subscribe(values => {
          this.route.data.subscribe(data => {
            if (data.read) {
              this.operation = Operation.READ;
              this.id = values.id_r;
            } else if (data.edit) {
              this.operation = Operation.UPDATE;
              this.id = values.id_u;
            } else if (data.create) {
              this.operation = Operation.CREATE;
            }

            if (this.operation === Operation.READ) {
              this.form.controls['tipologia'].disable();
              this.form.controls['tipo'].disable();
              this.form.controls['misura'].disable();

              this.form.controls['previsto'].disable();
              this.form.controls['erogato'].disable();
              this.form.controls['interno'].disable();

              this.form.controls['dataErogazione'].disable();
              this.form.controls['dataChiusura'].disable();
              this.form.controls['dataCensimento'].disable();
            }

            if (this.operation === Operation.READ || this.operation === Operation.UPDATE) {
              this.coursesService.getById(this.id).subscribe((respCourse: BaseResponseDto<CoursesDto>) => {
                if (respCourse.status === 200 && respCourse.success && respCourse.response !== null) {
                  this.companiesService.list().subscribe((respC: BaseResponseDto<CompaniesDto[]>) => {
                    if (respC.status === 200 && respC.success) {
                      this.allCompanies = respC.response;

                      this.tipologia = this.dictionaries.tipologie.find(item => item.tipologiaId === respCourse.response.tipologia);
                      this.misura = this.dictionaries.misure.find(item => item.misuraId === respCourse.response.misura);
                      this.company = this.allCompanies.find(item => item.societaId === respCourse.response.societa);

                      this.tipo = this.dictionaries.tipi.find(item => item.tipoId === respCourse.response.tipo).descrizione;
                      this.societa = this.company.ragioneSociale;

                      this.course = respCourse.response;
                      this.deleted = respCourse.response.trashed;
                      this.previsto = respCourse.response.previsto;
                      this.erogato = respCourse.response.erogato;
                      this.dataEro = respCourse.response.dataErogazione;
                      this.dataChi = respCourse.response.dataChiusura;
                      this.dataCen = respCourse.response.dataCensimento;
                      this.marks = respCourse.response.valutazioni;

                      this.form.patchValue({
                        tipo: respCourse.response.tipo,
                        descrizione: respCourse.response.descrizione,
                        edizione: respCourse.response.edizione,
                        previsto: respCourse.response.previsto,
                        erogato: respCourse.response.erogato,
                        durata: respCourse.response.durata,
                        note: respCourse.response.note,
                        luogo: respCourse.response.luogo,
                        ente: respCourse.response.ente,
                        dataErogazione: respCourse.response.dataErogazione ? new Date(respCourse.response.dataErogazione) : null,
                        dataChiusura: respCourse.response.dataChiusura ? new Date(respCourse.response.dataChiusura) : null,
                        dataCensimento: respCourse.response.dataCensimento ? new Date(respCourse.response.dataCensimento) : null,
                        interno: respCourse.response.interno,
                        valutazioni: respCourse.response.valutazioni
                      });

                      this.searchService.push(SearchListType.QUICK_ACCESS, {
                        id: this.id,
                        type: SearchType.COURSE,
                        firstRow: this.tipologia.descrizione + ' - ' + this.dictionaries.tipi.find(item => item.tipoId === respCourse.response.tipo)?.descrizione + (this.dataEro ? ' (' + moment(this.dataEro).format('DD/MM/YYYY') + ')' : '')
                      });
                    } else {
                      this.generateError('Impossibile caricare società', 'Non siamo riusciti a caricare la lista delle società');
                    }
                  });
                } else {
                  this.course = null;
                  this.generateError('Impossibile caricare il corso', 'Il corso scelto non esiste');
                }
              });
            }
          });
        });
      } else {
        this.generateError('Impossibile recuperare dizionari', 'Tipologie, tipi e misure potrebbero non funzionare');
      }

      if (this.allCompanies === null) {
        this.companiesService.list().subscribe((respC: BaseResponseDto<CompaniesDto[]>) => {
          if (respC.status === 200 && respC.success) {
            this.allCompanies = respC.response;
          } else {
            this.generateError('Impossibile caricare società', 'Non siamo riusciti a caricare la lista delle società');
          }
        });
      }
    });
  }

  filterCompanies(event: any): void {
    this.companies = this.allCompanies.filter(company =>
      company.ragioneSociale.trim().toLowerCase().includes(event.query.trim().toLowerCase())
    );
  }

  takeAction(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      this.messageService.add({
        severity: 'error',
        summary: 'Errori nel modulo',
        detail: 'Controlla e correggi gli errori segnalati'
      });
      return;
    } else {
      if (this.request) {
        this.request.unsubscribe();
      }

      let fn = null;

      const values = this.form.value;
      values.tipologia = this.tipologia.tipologiaId;
      values.misura = this.misura.misuraId;

      values.dataErogazione = values.dataErogazione !== null ? moment(values.dataErogazione).format('YYYY-MM-DD') : null;
      values.dataChiusura = values.dataChiusura !== null ? moment(values.dataChiusura).format('YYYY-MM-DD') : null;
      values.dataCensimento = values.dataCensimento !== null ? moment(values.dataCensimento).format('YYYY-MM-DD') : null;
      values.societa = this.company.societaId;


      if (values.dataErogazione > values.dataChiusura) {
        this.generateError('Date sbagliate', 'La data di chiusura non può avvenire prima della data di erogazione');
      } else {
        if (this.operation === Operation.CREATE) {
          fn = this.coursesService.add(values);
        } else if (this.operation === Operation.UPDATE) {
          fn = this.coursesService.update(this.id, values);
        }

        if (fn) {
          this.request = fn.subscribe((resp: BaseResponseDto<ServiceResponse<number>>) => {
            if (resp.status === 200 && resp.success) {
              this.messageService.add({
                severity: 'success',
                summary: 'Operazione riuscita!',
                detail: 'Corso ' + (this.operation === Operation.CREATE ? 'aggiunto' : 'modificato')
              });
              if (this.operation === Operation.CREATE) {
                if (resp.response.data > 0) {
                  this.router.navigate(['courses', 'edit', resp.response.data]);
                } else {
                  this.form.reset();
                  this.form.patchValue({
                    previsto: 1,
                    erogato: 0,
                    interno: 0
                  });
                }
              }
            } else {
              this.messageService.add({
                severity: 'error',
                summary: 'Società non valida'
              });
            }
          });
        }
      }
    }
  }

  changeStatus(x: string) {
    this.form.patchValue({
      erogato: !this.erogato,
      previsto: !this.previsto
    });
    this.erogato = !this.erogato;
    this.previsto = !this.previsto;
  }

  searchFn(table: string, value: string): void {
    this.tables[table].search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(table, 1);
  }

  private getRestFun(table: string, event: LazyLoadEvent): any {
    let search = null;
    let page = this.tables[table].page;
    if (this.tables[table].search.defaultValue !== undefined && this.tables[table].search.defaultValue !== null) {
      search = this.tables[table].search.defaultValue;
      page = 1;
    }

    if (table === 'teachers') {
      return this.coursesTeachersService.getByCourseId(
        this.id,
        page,
        this.tables[table].resultsPerPage,
        event.sortField,
        event.sortOrder,
        search
      );
    } else if (table === 'employees') {
      return this.coursesUsersService.getByCourseId(
        this.id,
        page,
        this.tables[table].resultsPerPage,
        event.sortField,
        event.sortOrder,
        search
      );
    }

    return null;
  }

  lazyLoad(table: string, event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.tables[table].lastLazyLoad;
    }

    this.tables[table].loading = true;
    this.tables[table].lastLazyLoad = event;

    this.tables[table].request?.unsubscribe();

    const fn = this.getRestFun(table, event);
    if (fn !== null) {
      this.tables[table].request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesTeachersDto[] | CoursesUsersDto[]>>) => {
        this.coursesService.setValutazioni(this.id, this.course).subscribe((res: BaseResponseDto<String>) => {

        });
        this.tables[table].loading = false;

        if (resp.status === 200 && resp.success && resp.response !== null) {
          if (resp.response.data !== null) {
            if (table === 'teachers') {
              resp.response.data.forEach(item => {
                item.type = item.type === 'TEACHER' ? 'Docente' : 'Dipendente';
              });
            }
          }
          this.tables[table].rows = resp.response.data;
          this.tables[table].totalRecords = resp.response.totalCount;
          this.tables[table].resultsPerPage = resp.response.resultsPerPage;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        } else {
          this.generateError('Impossibile caricare i ' + (table === 'teachers' ? 'docenti' : 'dipendenti'));
        }
      });
    } else {
      this.tables[table].loading = false;
    }
  }

  changeResultsPerPage(table: string, number: number): void {
    this.tables[table].resultsPerPage = number;
    this.pageChanged(table, 1);
  }

  actionBtnClicked(table: string, pair: any): void {
    const row = pair.row;
    const action = pair.action;

    const item = this.tables[table].rows.find(current =>
      current.corsiDocentiId === row || current.corsiUtentiId === row
    );

    let fn = null;
    if (table === 'teachers') {
      fn = this.coursesTeachersService.deleteTeacherOrEmployeeById(item.corsiDocentiId);
    } else if (table === 'employees') {
      fn = this.coursesUsersService.deleteEmployeeById(item.corsiUtentiId);
    }

    if (item !== null && fn !== null) {
      switch (action) {
        case 'edit':
          this.editEmployee = item;
          this.editLengthForm = this.fb.group({length: item.durata});
          this.displaySidebar = true;
          break;
        case 'delete':
          this.confirmationService.confirm({
            header: 'Confermare eliminazione',
            icon: 'pi pi-exclamation-triangle',
            message: 'Sei sicuro che vuoi scollegare il ' + (table === 'teachers' ? 'docente' : 'dipendente') + ' <b>' + (item.showedText) + '</b> da questo corso?',
            acceptLabel: 'Conferma',
            rejectLabel: 'Annulla',
            defaultFocus: 'reject',
            accept: () => {
              fn.subscribe((resp: BaseResponseDto<string>) => {
                if (resp.status === 200 && resp.success) {
                  this.messageService.add({
                    severity: 'success',
                    summary: (table === 'teachers' ? 'Docente' : 'Dipendente') + ' scollegato',
                    detail: 'Il ' + (table === 'teachers' ? 'docente' : 'dipendente') + ' ' + item.showedText + ' è stato scollegato'
                  });
                  if (table === 'teachers') {
                    this.marksTeachersService.deleteByCorsoAndInternoOrDocente(this.course.corsoId,
                      item.type === 'Docente' ? item.docenteOInternoId : 0, item.type === 'Docente' ? 0 : item.docenteOInternoId).subscribe();
                  } else {
                    this.marksUsersService.deleteByCorsoAndDipendente(this.course.corsoId, item.dipendente).subscribe();
                  }
                  this.lazyLoad(table);
                } else {
                  if (resp.response === 'DATA_INTEGRITY_ERROR') {
                    this.generateError('Impossibile eliminare', 'Il docente che vuoi scollegare sta svolgendo un corso');
                  }
                  if (resp.response === 'DELETE_FAILED') {
                    this.generateError('Impossibile eliminare', 'Non sono riuscito a scollegare il ' + (table === 'teachers' ? 'docente' : 'dipendente') + '');
                  }
                  if (resp.response === 'EMPTY_ID') {
                    this.generateError('Impossibile scollegare', 'Nessun ' + (table === 'teachers' ? 'docente' : 'dipendente') + ' selezionato');
                  }
                  if (resp.response === 'EMPTY_DTO') {
                    this.generateError('Impossibile scollegare', 'Il ' + (table === 'teachers' ? 'docente' : 'dipendente') + ' da scollegare non è stato passato al DTO');
                  }
                }
                this.coursesService.setValutazioni(this.id, this.course).subscribe((res: BaseResponseDto<String>) => {

                });
              });
            }
          });
          break;
      }
    }
  }

  generateError(text: string = null, detail: string = null) {
    this.messageService.add({
      severity: 'error',
      summary: text || 'Si &egrave; verificato un errore',
      detail
    });
  }

  suggest(table: string, value: any) {
    value = value.target.value.trim();

    if (value === '') {
      this.tables[table].suggestions = null;
    } else if (value.length >= 3) {
      let fn = null;
      if (table === 'teachers') {
        fn = this.coursesTeachersService.getTeachersAndEmployees(value);
      } else if (table === 'employees') {
        fn = this.coursesUsersService.getEmployees(value);
      }

      if (fn !== null) {
        fn.subscribe((resp: BaseResponseDto<CourseTeachersSuggestionDto[] | CourseUsersSuggestionDto[]>) => {
          if (resp.status === 200 && resp.success && resp.response !== null) {
            this.tables[table].suggestions = resp.response;
          } else {
            this.generateError('Lista docenti suggeriti non caricata');
          }
        });
      }
    }
  }

  durataBlur(): void {
    this.durataRef.nativeElement.value = Math.abs(Number(this.durataRef.nativeElement.value));
  }

  toggleSelected(table: string, index: number, fromSuggestion: boolean = false): void {
    const suggestionIndex = fromSuggestion ? index : this.tables[table].selected.findIndex(item => {
      if (item.dipendente !== 0) {
        return item.dipendente === this.tables[table].suggestions[index].dipendente;
      }
      if (item.docente !== 0) {
        return item.docente === this.tables[table].suggestions[index].docente;
      }
      return false;
    });

    if (suggestionIndex === -1) {
      const selected = this.tables[table].suggestions[index];
      if (table === 'employees') {
        selected.durata = this.durataRef.nativeElement.value || 1;
      }
      this.tables[table].selected.push(selected);
      this.tables[table].suggestions = null;
      if (table === 'teachers') {
        this.teachersRef.nativeElement.focus();
        this.teachersRef.nativeElement.value = '';
      } else if (table === 'employees') {
        this.employeesRef.nativeElement.focus();
        this.employeesRef.nativeElement.value = '';
      }
    } else {
      this.tables[table].selected.splice(suggestionIndex, 1);
    }
  }

  pair(table: string) {
    let fn = null;
    if (table === 'teachers') {
      fn = this.coursesTeachersService.pairTeachersAndEmployees(this.id, this.tables[table].selected);
    } else if (table === 'employees') {
      const errors = [];
      this.tables[table].selected.forEach((employee) => {
        if (employee.durata > this.course.durata) {
          errors.push(employee.visualizza);
        }
      });

      if (errors.length > 0) {
        this.messageService.add({
          severity: 'error',
          summary: 'Abbinamento fallito',
          detail: 'La durata di ' + errors.length + ' docent' + (errors.length === 1 ? 'e' : 'i') + ' supera la durata del corso:\n' + errors.join('\n')
        });
      } else {
        fn = this.coursesUsersService.pairEmployees(this.id, this.tables[table].selected);
      }
    }

    if (fn !== null) {
      fn.subscribe((resp: BaseResponseDto<Array<ErrorsDto>>) => {
        resp.response.forEach(value => {
          if (value.descrizione != null) {
            if (table === 'employees') {
              this.employeesService.getById(value.id).subscribe(value1 => {
                this.item = value1.response.nome + ' ' + value1.response.cognome;
                this.generateError(this.item + ' ' + value.descrizione);
              });
            } else {
              if (value.esterno !== 0) {
                this.teachersService.getById(value.esterno).subscribe(value1 => {
                  this.item = value1.response.ragioneSociale;
                  this.generateError(this.item + ' ' + value.descrizione);
                });
              } else {
                this.employeesService.getById(value.id).subscribe(value1 => {
                  this.item = value1.response.nome + ' ' + value1.response.cognome;
                  this.generateError(this.item + ' ' + value.descrizione);
                });
              }
            }
          }
        });
        this.lazyLoad(table);
        this.tables[table].selected = [];
        this.tables[table].suggestions = null;

        if (table === 'employees') {
          this.durataRef.nativeElement.value = '';
        }
      }, (e) => this.generateError((table === 'teachers' ? 'Docenti' : 'Dipendenti') + ' non abbinati'));
    }
  }

  changeLength(): void {
    const dto: CoursesUsersDto = {
      corsiUtentiId: 0,
      corso: 0,
      dipendente: 0,
      durata: this.editLengthForm.value.length,
      showedText: null
    };

    dto.durata = Math.abs(Number(dto.durata));

    if (dto.durata > this.course.durata) {
      this.messageService.add({
        severity: 'error',
        summary: 'Errore',
        detail: 'La durata inserita supera la durata del corso (' + this.course.durata + ')'
      });
    } else {
      this.coursesUsersService.update(this.editEmployee.corsiUtentiId, dto).subscribe((resp: BaseResponseDto<EServiceResponse>) => {
        if (resp.status === 200 && resp.success) {
          this.messageService.add({
            severity: 'success',
            summary: 'Durata modificata'
          });
          this.lazyLoad('employees');
          this.displaySidebar = false;
        } else {
          this.generateError('Aggiornamento durata fallito');
        }
      }, () => this.generateError('Aggiornamento durata fallito'));
    }
  }

  pageChanged(table: string, page: number) {
    this.tables[table].page = page;
    this.lazyLoad(table);
  }

  rowClicked(table: string, event: any): void {
    if (this.tables[table]) {
      const item = this.tables[table].rows.find(curr => curr[this.tables[table].rowID] === event);

      if (item) {
        let redirect = null;
        if (table === 'teachers') {
          redirect = [item.type === 'Docente' ? 'teachers' : 'employees', item.docenteOInternoId];
        } else if (table === 'employees') {
          redirect = [table, item.dipendente];
        }

        if (redirect) {
          this.router.navigate(redirect);
        }
      }
    }
  }

  showDialog() {
    this.displayModal = true;
  }

  deleteCourse() {
    this.coursesService.setTrashed(this.id, this.course, true).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['courses']);
        }
        if (resp.error) {
          if (resp.response === 'DATA_INTEGRITY_ERROR') {
            this.generateError('Impossibile eliminare', 'Per cestinare un corso, devi prima eliminare il docente e gli studenti');
          }
          if (resp.response === 'DELETE_FAILED') {
            this.generateError('Impossibile eliminare', 'Non sono riuscito a cestinare il corso');
          }
          if (resp.response === 'EMPTY_ID') {
            this.generateError('Impossibile eliminare', 'Nessun corso selezionato');
          }
          if (resp.response === 'EMPTY_DTO') {
            this.generateError('Impossibile eliminare', 'Il corso da cestinare non è stato passato al DTO');
          }
          if (resp.response === 'UPDATE_FAILED') {
            this.generateError('Impossibile modificare', 'Questo corso non può essere modificato:(');
          }
        }
      }
    }, () => {
      this.generateError('Errore rimozione', 'Non sono riuscito a cestinare il corso');
    });
  }


  deleteDefinitively() {
    this.deleteDef = true;
  }

  yesDeleteDefinitively() {
    this.coursesService.deleteById(this.id).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['', 'trashbin', 'courses']);
        }
        if (resp.error) {
          if (resp.response === 'DATA_INTEGRITY_ERROR') {
            this.generateError('Impossibile eliminare', 'Per eliminare un corso, devi prima eliminare il docente e gli studenti');
          }
          if (resp.response === 'DELETE_FAILED') {
            this.generateError('Impossibile eliminare', 'Non sono riuscito a rimuovere il corso');
          }
          if (resp.response === 'EMPTY_ID') {
            this.generateError('Impossibile eliminare', 'Nessun corso selezionato');
          }
          if (resp.response === 'EMPTY_DTO') {
            this.generateError('Impossibile eliminare', 'Il corso da eliminare non è stato passato al DTO');
          }
          if (resp.response === 'UPDATE_FAILED') {
            this.generateError('Impossibile modificare', 'Questo corso non può essere modificato:(');
          }
        }
      }
      this.deleteDef = false;
    }, () => {
      this.deleteDef = false;
      this.generateError('Errore rimozione', 'Non sono riuscito a rimuovere il corso');
    });
  }

  restore() {
    this.restoreCourse = true;
  }

  yesRestore() {
    this.coursesService.setTrashed(this.id, this.course, false).subscribe((resp: BaseResponseDto<boolean | string>) => {
      if (resp.status === 200) {
        if (resp.success) {
          this.router.navigate(['', 'trashbin', 'courses']);
        }
        if (resp.error) {
          this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il corso');
        }
      } else {
        this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il corso');
      }
    }, () => {
      this.generateError('Errore rimozione', 'Non sono riuscito a ripristinare il corso');
    });
  }
}
