import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CoursesRoutingModule} from './courses-routing.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../modules/shared.module';
import {CoursesComponent} from './courses.component';
import {NewEditCourseComponent} from './new-edit-course/new-edit-course.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {CalendarModule} from 'primeng/calendar';
import {SelectButtonModule} from 'primeng/selectbutton';
import {KeyFilterModule} from 'primeng/keyfilter';
import {DialogModule} from 'primeng/dialog';
import {ProgressBarModule} from 'primeng/progressbar';
import {MessageModule} from 'primeng/message';

@NgModule({
  imports: [
    FormsModule,
    CoursesRoutingModule,
    CommonModule,
    SharedModule,
    InputSwitchModule,
    InputTextareaModule,
    CalendarModule,
    SelectButtonModule,
    KeyFilterModule,
    DialogModule,
    ProgressBarModule,
    MessageModule
  ],
  declarations: [
    CoursesComponent,
    NewEditCourseComponent
  ]
})
export class CoursesModule {
}
