import {Component, OnInit} from '@angular/core';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../services/courses.service';
import {ActionBtn, DynamicColumns, HeaderBtn, Position, Search} from '../../containers/advanced-table/advanced-table.component';
import {Router} from '@angular/router';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {PageableDto} from '../../interfaces/pageableDto';
import {ServiceResponse} from '../../interfaces/serviceResponse';
import {DatePipe} from '@angular/common';
import {OrderService} from '../../services/order.service';
import * as moment from 'moment';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  providers: [DatePipe]
})
export class CoursesComponent implements OnInit {

  loading: boolean = true;
  courses: CoursesDto[] = null;
  errors: string[] = [];
  request: any = null;
  totalRecords: number;
  resultsPerPage: number = null;
  visible: boolean = true;
  lastLazyLoad: LazyLoadEvent = null;
  dictionaries: CoursesDictionariesDto = null;
  page: number = 1;

  columns: DynamicColumns = {
    enabled: true,
    columns: [
      {
        field: 'tipologia',
        header: 'Tipologia',
        visible: true
      },
      {
        field: 'tipo',
        header: 'Tipo',
        visible: true
      },
      {
        field: 'luogo',
        header: 'Luogo',
        visible: true
      },
      {
        field: 'ente',
        header: 'Ente',
        visible: true
      },
      {
        field: 'edizione',
        header: 'Edizione',
        visible: false
      },
      {
        field: 'dataErogazione',
        header: 'Data Erogazione',
        visible: true
      },
      {
        field: 'dataChiusura',
        header: 'Data Chiusura',
        visible: false
      },
      {
        field: 'dataCensimento',
        header: 'Data Censimento',
        visible: false
      },
    ]
  };

  actions: ActionBtn[] = [
    {
      id: 'open',
      title: 'Visualizza',
      icon: 'pi-eye'
    },
    {
      id: 'edit',
      title: 'Modifica',
      icon: 'pi-pencil',
      class: ['btn-warning', 'text-white']
    },
    {
      id: 'rate',
      title: 'Valuta',
      icon: 'pi-check',
      class: ['btn-success', 'text-white']
    },
    {
      id: 'delete',
      title: 'Elimina',
      icon: 'pi-trash',
      class: ['btn-danger', 'text-white']
    }
  ];

  search: Search = {
    enabled: true,
    placeholder: 'Cerca nei corsi...'
  };

  headersBtns: HeaderBtn[] = [
    {
      id: 'insert',
      title: 'Inserisci',
      icon: 'pi-plus',
      class: ['btn-success', 'text-white', 'float-left'],
      position: Position.LEFT
    },
    {
      id: 'types',
      title: 'Tipi',
      icon: 'pi-palette',
      class: ['btn-primary', 'text-white', 'float-left'],
      position: Position.LEFT
    }
  ];

  constructor(
    private coursesService: CoursesService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private orderService: OrderService
  ) {
  }

  searchFn(value: string): void {
    this.search.defaultValue = value.trim().length === 0 ? null : value.trim();
    this.pageChanged(1);
  }

  ngOnInit(): void {
    this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.dictionaries = resp.response.data;
      }
    });
  }

  generateError(text: string = null, tooltip: boolean = false, detail: string = null) {
    if (tooltip) {
      this.messageService.add({
        severity: 'error',
        summary: text || 'Si &egrave; verificato un errore',
        detail
      });
    } else {
      this.errors.push(text || 'Si &egrave; verificato un errore');
    }
  }

  rowClicked(row: string): void {
    this.router.navigate(['courses', row]);
  }

  private mapSortField(field: string): string {
    switch (field) {
      case 'tipo':
        return 'typesDao.descrizione';
      case 'tipologia':
        return 'typologiesDao.descrizione';
      default:
        return field;
    }
  }

  lazyLoad(event: LazyLoadEvent = null): void {
    if (event === null) {
      event = this.lastLazyLoad;
    }

    this.loading = true;
    this.lastLazyLoad = event;

    if (this.request) {
      this.request.unsubscribe();
    }

    const multiSortMeta = [];
    if (event.multiSortMeta !== null && event.multiSortMeta !== undefined) {
      event.multiSortMeta.forEach(item => {
        const temp = {...item};
        temp.field = this.mapSortField(temp.field);
        multiSortMeta.push(temp);
      });
    }

    let fn;
    if (this.search.defaultValue !== undefined && this.search.defaultValue !== null) {
      fn = this.coursesService.searchByField(
        this.search.defaultValue,
        this.page,
        this.resultsPerPage,
        this.orderService.parse(multiSortMeta)
      );
    } else {
      fn = this.coursesService.list(
        this.page,
        this.resultsPerPage,
        this.orderService.parse(multiSortMeta)
      );
      this.search.defaultValue = null;
    }

    if (fn) {
      this.request = fn.subscribe((resp: BaseResponseDto<PageableDto<CoursesDto[]>>) => {
        this.loading = false;

        if (resp.status === 200 && resp.success) {
          if (this.dictionaries !== null) {
            resp.response.data.forEach(course => {
              course.dataErogazione = course.dataErogazione !== null ? this.datePipe.transform(course.dataErogazione, 'dd/MM/yyyy') : 'Non impostata';
              course.dataChiusura = course.dataChiusura !== null ? this.datePipe.transform(course.dataChiusura, 'dd/MM/yyyy') : 'Non impostata';
              course.dataCensimento = course.dataCensimento !== null ? this.datePipe.transform(course.dataCensimento, 'dd/MM/yyyy') : 'Non impostata';
              course.tipo = this.dictionaries.tipi.find(type => type.tipoId === course.tipo).descrizione;
              course.tipologia = this.dictionaries.tipologie.find(typology => typology.tipologiaId === course.tipologia).descrizione;
            });
          }
          this.courses = resp.response.data;
          this.totalRecords = resp.response.totalCount;
          this.resultsPerPage = resp.response.resultsPerPage;
          event.rows = resp.response.resultsPerPage;
          event.first = 0;
        } else {
          this.generateError('Impossibile caricare la lista dei corsi', true);
        }
      });
    }
  }

  headerBtnClicked(action: string) {
    switch (action) {
      case 'insert':
        this.router.navigate(['courses', 'insert']);
        break;
      case 'types':
        this.router.navigate(['types']);
        break;
    }
  }

  changeResultsPerPage(number: number): void {
    this.resultsPerPage = number;
    this.pageChanged(1);
  }

  actionBtnClicked(pair: any): void {
    const corsoId = pair.row;
    const action = pair.action;

    switch (action) {
      case 'open':
        this.router.navigate(['courses', corsoId]);
        break;
      case 'edit':
        this.router.navigate(['courses', 'edit', corsoId]);
        break;
      case 'rate':
        this.router.navigate(['marks', corsoId]);
        break;
      case 'delete':
        const course = this.courses.find(corso => corso.corsoId === corsoId);
        const detail = 'Il ' + course.tipologia + ' - ' + course.tipo + ' è stato cestinato';
        this.confirmationService.confirm({
          message: 'Sei sicuro che vuoi cestinare il corso <b>' + course.tipologia + ' - ' + course.tipo + '</b>?',
          acceptLabel: 'Conferma',
          rejectLabel: 'Annulla',
          defaultFocus: 'reject',
          accept: () => {
            course.tipo = this.dictionaries.tipi.find(type => type.descrizione === course.tipo).tipoId;
            course.tipologia = this.dictionaries.tipologie.find(typology => typology.descrizione === course.tipologia).tipologiaId;
            course.dataErogazione === 'Non impostata' ? course.dataErogazione = null : course.dataErogazione = moment(this.coursesService.convertDate(course.dataErogazione)).format('YYYY-MM-DD');
            course.dataChiusura === 'Non impostata' ? course.dataChiusura = null : course.dataChiusura = moment(this.coursesService.convertDate(course.dataChiusura)).format('YYYY-MM-DD');
            course.dataCensimento === 'Non impostata' ? course.dataCensimento = null : course.dataCensimento = moment(this.coursesService.convertDate(course.dataCensimento)).format('YYYY-MM-DD');
            this.coursesService.setTrashed(course.corsoId, course, true).subscribe((resp: BaseResponseDto<boolean | string>) => {
              if (resp.status === 200) {
                if (resp.success) {
                  this.messageService.add({
                    severity: 'success',
                    summary: 'Rimozione riuscita',
                    detail
                  });
                  this.lazyLoad();
                }
                if (resp.error) {
                  if (resp.response === 'DATA_INTEGRITY_ERROR') {
                    this.generateError('Impossibile eliminare', true, 'Per cestinare un corso, devi prima eliminare il docente e gli studenti');
                  }
                  if (resp.response === 'DELETE_FAILED') {
                    this.generateError('Impossibile eliminare', true, 'Non sono riuscito a cestinare il corso');
                  }
                  if (resp.response === 'EMPTY_ID') {
                    this.generateError('Impossibile eliminare', true, 'Nessun corso selezionato');
                  }
                  if (resp.response === 'EMPTY_DTO') {
                    this.generateError('Impossibile eliminare', true, 'Il corso da cestinare non è stato passato al DTO');
                  }
                  if (resp.response === 'UPDATE_FAILED') {
                    this.generateError('Impossibile modificare', true, 'Questo corso non può essere modificato:(');
                  }
                }
              }
            }, () => {
              this.generateError('Errore rimozione', true, 'Non sono riuscito a cestinare il corso');
            });
          }

        });
        break;
    }
  }

  pageChanged(page: number) {
    this.page = page;
    this.lazyLoad();
  }

}
