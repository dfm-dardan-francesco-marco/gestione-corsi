import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesComponent} from './courses.component';
import {NewEditCourseComponent} from './new-edit-course/new-edit-course.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    data: {
      title: 'Lista corsi'
    }
  },
  {
    path: 'insert',
    component: NewEditCourseComponent,
    data: {
      title: 'Inserimento corso',
      create: true
    }
  },
  {
    path: ':id_r',
    component: NewEditCourseComponent,
    data: {
      title: 'Visualizza corso',
      read: true
    }
  },
  {
    path: ':edit/:id_u',
    component: NewEditCourseComponent,
    data: {
      title: 'Modifica corso',
      edit: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {
}
