import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SearchItem, SearchType} from '../../../services/search.service';

@Component({
  selector: 'app-search-items',
  templateUrl: './search-items.component.html',
  styleUrls: ['./search-items.component.scss']
})
export class SearchItemsComponent implements OnInit {

  readonly SearchType = SearchType;

  @Input()
  public items: SearchItem[] = [];

  @Output()
  public clickEvent: EventEmitter<SearchItem> = new EventEmitter<SearchItem>();

  constructor() {
  }

  ngOnInit(): void {
  }

}
