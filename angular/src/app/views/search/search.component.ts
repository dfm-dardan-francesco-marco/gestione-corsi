import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SearchItem, SearchListType, SearchService, SearchType} from '../../services/search.service';
import {TeacherDto, TeachersService} from '../../services/teachers.service';
import {BaseResponseDto} from '../../interfaces/baseResponseDto';
import {PageableDto} from '../../interfaces/pageableDto';
import {EmployeesDto, EmployeesService} from '../../services/employees.service';
import {CoursesDictionariesDto, CoursesDto, CoursesService} from '../../services/courses.service';
import {ServiceResponse} from '../../interfaces/serviceResponse';
import * as moment from 'moment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  searchValue: string = null;
  searchResults: SearchItem[] = [];
  recentSearches: SearchItem[] = [];
  quickAccess: SearchItem[] = [];
  observables: any[] = [];
  loading: number = 0;
  timeout: any = null;
  coursesDictionaries: CoursesDictionariesDto = null;

  constructor(
    private searchService: SearchService,
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private teachersService: TeachersService,
    private employeesService: EmployeesService,
    private coursesService: CoursesService
  ) {
  }

  @ViewChild('searchInput') searchInput: ElementRef;

  ngOnInit(): void {
    this.coursesService.dictionaries().subscribe((resp: BaseResponseDto<ServiceResponse<CoursesDictionariesDto>>) => {
      if (resp.status === 200 && resp.success) {
        this.coursesDictionaries = resp.response.data;
      }
    });

    this.searchForm = this.fb.group({
      search: ['', Validators.required]
    });

    setTimeout(() => {
      this.searchInput?.nativeElement.focus();
    }, 100);

    this.recentSearches = this.searchService.getRecentSearches();
    this.quickAccess = this.searchService.getQuickAccess();
  }

  close(): void {
    this.ref.close();
  }

  search(event: any): void {
    this.loading = 3;
    let value = event.target.value.trim();
    if (value === '') {
      value = null;
    }
    this.searchValue = value;

    this.observables.forEach(observable => {
      observable.unsubscribe();
      this.observables.shift();
    });

    if (this.timeout !== null) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }

    this.searchResults = [];
    if (value === null) {
      this.searchValue = null;
      this.loading = 0;
    } else {
      this.timeout = setTimeout(() => {
        this.observables.push(this.teachersService.searchByField(value).subscribe((resp: BaseResponseDto<PageableDto<TeacherDto[]>>) => {
          this.loading--;
          if (resp.status === 200 && resp.success) {
            resp.response.data.forEach((teacher: TeacherDto) => {
              this.searchResults.push({
                id: teacher.docenteId,
                type: SearchType.TEACHER,
                firstRow: teacher.ragioneSociale
              });
            });
          } else {

          }
        }));

        this.observables.push(this.employeesService.searchByMatricola(value).subscribe((resp: BaseResponseDto<PageableDto<EmployeesDto[]>>) => {
          this.loading--;
          if (resp.status === 200 && resp.success) {
            resp.response.data.forEach((employee: EmployeesDto) => {
              this.searchResults.push({
                id: employee.dipendenteId,
                type: SearchType.EMPLOYEE,
                firstRow: employee.cognome + ' ' + employee.nome
              });
            });
          } else {

          }
        }));

        this.observables.push(this.coursesService.searchByField(value).subscribe((resp: BaseResponseDto<PageableDto<CoursesDto[]>>) => {
          this.loading--;
          if (resp.status === 200 && resp.success) {
            resp.response.data.forEach((course: CoursesDto) => {
              const typology = this.coursesDictionaries.tipologie.find(cTypology => cTypology.tipologiaId === course.tipologia);
              const type = this.coursesDictionaries.tipi.find(cType => cType.tipoId === course.tipo);
              this.searchResults.push({
                id: course.corsoId,
                type: SearchType.COURSE,
                firstRow: (typology?.descrizione || course.tipologia) + ' - ' + (type?.descrizione || course.tipologia) + (course.dataErogazione ? ' (' + moment(course.dataErogazione).format('DD/MM/YYYY') + ')' : '')
              });
            });
          } else {

          }
        }));
      }, 1000);
    }
  }

  clickSearchResult(event: SearchItem): void {
    this.searchService.push(SearchListType.RECENT_SEARCHES, event);
    this.searchService.skipNext();
    this.close();
  }

}
