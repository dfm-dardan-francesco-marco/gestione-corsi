import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LazyLoadEvent} from 'primeng/api';
import {FormBuilder, FormGroup} from '@angular/forms';

export interface Search {
  enabled: boolean;
  placeholder?: string;
  defaultValue?: string;
}

export interface ActionBtn {
  id: string;
  title: string;
  icon: string;
  class?: string[];
}

export enum Position {
  LEFT,
  CENTER,
  RIGHT
}

export interface HeaderBtn {
  id: string;
  title: string;
  icon: string;
  position: Position;
  class?: string[];
}

export interface DynamicColumns {
  enabled: boolean;
  columns: any[];
}

export enum ColType {
  TEXT,
  PROGRESS_BAR
}

@Component({
  selector: 'app-advanced-table',
  templateUrl: './advanced-table.component.html',
  styleUrls: ['./advanced-table.component.scss']
})
export class AdvancedTableComponent implements OnInit {

  readonly Position = Position;
  readonly ColType = ColType;

  @Input()
  public rowID: string = 'id';

  @Input()
  public rowClickable: boolean = false;

  @Input()
  public loading: boolean = true;

  @Input()
  public class: string[] = [];

  /**
   * @deprecated
   */
  @Input()
  public columns: any[] = [];

  @Input()
  public dColumns: DynamicColumns = null;

  @Input()
  public rows: any = [];

  @Input()
  public search: Search = {enabled: false};

  @Input()
  public headerBtns: HeaderBtn[] = [];

  @Input()
  public actions: ActionBtn[] = [];

  @Input()
  public totalRecords: number;

  @Input()
  public resultsPerPage: number = 1;

  @Input()
  public cache: string = null;

  @Input()
  public exportAsCSV: boolean = false;

  @Input()
  public enableChangeResultsPerPage: boolean = false;

  @Input()
  public singleSort: boolean = true;

  @Input()
  public multiSort: boolean = false;

  @Output()
  public loadLazyEvent: EventEmitter<LazyLoadEvent> = new EventEmitter<LazyLoadEvent>();

  @Output()
  public rowClickedEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public actionBtnEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public headerBtnEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public changeResultsPerPageEvent: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public pageChanged: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public searchEvent: EventEmitter<string> = new EventEmitter<string>();

  public visibleColumns: any[];

  public visibleColumnsSidebar: boolean = false;

  public dColumnsForm: FormGroup = null;

  public prevEvent: any = null;

  private searchTimeout: any = null;

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.calculateVisibleColumns();
  }

  private calculateVisibleColumns(): void {
    if (this.dColumns) {
      if (this.dColumnsForm === null) {
        const group = {};
        this.dColumns.columns.forEach(column => { // Costruzione dinamica del FormGroup
          group['checkbox-' + column.field] = column.visible;
        });
        this.dColumnsForm = this.fb.group(group); // Creazione effettiva del FormGroup
        this.dColumnsForm.valueChanges.subscribe(val => { // Mi metto in ascolto per cambiamenti nelle checkbox
          this.dColumns.columns.map(column => {
            column.visible = val['checkbox-' + column.field]; // Setta il valore visible per ogni colonna
          });
          this.calculateVisibleColumns(); // Ricalcola le colonne da mostrare
        });
      }
      this.visibleColumns = this.dColumns.columns.filter(column => column.visible);
    } else {
      this.visibleColumns = this.columns;
    }
  }

  toggleColumnsSidebar(force: boolean = null): void {
    this.visibleColumnsSidebar = force || !this.visibleColumnsSidebar;
  }

  loadLazy(event: LazyLoadEvent): void {
    let emit = true;

    if (this.prevEvent !== null) {
      Object.keys(this.prevEvent).find(key => {
        if (key === 'first' && this.prevEvent[key] !== event.first) {
          emit = false;
          return;
        }
      });
    }

    this.prevEvent = event;
    if (emit) {
      this.loadLazyEvent.emit(event);
    }
  }

  rowClicked(rowID: string): void {
    if (this.rowClickable && !this.loading) {
      this.rowClickedEvent.emit(rowID);
    }
  }

  actionBtnClicked(row: string, action: string): void {
    if (!this.loading) {
      this.actionBtnEvent.emit({row, action});
    }
  }

  headerBtnClicked(action: string): void {
    if (!this.loading) {
      this.headerBtnEvent.emit(action);
    }
  }

  changeResultsPerPage(event: any): void {
    if (!this.loading) {
      this.changeResultsPerPageEvent.emit(event.target.value);
    }
  }

  onPage(event: any): void {
    const page = event.first / event.rows + 1;
    this.pageChanged.emit(page);
  }

  searchFn(event: any): void {
    if (this.searchTimeout !== null) {
      clearTimeout(this.searchTimeout);
    }

    this.loading = true;
    this.searchTimeout = setTimeout(() => {
      this.searchEvent.emit(event.target.value);
    }, 2000);
  }

}
