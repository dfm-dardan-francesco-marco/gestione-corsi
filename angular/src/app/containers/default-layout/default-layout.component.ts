import {Component} from '@angular/core';
import {navItems} from '../../_nav';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {SearchComponent} from '../../views/search/search.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent {

  public sidebarMinimized = false;
  public navItems = navItems;

  private searchDialog: DynamicDialogRef = null;

  constructor(
    public dialogService: DialogService
  ) {
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  showSearch(): void {
    if (this.searchDialog === null) {
      this.searchDialog = this.dialogService.open(SearchComponent, {
        header: 'Ricerca',
        showHeader: false,
        styleClass: 'global-search',
        closeOnEscape: true,
        baseZIndex: 1500
      });

      this.searchDialog.onDestroy.subscribe(() => {
        this.searchDialog = null;
      });
    }
  }
}
