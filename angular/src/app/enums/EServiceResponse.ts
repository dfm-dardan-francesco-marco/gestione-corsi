export enum EServiceResponse {
  OK = 1,
  CREATE_FAILED = -1,
  UPDATE_FAILED = -2,
  DELETE_FAILED = -3,
  DATA_INTEGRITY_ERROR = -4,
  EMPTY_DTO = -5,
  EMPTY_ID = -6
}
