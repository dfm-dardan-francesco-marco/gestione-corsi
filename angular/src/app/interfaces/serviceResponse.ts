import {EServiceResponse} from '../enums/EServiceResponse';

export interface ServiceResponse<T> {
  response: EServiceResponse;
  data: T;
}
