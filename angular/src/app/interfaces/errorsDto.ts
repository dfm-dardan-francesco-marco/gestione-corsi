export interface ErrorsDto {
  id: number;
  esterno: number;
  descrizione: string;
}
