export interface CountryDto {
  name: string;
  code2Alpha?: string;
}
