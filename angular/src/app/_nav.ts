import {INavData} from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'cil-speedometer'
  },
  {
    name: 'Docenti',
    url: '/teachers',
    icon: 'cil-user'
  },
  {
    name: 'Dipendenti',
    url: '/employees',
    icon: 'cil-people'
  },
  {
    name: 'Corsi',
    url: '/courses',
    icon: 'cil-bolt',
  },
  {
    name: 'Valutazioni',
    url: '/marks',
    icon: 'cil-check'
  },
  {
    name: 'Cestino',
    url: '/trashbin',
    icon: 'cil-trash',
    children: [
      {
        name: 'Docenti',
        url: '/trashbin/teachers',
        icon: 'cil-user'
      },
      {
        name: 'Corsi',
        url: '/trashbin/courses',
        icon: 'cil-bolt'
      }
    ]
  }
];
