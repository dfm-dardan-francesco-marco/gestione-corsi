import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Import Containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
import {P500Component} from './views/error/500.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },

  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'teachers',
        loadChildren: () => import('./views/teachers/teachers.module').then(m => m.TeachersModule)
      },
      {
        path: 'employees',
        loadChildren: () => import('./views/employees/employees.module').then(m => m.EmployeesModule)
      },
      {
        path: 'courses',
        loadChildren: () => import('./views/courses/courses.module').then(m => m.CoursesModule)
      },
      {
        path: 'types',
        loadChildren: () => import('./views/courses-types/courses-types.module').then(m => m.CoursesTypesModule)
      },
      {
        path: 'marks',
        loadChildren: () => import('./views/marks/marks.module').then(m => m.MarksModule)
      },
      {
        path: 'trashbin',
        loadChildren: () => import('./views/trash-bin/trash-bin.module').then(m => m.TrashBinModule)
      },
    ]
  },
  {path: '**', component: P404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
