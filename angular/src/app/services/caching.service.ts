import {Injectable} from '@angular/core';

export interface Cache<T> {
  created: Date;
  validity: Date;
  data: T;
}

@Injectable({
  providedIn: 'root'
})
export class CachingService {

  constructor() {
  }

  create<T>(data: any, minutes: number = 60): Cache<T> {
    return {
      created: new Date(),
      validity: new Date(new Date().getTime() + minutes * 60_000),
      data: data
    };
  }

  valid(cached: Cache<any>): boolean {
    return cached ? cached.validity > new Date() : false;
  }

  revalidate(cache: Cache<any>, minutes: number = 60) {
    cache.validity = new Date(new Date().getTime() + minutes * 60_000);
  }
}
