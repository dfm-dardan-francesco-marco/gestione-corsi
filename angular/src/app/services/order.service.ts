import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor() {
  }

  public parse(multiSortMeta: any): string {
    let orderString = null;
    if (multiSortMeta !== null && multiSortMeta !== undefined) {
      orderString = '';
      multiSortMeta.forEach((item, index: number) => {
        orderString += item.field + ',' + item.order;
        if (index !== multiSortMeta.length - 1) {
          orderString += ';';
        }
      });
    }
    return orderString;
  }
}
