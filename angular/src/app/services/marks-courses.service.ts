import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface MarksCoursesDto {
  valutazioniCorsiId: number;
  corso: number;
  criterio: number;
  valore: number[];
}

export interface ParametersMarksDto {
  idCriterio: number;
  descrizione: string;
  tipo: string;
}

export interface ParamsMarksDto {
  idCriterio: number;
  descrizione: string;
}


@Injectable({
  providedIn: 'root'
})
export class MarksCoursesService {
  constructor(
    private http: HttpClient
  ) {
  }

  getCourseMarks(course: number) {
    return this.http.get(environment.host + environment.endpoint.marksCourses + '?course=' + course + '&resultsPerPage=10');
  }

  getParametersMarks(): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.marksCourses + 'params');
  }

  insert(body: MarksCoursesDto[]): Observable<any> {
    return this.http.post(environment.host + environment.endpoint.marksCourses, body);
  }

  getCoursesWithMissingMark(
    page: number = 1,
    search: string = null,
    resultsPerPage: number = null,
    sort: string = null
  ): Observable<any> {
    const url = environment.host + environment.endpoint.marksCourses + 'missing' +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (search !== null ? '&field=' + search : '') +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }
}
