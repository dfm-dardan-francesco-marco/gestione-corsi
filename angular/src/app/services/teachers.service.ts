import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

export interface TeacherDto {
  docenteId: number;
  titolo: string;
  ragioneSociale: string;
  indirizzo: string;
  localita: string;
  provincia: string;
  nazione: string;
  telefono: string;
  fax: string;
  email: string;
  noteGeneriche: string;
  partitaIva: string;
  referente: string;
  trashed: any;
}

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  constructor(
    private http: HttpClient
  ) {
  }

  add(body: any): Observable<any> {
    return this.http.post(environment.host + environment.endpoint.teachers, body);
  }

  list(
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
    trashed: boolean = false
  ): Observable<any> {
    const url = environment.host + environment.endpoint.teachers +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '') + '&trashed=' + trashed;

    return this.http.get(url);
  }

  searchByField(
    ragioneSociale: string,
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
    trashed: boolean = false
  ): Observable<any> {
    const url = environment.host + environment.endpoint.teachers +
      '?field=' + ragioneSociale +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '') + '&trashed=' + trashed;

    return this.http.get(url);
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.teachers + id);
  }

  update(id: number, body: any): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.teachers + id, body);
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete(environment.host + environment.endpoint.teachers + id);
  }

  listCourses(
    teacher: number,
    page: number = 1,
    resultsPerPage: number = null,
    sortField: string = null,
    sortOrder: number = null
  ): Observable<any> {
    const url = environment.host + environment.endpoint.teachers + teacher + '/courses' +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sortField && sortOrder ? '&order=' + sortField + ',' + sortOrder : '');

    return this.http.get(url);
  }

  setTrashed(id: number, body: any, trashed: boolean): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.teachers +
      'setTrashed/' + id + '?trashed=' + trashed, body);
  }
}
