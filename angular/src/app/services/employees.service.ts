import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface EmployeesDto {
  dipendenteId: number;
  matricola: string;
  societa: number;
  cognome: string;
  nome: string;
  sesso: string;
  dataNascita: any;
  luogoNascita: string;
  statoCivile: string;
  titoloStudio: string;
  conseguitoPresso: string;
  annoConseguimento: number;
  tipoDipendente: string;
  qualifica: string;
  livello: string;
  dataAssunzione: any;
  responsabileRisorsa: string;
  responsabileArea: string;
  dataFineRapporto: any;
  dataScadenzaContratto: any;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(
    private http: HttpClient
  ) {
  }

  list(
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null
  ): Observable<any> {
    const url = environment.host + environment.endpoint.employees +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }

  employeesTeacherToo(
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null
  ): Observable<any> {
    const url = environment.host + environment.endpoint.employees + 'teachers' +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }

  searchByMatricola(
    field: any,
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
  ): Observable<any> {
    const url = environment.host + environment.endpoint.employees +
      '?field=' + field +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }

  searchByMatricolaEmployeesTeachers(
    field: any,
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
  ): Observable<any> {
    const url = environment.host + environment.endpoint.employees + '/getEmployeesTeachers' +
      '?field=' + field +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.employees + id);
  }

  listCourses(
    table: string,
    employee: number,
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
  ): Observable<any> {
    const url = environment.host + environment.endpoint.employees + employee + '/courses/' + table +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '');

    return this.http.get(url);
  }

}
