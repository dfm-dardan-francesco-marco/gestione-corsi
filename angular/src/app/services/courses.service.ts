import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface CoursesDto {
  corsoId: number;
  tipo: number | string;
  tipologia: number | string;
  descrizione: string;
  edizione: string;
  previsto: boolean;
  erogato: boolean;
  durata: number;
  misura: number;
  note: string;
  luogo: string;
  ente: string;
  societa: number;
  dataErogazione: any;
  dataChiusura: any;
  dataCensimento: any;
  interno: boolean;
  trashed: any;
  valutazioni: number;
}

export interface TypologiesDto {
  tipologiaId: number;
  descrizione: string;
}

export interface TypesDto {
  tipoId: number;
  descrizione: string;
}

export interface MeasuresDto {
  misuraId: number;
  descrizione: string;
}

export interface CoursesDictionariesDto {
  tipologie: TypologiesDto[];
  tipi: TypesDto[];
  misure: MeasuresDto[];
}

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(
    private http: HttpClient
  ) {
  }

  list(
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
    trashed: boolean = false
  ): Observable<any> {
    const url = environment.host + environment.endpoint.courses +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '') + '&trashed=' + trashed;

    return this.http.get(url);
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.courses + id);
  }

  searchByField(
    field: string,
    page: number = 1,
    resultsPerPage: number = null,
    sort: string = null,
    trashed: boolean = false
  ): Observable<any> {
    const url = environment.host + environment.endpoint.courses +
      '?field=' + field +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sort ? '&order=' + sort : '') + '&trashed=' + trashed;

    return this.http.get(url);
  }

  getIncoming(): Observable<any> {
    return this.http.get(
      environment.host + environment.endpoint.courses + 'incoming');
  }

  getOngoing(): Observable<any> {
    return this.http.get(
      environment.host + environment.endpoint.courses + 'ongoing');
  }

  getFinished(): Observable<any> {
    return this.http.get(
      environment.host + environment.endpoint.courses + 'finished');
  }

  add(body: any): Observable<any> {
    return this.http.post(environment.host + environment.endpoint.courses, body);
  }

  update(id: number, body: any): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.courses + id, body);
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete(environment.host + environment.endpoint.courses + id);
  }

  dictionaries(): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.courses + 'dictionaries');
  }

  getByType(
    id: number,
    page: number = 1,
    resultsPerPage: number = null,
    sortField: string = null,
    sortOrder: number = null
  ): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.courses + 'getByTipo?tipo=' + id +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sortField && sortOrder ? '&order=' + sortField + ',' + sortOrder : ''));
  }

  setTrashed(
    id: number,
    body: any,
    trashed: boolean
  ): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.courses + 'setTrashed/' + id
      + '?trashed=' + trashed, body);
  }

  setValutazioni(
    id: number,
    body: any
  ): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.courses + 'setValutazioni/' + id, body);
  }

  convertDate(dataErogazione: any) {
    let anno: string = '';
    for (let i = 6; i <= 9; i++) {
      anno = anno + dataErogazione.charAt(i);
    }
    let mese: string = '';
    for (let i = 3; i <= 4; i++) {
      mese = mese + dataErogazione.charAt(i);
    }
    let giorno: string = '';
    for (let i = 0; i <= 1; i++) {
      giorno = giorno + dataErogazione.charAt(i);
    }
    return anno + '-' + mese + '-' + giorno;
  }

}

