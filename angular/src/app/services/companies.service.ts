import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface CompaniesDto {
  societaId: number;
  ragioneSociale: string;
  indirizzo: string;
  localita: string;
  provincia: string;
  nazione: string;
  telefono: string;
  fax: string;
  partitaIva: string;
}

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(
    private http: HttpClient
  ) {
  }

  list(): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.companies);
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.companies + id);
  }

}
