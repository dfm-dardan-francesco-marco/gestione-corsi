import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ThirdPartiesService {

  constructor(
    private http: HttpClient
  ) {
  }

  countries(): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.thirdParties + '/countries');
  }
}
