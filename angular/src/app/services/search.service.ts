import {Injectable} from '@angular/core';
import {LocalStorageService} from './local-storage.service';

export enum SearchType {
  TEACHER,
  EMPLOYEE,
  COURSE,
  MARK
}

export interface SearchItem {
  id: number;
  type: SearchType;
  firstRow: string;
  secondRow?: string;
}

export enum SearchListType {
  RECENT_SEARCHES,
  QUICK_ACCESS
}

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public readonly MAX_RECENT_SEARCHES = 5;
  public readonly MAX_QUICK_ACCESS = 5;

  private skip: boolean = false;

  constructor(
    private localStorage: LocalStorageService
  ) {
  }

  getRecentSearches(): SearchItem[] {
    if (this.localStorage.hasItem('recent-searches')) {
      return this.localStorage.getItem<SearchItem[]>('recent-searches').reverse();
    }
    return [];
  }

  getQuickAccess(): SearchItem[] {
    if (this.localStorage.hasItem('quick-access')) {
      return this.localStorage.getItem<SearchItem[]>('quick-access').reverse();
    }
    return [];
  }

  skipNext(): void {
    this.skip = true;
  }

  push(type: SearchListType, item: SearchItem): void {
    let array = null;
    if (type === SearchListType.RECENT_SEARCHES) {
      array = this.getRecentSearches();
    } else if (type === SearchListType.QUICK_ACCESS) {
      array = this.getQuickAccess();
    }

    if (array !== null && array.findIndex(arrayItem => arrayItem.id === item.id && arrayItem.type === item.type) === -1 && this.skip === false) {
      const max = SearchListType.RECENT_SEARCHES ? this.MAX_RECENT_SEARCHES : this.MAX_QUICK_ACCESS;
      if (array.length >= max) {
        array.reverse().shift();
      }
      array.push(item);
      this.localStorage.setItem(type === SearchListType.RECENT_SEARCHES ? 'recent-searches' : 'quick-access', array);
    }
    this.skip = false;
  }
}
