import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface CoursesUsersDto {
  corsiUtentiId: number;
  corso: number;
  dipendente: number;
  durata: number;
  showedText: string;
}

export interface CourseUsersSuggestionDto {
  dipendente: number;
  visualizza: string;
  durata?: number;
}

@Injectable({
  providedIn: 'root'
})
export class CoursesUsersService {

  constructor(
    private http: HttpClient
  ) {
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.courseUsers + id);
  }

  getByCourseId(
    courseId: number,
    page: number = 1,
    resultsPerPage: number = null,
    sortField: string = null,
    sortOrder: number = null,
    search: string = null
  ): Observable<any> {
    return this.http.get(
      environment.host + environment.endpoint.courseUsers +
      '?course=' + courseId +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sortField && sortOrder ? '&order=' + sortField + ',' + sortOrder : '') +
      (search !== null ? '&search=' + search : ''));
  }

  deleteEmployeeById(corsiDocentiId: number) {
    return this.http.delete(environment.host + environment.endpoint.courseUsers + corsiDocentiId);
  }

  getEmployees(value: string) {
    return this.http.get(environment.host + environment.endpoint.courseUsers + 'employees?value=' + value);
  }

  pairEmployees(course: number, values: CourseUsersSuggestionDto[]) {
    return this.http.post(environment.host + environment.endpoint.courseUsers + course, values);
  }

  update(corsiUtentiId: number, value: CoursesUsersDto) {
    return this.http.patch(environment.host + environment.endpoint.courseUsers + corsiUtentiId, value);
  }

}
