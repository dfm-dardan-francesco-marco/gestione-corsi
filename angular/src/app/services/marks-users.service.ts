import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface MarksUsersDto {
  valutazioniUtentiId: number;
  corso: number;
  dipendente: number;
  criterio: number;
  valore: number;
}

@Injectable({
  providedIn: 'root'
})
export class MarksUsersService {
  constructor(
    private http: HttpClient
  ) {
  }

  insert(body: any): Observable<any> {
    return this.http.post(environment.host + environment.endpoint.marksUsers, body);
  }

  update(body: any): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.marksUsers, body);
  }

  getByCourseAndEmployee(course: number, employee: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.marksUsers + '?course=' + course + '&resultsPerPage=10' + '&employee=' + employee);
  }

  getByCourse(course: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.marksUsers + '?course=' + course);
  }

  deleteByCorsoAndDipendente(corso: number, dipendente: number): Observable<any> {
    return this.http.delete(environment.host + environment.endpoint.marksUsers + '?corso=' + corso + '&dipendente=' + dipendente);
  }
}
