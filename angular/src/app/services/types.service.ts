import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface TypesDto {
  tipoId: number;
  descrizione: string;
}

@Injectable({
  providedIn: 'root'
})
export class TypesService {

  constructor(
    private http: HttpClient
  ) {
  }

  list(
    page: number = 1,
    resultsPerPage: number = null,
    sortOrder: number = null
  ): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.types +
      '?page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sortOrder ? '&order=' + sortOrder : ''));
  }

  add(body: any): Observable<any> {
    return this.http.post(environment.host + environment.endpoint.types, body);
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete(environment.host + environment.endpoint.types + id);
  }

  update(id: number, body: any): Observable<any> {
    return this.http.patch(environment.host + environment.endpoint.types + id, body);
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.types + id);
  }

}
