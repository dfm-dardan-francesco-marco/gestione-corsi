import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface MarksTeachersDto {
    valutazioniDocentiId: number;
    corso: number;
    docente: number;
    interno: number;
    criterio: number;
    valore: number[];
}

@Injectable({
    providedIn: 'root'
})
export class MarksTeachersService {
    constructor(
        private http: HttpClient
    ) {
    }

    insert(body: any): Observable<any> {
        return this.http.post(environment.host + environment.endpoint.marksTeachers, body);
    }

    getByDocente(corso: number, docente: number): Observable<any> {
        return this.http.get(environment.host + environment.endpoint.marksTeachers + '?course=' + corso + '&teacher=' + docente);
    }

    getByInterno(corso: number, interno: number): Observable<any> {
        return this.http.get(environment.host + environment.endpoint.marksTeachers + '?course=' + corso + '&interno=' + interno);
    }

  getByCourse(course: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.marksTeachers + '?course=' + course);
  }

    deleteByCorsoAndInternoOrDocente(corso: number, docente: number, interno: number): Observable<any> {
        return this.http.delete(environment.host + environment.endpoint.marksTeachers +
            '?corso=' + corso + '&docente=' + docente + '&interno=' + interno);
    }

}
