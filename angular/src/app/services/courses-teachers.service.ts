import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

export interface CoursesTeachersDto {
  corsiDocentiId: number;
  corso: number;
  docenteOInternoId: number;
  type: string;
  showedText: string;
}

export interface CourseTeachersSuggestionDto {
  docente: number;
  dipendente: number;
  visualizza: string;
}

@Injectable({
  providedIn: 'root'
})
export class CoursesTeachersService {

  constructor(
    private http: HttpClient
  ) {
  }

  getById(id: number): Observable<any> {
    return this.http.get(environment.host + environment.endpoint.courseTeachers + id);
  }

  getByCourseId(
    courseId: number,
    page: number = 1,
    resultsPerPage: number = null,
    sortField: string = null,
    sortOrder: number = null,
    search: string = null
  ): Observable<any> {
    return this.http.get(
      environment.host + environment.endpoint.courseTeachers +
      '?course=' + courseId +
      '&page=' + (Number.isNaN(page) ? 0 : page - 1) +
      (resultsPerPage !== null ? '&resultsPerPage=' + resultsPerPage : '') +
      (sortField && sortOrder ? '&order=' + sortField + ',' + sortOrder : '') +
      (search !== null ? '&search=' + search : ''));
  }

  deleteTeacherOrEmployeeById(corsiDocentiId: number) {
    return this.http.delete(environment.host + environment.endpoint.courseTeachers + corsiDocentiId);
  }

  getTeachersAndEmployees(value: string) {
    return this.http.get(environment.host + environment.endpoint.courseTeachers + 'teachers-employees?value=' + value);
  }

  pairTeachersAndEmployees(course: number, values: CourseTeachersSuggestionDto[]) {
    return this.http.post(environment.host + environment.endpoint.courseTeachers + course, values);
  }

}
