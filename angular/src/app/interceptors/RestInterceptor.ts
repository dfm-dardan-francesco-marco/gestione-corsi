import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {v4 as uuidv4} from 'uuid';
import {MessageService} from 'primeng/api';
import {Injectable} from '@angular/core';

@Injectable()
export class RestInterceptor implements HttpInterceptor {

  constructor(private messageService: MessageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const uuid: string = uuidv4();

    if (!environment.production) {
      console.log(uuid, '(Request)', req);
    }

    return next.handle(req).pipe(
      tap((res: any) => {
        if (!environment.production && (res.type === undefined || res.type !== 0)) {
          console.log(uuid, '(Response)', res);
        }
      }),
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          console.log(uuid, '(Error)', err);

          this.messageService.add({
            severity: 'error',
            summary: 'Errore server',
            detail: 'Contattare l\'ufficio tecnico',
            life: 60 * 60 // wait an hour before auto-closing message, so if the user is away from desk when error occurs, he will still see a feedback
          });
        }
        return of(err);
      }));
  }
}
