﻿USE gestione_corsi;


INSERT INTO docenti (titolo,ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,email,note_generiche,partita_iva,referente,trashed) VALUES 
('Dr','Toxas SpA','via Garibaldi 9','Camposampiero','PD','Italia','0491234567','0491234567','maurizio@gmail.com','Gli manca il collo','12345678901','Stefano Alto',NULL)
,('Dr','Ripas Snc','via Verdi 15','Quinto di Treviso','TV','Italia','0422652674','0422652674','martino@gmail.com','Fra martino campanaro','65586406707','Matteo Sempronio',NULL)
,('Prof','Fogar Srl','via San Giacomo 36','Marghera','VE','Italia','0419751386','0419751386','rocco@gmail.com','Non gli manca niente','74563021865','Mattia Basso',NULL)
,('Prof','Lunac Sapa','via delle Fontane 147','Feltre','BL','Italia','0437852649','0437852649','bocelli@gmail.com','Gli manca la vista','013882694248','Marco Caio',NULL)
,('Dr','Machete Sas','via Ghandi 73','Bassano del Grappa','VI','Italia','0444369624','0444369624','corona@gmail.com','Fabrizio Corona ovviamente','42690325875','Francesco Tizio',NULL)
,('Dr','GTA SpA','via Alpini 9','Camposampiero','PD','Italia','0491234567','0491234567','antonino@gmail.com','bravo','12345670000','Luciano Alto',NULL)
,('Dr','Dubrov Snc','via Moda 15','Quinto di Treviso','TV','Italia','0422652674','0422652674','lotardo@gmail.com','Suono il violino','65586411107','Lucia Simpiosi',NULL)
,('Prof','Lingor Srl','via San Giovanni 36','Marghera','VE','Italia','0419751386','0419751386','roccardo@gmail.com','bravo','74563021115','Nicola Jordan',NULL)
,('Prof','Spirgon Sapa','via delle Trev 147','Feltre','BL','Italia','0437852649','0437852649','bellinelli@gmail.com','Bravo a calcio','013945694248','Luca Bracco',NULL)
,('Dr','Muniser Sas','via Gaspri 73','Bassano del Grappa','VI','Italia','0444369624','0444369624','zlatan@gmail.com','Fabrizio Corona ovviamente','42690044675','Mino Raiola',NULL)
;
INSERT INTO docenti (titolo,ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,email,note_generiche,partita_iva,referente,trashed) VALUES 
('Dr','Mediaset SpA','via Ronchi9','Padova','PD','Italia','0491234567','0491234567','mediaset@gmail.com','Menomale che Silvio cè','48945347984','Silvio Berlusconi',NULL)
,('Dr','Pitarello Sas','via Bianhi15','Campodarsego','PD','Italia','0422652674','0422652674','pita@gmail.com','Bravissimo','87454834842','Mariano Lori',NULL)
,('Prof','Bailocom srl','via Lorenzo 36','Milano','MI','Italia','0419751386','0419751386','stefano@gmail.com','vende pomodori','86452315379','Alto Gottardelo',NULL)
,('Prof','O.B.I SpA','via Trevisan 147','Bari','BR','Italia','0437852649','0437852649','obi@gmail.com','insegnante eccellente','89745231753','Vincenzo Latino',NULL)
,('Dr','Unox Sas','via delle Apli 73','Canisano','TV','Italia','354567842124','','unox@gmail.com','Grande professore.','23158974121','Flavia Mometto',NULL)
,('Dr','Tenco SpA','via Rossana 9','Pisa','PS','Italia','3454864124','0491234567','tenco@gmail.com','Un po'' cattiva','45465212315','Lucia Carletto',NULL)
,('Dr','Verdi SpA','via Stelvio 15','Albignasegoo','PD','Burundi','32485646687','845632156','verdi@gmail.com','Bellissima','74543126894','Lucia Gotardo',NULL)
,('Prof','Adalet Srl','via pino 36','Marghera','VE','Italia','43454635465','81246812156','adalet@gmail.com','fenomeno','16418216761','Lionel Messi',NULL)
,('Prof','Romini Srl','via Michelangelo 147','Catania','CT','Togo','3211879845','0437852649','romini@gmail.com','docenti di java','00544612875','Romano Lucio',NULL)
,('Dr','Moretti SpA','via Bellotto 73','Rustega','PD','Italia','3214651234','0444369624','moretti@gmail.com','Vivaaaaaa','15412313570','Moro Biondo',NULL);

INSERT INTO docenti (titolo,ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,email,note_generiche,partita_iva,referente,trashed) VALUES 
('Dr.ssa','Mastro Lindo Srl','via Marcolini 12','San Giorgio delle Pertiche','PD','Turkey','354123456',NULL,'lindo@gmail.com','Viva Angular','15455325640','Mastro Geppetto',NULL)
,('Ing','Rindor Sapa','via delle Spine 40','Lorena','LO','Mauritania','312484124489','1154654184','rindor@gmail.com',NULL,'54054065405','Guido Vespa',NULL)
,(NULL,'Telebingo Sas','via Renzo 21','Rustega','PD','Italia','3249645389',NULL,'telebingo@gmail.com',NULL,'12346545123','Nicola Michelon',NULL)
,('Dr.ssa','Cristina Malvestio Snc','via Vivladi 32','Roma','RM','Italia','3215641348','0491234567','malvestio@gmail.com','lavora sodo','23145674124','Stefano Alto',NULL)
,(NULL,'Salmo SpA','via Rana 11','Loreggia','PD','Italia','0321231321','021615516','salmo@gmail.com',NULL,'10510066441','Matteo Sonsio',NULL)
,(NULL,'Fedre Romano SpA','via Gingeriono 34','Campiso','RD','Italia','3215454145','044984121','fedre@gmail.com','ciao ragazzi','82373248923','Mario Rossi',NULL)
,(NULL,'Gonfo SpA','via vivano 29','Rimini','FE','Italia','3124654894','05498415645','gonfo@gmail.com','Weilaaa','02564102154','Marino Lodi',NULL)
,('Dr','Dentox Srl','via San Giovanni 36','Pula','PL','Croatia','13265412156','2165465','dentox@gmail.com',NULL,'32154984126','Viviano Mirco',NULL)
,('Prof','Inspect SpA','via Bellotto 73','Rustega','PD','United Kingdom of Great Britain and Northern Ireland','31564812498','015498412','inspect@gmail.com','Lorem ipsum caren lore','51234641245','Mario Rossi',NULL)
,('Prof','Sondrioso SpA','via Romolo 21','Roma','RM','Albania','321564984','0549794123','sondrioso@gmail.com',NULL,'15497842186','Sinsa Mihajlovic',NULL);

INSERT INTO docenti (titolo,ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,email,note_generiche,partita_iva,referente,trashed) VALUES 
(NULL,'Unicredit Snc','via Verdi 15','Camposampiero','PD','Italia','2154156544','1324654131','unicredit@gmail.com',NULL,'23487846145','Stefano Renzo',NULL);


INSERT INTO societa (ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,partita_iva) VALUES 
('Radolfre Spa','via Rossi 2','Milano','MI','Italia','0651649754','0651649754','85196584038')
,('Luxer Spa','via Gialli 4','Venezia','VE','Italia','0419541211','0419541211','98513696622')
,('Indivdor Sapa','via Fucsia 6','Roma','RM','Italia','0884593284','0884593284','75685210224')
,('Ynaipe Sapa','via Nera 8','Firenze','FI','Italia','0331597304','0331597304','45693259971')
,('Hinder Spa','via Roma 2','Milano','MI','Italia','0651649754','0651649754','85196584111')
,('Stirpe Snc','via Troino 4','Venezia','VE','Italia','0419541211','0419541211','98512226622')
,('Londsos Snc','via Siepe 6','Roma','RM','Italia','0884593284','0884593284','75645610224')
,('Tunder Snc','via Ronchi 8','Firenze','FI','Italia','0331597304','0331597304','45699129971')
,('Windows spa','via Fermi 2','Milano','MI','Italia','0651649754','0651649754','85789584038')
,('Poster sp','via Battisi 4','Venezia','VE','Italia','0419541211','0419541211','98512486622');

INSERT INTO societa (ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,partita_iva) VALUES 
('Inxear spa','via Canova 6','Roma','RM','Italia','0884593284','0884593284','75686780224')
,('Spinof Srl','via Pertini 8','Firenze','FI','Italia','0331597304','0331597304','45693266671')
,('Pino Spa','via Rossi 2','Milano','MI','Italia','0651649754','0651649754','85196584039')
,('Rondo Srl','via Gialli 4','Venezia','VE','Italia','0419541211','0419541211','98513696623')
,('Ganzo Srl','via Fucsia 6','Roma','RM','Italia','0884593284','0884593284','75685210225')
,('Fintor Spa','via Nera 8','Firenze','FI','Italia','0331597304','0331597304','45693259972')
,('Limon Srl','via Roma 2','Milano','MI','Italia','0651649754','0651649754','85196584112')
,('Linux Srl','via Troino 4','Venezia','VE','Italia','0419541211','0419541211','98512226623')
,('Soper Snc','via Siepe 6','Roma','RM','Italia','0884593284','0884593284','75645610225')
,('Bander Srl','via Ronchi 8','Firenze','FI','Italia','0331597304','0331597304','45699129976');

INSERT INTO societa (ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,partita_iva) VALUES 
('Asper Srl','via Fermi 2','Milano','MI','Italia','0651649754','0651649754','85789584039')
,('Paler Srl','via Battisi 4','Venezia','VE','Italia','0419541211','0419541211','98512486623')
,('Mander Srl','via Canova 6','Roma','RM','Italia','0884593284','0884593284','75686780221')
,('Mastic Sapa','via Pertini 8','Firenze','FI','Italia','0331597304','0331597304','45693266673')
,('Rompsker Srl','via Rossi 2','Milano','MI','Italia','0651649754','0651649754','15196584038')
,('Ginger Spa','via Gialli 4','Venezia','VE','Italia','0419541211','0419541211','18513696622')
,('Larder Srl','via Fucsia 6','Roma','RM','Italia','0884593284','0884593284','15685210224')
,('Tasker  Srl','via Nera 8','Firenze','FI','Italia','0331597304','0331597304','15693259971')
,('Suber  Srl','via Roma 2','Milano','MI','Italia','0651649754','0651649754','15196584111')
,('Ispecte Srl','via Troino 4','Venezia','VE','Italia','0419541211','0419541211','18512226622');

INSERT INTO societa (ragione_sociale,indirizzo,localita,provincia,nazione,telefono,fax,partita_iva) VALUES 
('Ouier Srl','via Siepe 6','Roma','RM','Italia','0884593284','0884593284','15645610224')
,('Yuinde Spa','via Ronchi 8','Firenze','FI','Italia','0331597304','0331597304','15699129971')
,('Vivano Srl','via Fermi 2','Milano','MI','Italia','0651649754','0651649754','15789584038')
,('Pooka Srl','via Battisi 4','Venezia','VE','Italia','0419541211','0419541211','18512486622')
,('Apple Snc','via Canova 6','Roma','RM','Italia','0884593284','0884593284','15686780224')
,('Xiamoi Srl','via Pertini 8','Firenze','FI','Italia','0331597304','0331597304','15693266671');


INSERT INTO corsi (tipo,tipologia,descrizione,edizione,previsto,erogato,durata,misura,note,luogo,ente,societa,data_erogazione,data_chiusura,data_censimento,interno,trashed,valutazioni) VALUES 
(1,1,'descrizione1','Prima',1,0,8,1,'note1','Padova','Nicola',1,NULL,NULL,'2020-03-01',1,NULL,1)
,(1,2,'descrizione2','Seconda',0,1,16,1,'note2','Padova','Matteo',1,'2020-03-15','2020-03-16','2020-03-01',1,NULL,1)
,(1,3,'descrizione3','Prima',1,0,8,2,'note3','Venezia','Mattia',2,NULL,NULL,'2020-03-01',1,NULL,1)
,(1,2,'descrizione4','Seconda',1,0,8,1,'note4','Milano','Marco',2,NULL,NULL,'2020-03-01',1,NULL,1)
,(1,1,'descrizione5','Terza',0,1,10,1,'note5','Padova','Stefano',3,'2020-03-16','2020-03-20','2020-03-01',0,NULL,1)
,(1,3,'descrizione6','Prima',1,0,8,2,'note6','Firenze','Francesco',4,NULL,NULL,'2020-03-01',0,NULL,1)
,(1,1,'Agile','Prima',0,1,8,1,NULL,'Rimini','Mediaset',36,'2020-05-04','2020-05-21','2020-05-12',1,NULL,0)
,(1,2,NULL,'Prima',1,0,8,2,NULL,'Milano','Provincia',3,'2020-05-28','2020-05-30','2020-05-28',1,NULL,0)
,(1,3,NULL,'Seconda',0,1,8,2,NULL,'Padova','Comune',15,'2020-05-03','2020-06-18','2020-05-12',1,NULL,0)
,(6,2,'Solo mattina',NULL,1,0,48,2,'Alcuni dipendenti ne avevano bisongo','Ibiza','Comune',21,'2020-05-03',NULL,'2020-05-12',0,NULL,1);

INSERT INTO corsi (tipo,tipologia,descrizione,edizione,previsto,erogato,durata,misura,note,luogo,ente,societa,data_erogazione,data_chiusura,data_censimento,interno,trashed,valutazioni) VALUES 
(3,2,'sperimentazione','Seconda',1,0,78,1,'','Treviso','Provincia',9,'2020-05-03','2020-05-11','2020-05-12',1,NULL,1)
,(47,3,NULL,'Prima',1,0,42,1,NULL,'Vicenza','Corvallis',4,'2020-05-18','2020-05-22','2020-05-12',0,NULL,0)
,(72,1,'mobile','Prima',1,0,15,2,'app mobile','Milano','Camera di commercio',25,'2020-05-28','2020-05-30','2020-05-12',1,NULL,0)
,(23,2,'database','Seconda',1,0,65,1,NULL,'Atene','Corvallis',27,'2020-05-25',NULL,'2020-05-12',1,NULL,0)
,(77,1,'','Terza',1,0,21,2,NULL,'Padova','Corvallis',11,NULL,'2020-05-30','2020-05-12',0,NULL,0)
,(45,1,'','Quarta',1,0,54,1,NULL,'Vicenza','Comune',1,'2019-10-01','2019-11-15','2020-05-12',1,NULL,3)
,(73,1,NULL,'Prima',1,0,54,2,NULL,'Treviso','Mediaset',7,'2020-08-06',NULL,'2020-05-12',0,NULL,0)
,(54,3,NULL,'Seconda',1,0,58,1,NULL,'Padova','Camera di commercio',28,'2020-04-12','2020-06-26','2020-05-12',0,NULL,0)
,(1,2,'programmazione','Seconda',1,0,15,2,NULL,'Padova','Comune',31,'2020-05-19','2020-05-30','2020-05-12',0,NULL,0)
,(75,3,NULL,'Seconda',1,0,8,2,NULL,'Milano','Regione Veneto',25,NULL,NULL,'2020-05-12',1,NULL,1);

INSERT INTO corsi (tipo,tipologia,descrizione,edizione,previsto,erogato,durata,misura,note,luogo,ente,societa,data_erogazione,data_chiusura,data_censimento,interno,trashed,valutazioni) VALUES 
(38,3,'Cisco ','Seconda',1,0,121,1,NULL,'Padova','Sky Java',5,'2020-04-05','2020-06-25','2020-04-05',0,NULL,0)
,(71,3,'Database','Seconda',1,0,25,1,NULL,'Padova','Oracle',25,'2020-05-03','2020-05-16','2020-05-12',1,NULL,0)
,(44,1,'Photoshop','Prima',1,0,25,2,NULL,'Padova','Sky Photo',2,'2020-05-12','2020-05-15','2020-05-12',0,NULL,0)
,(19,2,'descrizione7','Prima',1,0,123,1,NULL,'Lecce','Corvallis',29,'2020-02-02',NULL,'2020-05-12',1,NULL,1)
,(1,3,'Spring Boot','Seconda',1,0,31,2,NULL,'Padova','Eclipse',31,'2020-05-30','2020-08-15','2020-05-12',0,NULL,0)
,(18,3,'Oracle',NULL,1,0,25,2,NULL,'Vicenza','Oracle',35,'2020-05-03','2020-05-12','2020-05-12',0,NULL,2)
,(77,1,'VSCode',NULL,1,0,31,1,NULL,'Padova','Corvallis',4,'2020-05-27',NULL,'2020-05-12',1,NULL,0)
,(25,3,NULL,'Prima',1,0,14,2,NULL,'Londra','Mediaset',16,'2020-05-10','2020-05-30','2020-05-12',1,NULL,0)
,(3,2,NULL,'Prima',1,0,31,2,NULL,'Vicenza','Eclipse',26,'2020-05-30','2020-08-14','2020-05-12',0,NULL,0)
,(56,1,'Solo mattina','Prima',1,0,31,2,NULL,'Milano','Mediaset',17,'2020-05-27',NULL,'2020-05-12',0,NULL,0);

INSERT INTO corsi (tipo,tipologia,descrizione,edizione,previsto,erogato,durata,misura,note,luogo,ente,societa,data_erogazione,data_chiusura,data_censimento,interno,trashed,valutazioni) VALUES 
(1,3,'programmazione','Terza',1,0,25,1,NULL,'Londra','Mediaset',33,'2020-05-24','2020-05-29','2020-05-24',1,NULL,0)
,(51,3,'Solo mattina','Seconda',1,0,25,2,NULL,'Lecce','Eclipse',14,'2020-05-28','2020-05-30','2020-05-14',1,NULL,0)
,(16,2,'Database','Seconda',1,0,25,2,'Ripasso di Database','Milano','Mediaset',2,'2020-05-03','2020-05-30','2020-05-03',0,NULL,0)
,(77,1,'VSCode','Seconda',1,0,121,1,'Corso completo','Padova','Corvallis',12,'2020-05-03',NULL,'2020-05-12',1,NULL,1);


INSERT INTO dipendenti (matricola,societa,cognome,nome,sesso,data_nascita,luogo_nascita,stato_civile,titolo_studio,conseguito_presso,anno_conseguimento,tipo_dipendente,qualifica,livello,data_assunzione,responsabile_risorsa,responsabile_area,data_fine_rapporto,data_scadenza_contratto) VALUES 
('0001',1,'Panozzo','Marco','M','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Segretario','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0002',2,'Baljaj','Dardan','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0003',1,'Solivo','Matteo','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0004',1,'Zogno','Stefano','M','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Amministratore','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0005',1,'Zogno','Mattia','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Segretario','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL)
,('0006',1,'Michelon','Nicola','M','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0007',2,'Giacometti','Chiara','F','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Segretario','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0008',1,'Zuanon','Erika','F','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Capo reparto','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0009',1,'Scabbia','Aurora','F','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Segretario','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0010',1,'Perin','Stefano','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL);

INSERT INTO dipendenti (matricola,societa,cognome,nome,sesso,data_nascita,luogo_nascita,stato_civile,titolo_studio,conseguito_presso,anno_conseguimento,tipo_dipendente,qualifica,livello,data_assunzione,responsabile_risorsa,responsabile_area,data_fine_rapporto,data_scadenza_contratto) VALUES 
('0011',1,'Carraro','Alberto','M','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0012',2,'Ibra','Eddy','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0013',1,'State','Mauri','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0014',1,'Monteanu','Giovanni','M','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0015',1,'Sorge','Francesco','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Dirigente','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL)
,('0016',1,'Chinellato','Sara','F','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0017',2,'Karaboja','Rebecca','F','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0018',1,'Zorzi','Chiara','F','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0019',1,'Piccolo','Erica','M','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'CEO','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0020',1,'Lorenzin','Alessia','F','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL);

INSERT INTO dipendenti (matricola,societa,cognome,nome,sesso,data_nascita,luogo_nascita,stato_civile,titolo_studio,conseguito_presso,anno_conseguimento,tipo_dipendente,qualifica,livello,data_assunzione,responsabile_risorsa,responsabile_area,data_fine_rapporto,data_scadenza_contratto) VALUES 
('0021',11,'Menato','Lorenzo','M','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0022',25,'Sheffiel','Thomas','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0023',17,'Pallaro','Marco','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Capo reparto','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0024',1,'Pausini','Laura','F','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Dirigente','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0025',13,'Zlatan','Ibrahimovic','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Amministratore','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL)
,('0026',16,'Donnarumma','Lorenzo','M','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0027',21,'Fantinato','Mauro','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Capo reparto','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0028',19,'Bertazzo','Matteo','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0029',13,'Zorzi','Gaia','F','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'CEO','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0030',1,'Manera','Federico','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL);

INSERT INTO dipendenti (matricola,societa,cognome,nome,sesso,data_nascita,luogo_nascita,stato_civile,titolo_studio,conseguito_presso,anno_conseguimento,tipo_dipendente,qualifica,livello,data_assunzione,responsabile_risorsa,responsabile_area,data_fine_rapporto,data_scadenza_contratto) VALUES 
('0031',25,'Malvestio','Sharon','F','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0032',34,'Luisetto','Mirco','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0033',34,'Sandro','Nicola','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'CEO','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0034',29,'Rossi','Luifi','M','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Amministratore','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0035',24,'Ungharetti','Giuseppe','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL)
,('0036',12,'Rossi','Flavia','F','2000-07-03','Camposampiero','celibe','titoloStudio1','IIS Newton-Pertini',2019,'Segretario','Primo','Primo','2020-02-15','Lui','Lui',NULL,NULL)
,('0037',29,'Pallaro','Lucio','M','2000-03-08','Camposampiero','celibe','titoloStudio2','IIS Newton-Pertini',2019,'Amministratore','Primo','Primo','2020-02-15','Matteo','Marco',NULL,NULL)
,('0038',14,'Bertazzo','Eduard','M','2000-05-06','Camposampiero','celibe','titoloStudio3','IIS Newton-Pertini',2019,'Dirigente','Primo','Primo','2020-02-15','Marco','Lui',NULL,NULL)
,('0039',17,'Peron','Lorenzo','M','2000-12-07','Due Carrare','celibe','titoloStudio4','IIS Garibaldi',2019,'Capo reparto','Secondo','Secondo','2020-02-15','Marco','Dardan',NULL,NULL)
,('0040',18,'Bedin','Alice','F','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Amministratore','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL);

INSERT INTO dipendenti (matricola,societa,cognome,nome,sesso,data_nascita,luogo_nascita,stato_civile,titolo_studio,conseguito_presso,anno_conseguimento,tipo_dipendente,qualifica,livello,data_assunzione,responsabile_risorsa,responsabile_area,data_fine_rapporto,data_scadenza_contratto) VALUES 
('0041',19,'Bertoncello','Federico','M','2000-09-27','Due Carrare','celibe','titoloStudio5','IIS Garibaldi',2019,'Amministratore','Secondo','Secondo','2020-02-15','Dardan','Stefano',NULL,NULL);


INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(5,NULL,1)
,(5,3,NULL)
,(4,NULL,4)
,(6,NULL,4)
,(1,17,NULL)
,(1,18,NULL)
,(2,25,NULL)
,(3,NULL,6)
,(3,29,NULL)
,(7,30,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(7,NULL,15)
,(8,13,NULL)
,(8,14,NULL)
,(8,9,NULL)
,(8,1,NULL)
,(8,2,NULL)
,(8,3,NULL)
,(9,4,NULL)
,(9,5,NULL)
,(9,6,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(10,7,NULL)
,(10,8,NULL)
,(10,9,NULL)
,(10,NULL,2)
,(10,NULL,15)
,(11,10,NULL)
,(11,11,NULL)
,(11,12,NULL)
,(11,NULL,2)
,(12,13,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(12,14,NULL)
,(12,15,NULL)
,(12,NULL,15)
,(14,16,NULL)
,(14,17,NULL)
,(14,NULL,1)
,(15,18,NULL)
,(16,NULL,4)
,(16,NULL,5)
,(17,NULL,3)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(17,19,NULL)
,(18,20,NULL)
,(18,21,NULL)
,(19,2,NULL)
,(20,22,NULL)
,(20,23,NULL)
,(21,24,NULL)
,(21,25,NULL)
,(22,26,NULL)
,(22,27,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(23,28,NULL)
,(23,29,NULL)
,(23,NULL,21)
,(24,NULL,2)
,(24,30,NULL)
,(25,NULL,15)
,(26,31,NULL)
,(27,18,NULL)
,(28,23,NULL)
,(30,9,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(30,NULL,6)
,(31,27,NULL)
,(31,NULL,10)
,(32,5,NULL)
,(33,NULL,5)
,(34,NULL,1)
,(1,NULL,11)
,(2,NULL,11)
,(3,NULL,11)
,(4,NULL,11)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(5,NULL,11)
,(6,NULL,11)
,(8,NULL,11)
,(10,NULL,11)
,(11,NULL,11)
,(20,NULL,11)
,(9,NULL,11)
,(1,2,NULL)
,(3,2,NULL)
,(4,2,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(5,2,NULL)
,(6,2,NULL)
,(7,2,NULL)
,(9,2,NULL)
,(11,2,NULL)
,(13,2,NULL)
,(1,6,NULL)
,(1,26,NULL)
,(1,27,NULL)
,(1,11,NULL)
;
INSERT INTO corsi_docenti (corso,docente,interno) VALUES 
(1,20,NULL)
,(1,14,NULL)
,(1,25,NULL)
;


INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(1,1,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "1"}')
,(1,2,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(1,3,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(1,4,'{"1": "0", "2": "0", "3": "1", "4": "1", "na": "0"}')
,(1,5,'{"1": "1", "2": "1", "3": "0", "4": "0", "na": "0"}')
,(1,6,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(1,7,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(1,8,'{"1": "0", "2": "0", "3": "1", "4": "1", "na": "0"}')
,(1,9,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(2,1,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "2"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(2,2,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "2"}')
,(2,3,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(2,4,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(2,5,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(2,6,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(2,7,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(2,8,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(2,9,'{"1": "1", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(3,1,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(3,2,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(3,3,'{"1": "0", "2": "0", "3": "1", "4": "0", "na": "0"}')
,(3,4,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(3,5,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(3,6,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(3,7,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(3,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(3,9,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "2"}')
,(4,1,'{"1": "3", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(4,2,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
,(4,3,'{"1": "0", "2": "0", "3": "3", "4": "0", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(4,4,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(4,5,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "2"}')
,(4,6,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(4,7,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(4,8,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(4,9,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(5,1,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(5,2,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(5,3,'{"1": "0", "2": "0", "3": "3", "4": "0", "na": "0"}')
,(5,4,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(5,5,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(5,6,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(5,7,'{"1": "0", "2": "0", "3": "3", "4": "0", "na": "0"}')
,(5,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(5,9,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(6,1,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(6,2,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(6,3,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(6,4,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(6,5,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "3"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(6,6,'{"1": "3", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(6,7,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(6,8,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(6,9,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(26,1,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(26,2,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(26,3,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(26,4,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(26,5,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(26,6,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "2"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(26,7,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(26,8,'{"1": "0", "2": "1", "3": "2", "4": "1", "na": "0"}')
,(26,9,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(11,1,'{"1": "3", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(11,2,'{"1": "0", "2": "0", "3": "3", "4": "0", "na": "0"}')
,(11,3,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(11,4,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(11,5,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "2"}')
,(11,6,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(11,7,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(11,8,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(11,9,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(16,1,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(16,2,'{"1": "0", "2": "1", "3": "0", "4": "0", "na": "0"}')
,(16,3,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(16,4,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(16,5,'{"1": "0", "2": "2", "3": "0", "4": "0", "na": "0"}')
,(16,6,'{"1": "0", "2": "0", "3": "1", "4": "1", "na": "0"}')
,(16,7,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
,(16,8,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(16,9,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
,(10,1,'{"1": "4", "2": "0", "3": "0", "4": "6", "na": "0"}')
,(10,2,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(10,3,'{"1": "0", "2": "1", "3": "0", "4": "0", "na": "0"}')
,(10,4,'{"1": "1", "2": "2", "3": "1", "4": "2", "na": "0"}')
,(10,5,'{"1": "3", "2": "2", "3": "2", "4": "2", "na": "1"}')
,(10,6,'{"1": "0", "2": "2", "3": "0", "4": "5", "na": "0"}')
,(10,7,'{"1": "0", "2": "0", "3": "0", "4": "6", "na": "0"}')
,(10,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(10,9,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(20,1,'{"1": "4", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(20,2,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
,(20,3,'{"1": "0", "2": "0", "3": "3", "4": "0", "na": "0"}')
,(20,4,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(20,5,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(20,6,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(20,7,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(20,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(20,9,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(24,1,'{"1": "4", "2": "0", "3": "0", "4": "0", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(24,2,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
,(24,3,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(24,4,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(24,5,'{"1": "0", "2": "0", "3": "0", "4": "0", "na": "2"}')
,(24,6,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(24,7,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(24,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(24,9,'{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}')
,(34,1,'{"1": "2", "2": "0", "3": "0", "4": "0", "na": "0"}')
,(34,2,'{"1": "0", "2": "3", "3": "0", "4": "0", "na": "0"}')
;
INSERT INTO gestione_corsi.valutazioni_corsi (corso,criterio,valore) VALUES 
(34,3,'{"1": "0", "2": "0", "3": "2", "4": "0", "na": "0"}')
,(34,4,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(34,5,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(34,6,'{"1": "0", "2": "0", "3": "0", "4": "3", "na": "0"}')
,(34,7,'{"1": "0", "2": "1", "3": "2", "4": "2", "na": "0"}')
,(34,8,'{"1": "0", "2": "0", "3": "0", "4": "2", "na": "0"}')
,(34,9,'{"1": "2", "2": "0", "3": "2", "4": "0", "na": "0"}')
;


INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(1,1,8)
,(2,3,8)
,(2,4,8)
,(3,3,8)
,(3,4,8)
,(3,5,8)
,(4,2,8)
,(4,1,8)
,(4,3,8)
,(5,2,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(5,3,8)
,(5,5,8)
,(1,2,8)
,(1,38,8)
,(1,7,8)
,(1,3,8)
,(1,40,8)
,(1,28,8)
,(1,6,8)
,(1,38,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(1,33,3)
,(2,28,8)
,(2,6,8)
,(2,20,8)
,(2,31,8)
,(2,18,8)
,(2,34,8)
,(2,19,8)
,(2,7,8)
,(2,39,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(3,35,8)
,(3,33,8)
,(3,31,8)
,(3,40,8)
,(3,38,8)
,(4,15,8)
,(4,9,8)
,(4,29,8)
,(4,27,8)
,(4,16,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(4,32,8)
,(4,10,8)
,(5,24,8)
,(5,10,8)
,(5,32,8)
,(5,40,8)
,(5,27,8)
,(5,30,8)
,(5,36,8)
,(5,14,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(5,12,8)
,(6,7,8)
,(6,34,8)
,(6,8,8)
,(6,41,8)
,(6,19,8)
,(6,35,8)
,(6,39,8)
,(6,15,8)
,(6,14,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(6,6,8)
,(6,5,1)
,(7,20,8)
,(7,26,8)
,(7,31,8)
,(7,40,8)
,(7,10,8)
,(7,2,8)
,(7,1,8)
,(7,28,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(8,1,8)
,(8,2,8)
,(8,3,8)
,(8,5,8)
,(8,4,8)
,(8,6,8)
,(8,7,8)
,(8,8,8)
,(9,9,8)
,(9,10,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(9,12,8)
,(9,13,8)
,(9,14,8)
,(9,15,8)
,(9,16,8)
,(9,17,8)
,(9,18,8)
,(9,19,8)
,(9,20,8)
,(10,26,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(10,27,8)
,(10,28,8)
,(10,29,8)
,(10,30,8)
,(10,31,8)
,(10,32,8)
,(10,33,8)
,(10,34,8)
,(10,35,8)
,(11,36,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(11,37,8)
,(11,38,8)
,(11,41,8)
,(11,39,8)
,(11,40,8)
,(11,12,8)
,(12,33,8)
,(12,15,8)
,(12,16,8)
,(12,3,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(12,4,8)
,(12,35,8)
,(14,2,8)
,(14,15,8)
,(14,3,8)
,(14,5,8)
,(14,6,8)
,(14,19,8)
,(14,35,8)
,(14,20,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(15,14,8)
,(15,15,8)
,(15,41,8)
,(15,4,8)
,(15,3,8)
,(15,5,8)
,(16,2,8)
,(16,1,8)
,(16,40,8)
,(16,20,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(16,11,8)
,(16,33,8)
,(17,11,8)
,(17,5,8)
,(17,10,8)
,(17,12,8)
,(17,13,8)
,(17,29,8)
,(17,40,8)
,(18,2,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(18,15,8)
,(18,26,8)
,(18,30,8)
,(19,2,8)
,(19,39,8)
,(19,40,8)
,(19,38,8)
,(19,20,8)
,(19,6,8)
,(19,1,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(19,3,1)
,(20,3,8)
,(20,5,8)
,(20,30,8)
,(20,31,8)
,(20,2,8)
,(20,15,8)
,(20,28,8)
,(21,6,8)
,(21,7,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(21,2,8)
,(21,20,8)
,(21,33,8)
,(21,15,8)
,(21,1,8)
,(22,12,3)
,(22,11,3)
,(22,13,3)
,(22,2,3)
,(23,33,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(23,39,8)
,(23,14,8)
,(23,6,8)
,(23,20,8)
,(24,33,8)
,(24,38,8)
,(24,6,8)
,(24,3,8)
,(24,1,8)
,(24,15,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(25,2,8)
,(25,1,8)
,(26,26,8)
,(26,39,8)
,(26,29,8)
,(26,10,8)
,(27,11,8)
,(27,40,8)
,(27,41,8)
,(27,20,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(27,11,8)
,(28,1,8)
,(28,32,8)
,(28,33,8)
,(30,3,8)
,(30,32,8)
,(30,35,8)
,(30,10,8)
,(31,39,8)
,(31,4,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(31,6,8)
,(31,3,8)
,(31,33,8)
,(31,41,8)
,(31,39,8)
,(31,38,8)
,(32,33,8)
,(32,2,8)
,(32,11,8)
,(32,12,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(33,15,8)
,(33,2,8)
,(33,4,8)
,(33,1,8)
,(33,3,8)
,(34,15,8)
,(34,2,8)
,(34,5,8)
,(34,3,8)
,(34,26,8)
;
INSERT INTO corsi_utenti (corso,dipendente,durata) VALUES 
(2,2,1)
,(3,2,1)
,(6,2,1)
,(9,2,1)
,(26,2,1)
,(1,26,1)
;


INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (1, 10, null, 2, 16, '{"1": "1", "2": "0", "3": "0", "4": "0", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (2, 10, null, 2, 17, '{"1": "0", "2": "1", "3": "0", "4": "0", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (3, 10, null, 2, 18, '{"1": "0", "2": "0", "3": "1", "4": "0", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (4, 10, null, 2, 19, '{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (5, 2, null, 11, 16, '{"1": "2", "2": "3", "3": "4", "4": "3", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (6, 2, null, 11, 17, '{"1": "1", "2": "2", "3": "1", "4": "4", "na": "1"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (7, 2, null, 11, 18, '{"1": "1", "2": "1", "3": "2", "4": "1", "na": "2"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (8, 2, null, 11, 19, '{"1": "2", "2": "1", "3": "1", "4": "5", "na": "3"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (9, 2, 25, null, 16, '{"1": "1", "2": "0", "3": "0", "4": "1", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (10, 2, 25, null, 17, '{"1": "0", "2": "1", "3": "0", "4": "1", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (11, 2, 25, null, 18, '{"1": "0", "2": "0", "3": "1", "4": "1", "na": "0"}');
INSERT INTO gestione_corsi.valutazioni_docenti (valutazioni_docenti_id, corso, docente, interno, criterio, valore) VALUES (12, 2, 25, null, 19, '{"1": "0", "2": "0", "3": "0", "4": "1", "na": "0"}');


INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (1, 2, 2, 10, 5);
INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (2, 2, 2, 11, 4);
INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (3, 2, 2, 12, 3);
INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (4, 2, 2, 13, 2);
INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (5, 2, 2, 14, 1);
INSERT INTO gestione_corsi.valutazione_utenti (valutazioni_utenti_id, corso, dipendente, criterio, valore) VALUES (6, 2, 2, 15, 5);