package it.its.projectwork.dfm.gestionecorsifrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionecorsifrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionecorsifrontendApplication.class, args);
	}

}
