package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CompaniesDto;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.CompaniesService;

@RestController
@RequestMapping(value = "api/companies")
public class CompanyController {
	
	@Autowired
	private CompaniesService companiesService;
	
	/**
	 * GET ALL
	 * Try on POSTMAN http://localhost:4000/api/companies
	 * @return BaseResponseDto<List<CompaniesDto>>
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<List<CompaniesDto>> getAll() {
		BaseResponseDto<List<CompaniesDto>> result = new BaseResponseDto<>();
		result.setResponse(companiesService.getAll());
		return result;
	}
	
	/**
	 * GET BY ID
	 * Try on POSTMAN: http://localhost:4000/api/companies/{id}
	 * @return BaseResponseDto<CompaniesDto>
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<CompaniesDto> getById(@PathVariable("id") Integer id) {
		BaseResponseDto<CompaniesDto> result = new BaseResponseDto<>();
		result.setResponse(companiesService.getById(id));
		return result;
	}

}
