package it.its.projectwork.dfm.gestionecorsi.dao.dictionaries;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tipologie")
@Data
public class TypologiesDao {

	@Id
	@Column(name = "tipologia_id")
	private Integer tipologiaId;
	
	@Column(name = "descrizione")
	private String descrizione;
	
}
