package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksCoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ParametersMarksDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface IMarksCoursesService {

	// insert a new record
	EServiceResponse insert(List<MarksCoursesDto> dto);
	
	// get all records
	PageableDto<List<MarksCoursesDto>> getAll(int startPage, int resultsPerPage, Order order);
	
	// get a record by id
	MarksCoursesDto getById(int id);
	
	// get records by course
	PageableDto<List<MarksCoursesDto>> getByCourse(int course, int page, int resultsPerPage, Order order);
	
	// delete a record by id
	EServiceResponse deleteById(int id);
		
	// delete a record by corso
	EServiceResponse deleteByCorso(int corso);
	
	// transcribe dao into dto
	void daoToDto(MarksCoursesDao dao, MarksCoursesDto dto) throws JsonMappingException, JsonProcessingException;
	
	// transcribe dto into dao
	void dtoToDao(MarksCoursesDto dto, MarksCoursesDao dao);
	
	// count
	long count();
	
	// get all parameters mark-course
	List<ParametersMarksDto> getAllParamsByTipo();

	long countDistinct();

	PageableDto<List<CoursesDto>> getCoursesWithMissingMark(Object field, int startPage, int resultsPerPage,
			Order order);
	
	
}
