package it.its.projectwork.dfm.gestionecorsi.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.its.projectwork.dfm.gestionecorsi.dao.CoursesDao;

public interface CoursesRepository extends JpaRepository<CoursesDao, Integer>{

	@Query("SELECT C FROM CoursesDao C WHERE "
			+ "(C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%) AND C.trashed IS NULL")
	Page<CoursesDao> findNotTByField(Object field, Pageable page);

	@Query("SELECT COUNT(*) FROM CoursesDao C WHERE "
			+ "(C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%) AND C.trashed IS NULL")
	long countNotTByField(Object field);

	List<CoursesDao> findFirst5ByDataErogazioneGreaterThanEqualOrderByDataErogazioneAsc(Date date);

	List<CoursesDao> findFirst5ByDataChiusuraLessThanEqualOrderByDataChiusuraDesc(Date date);

	@Query("SELECT C FROM CoursesDao C WHERE "
			+ "C.dataErogazione < ?1 AND "
			+ "C.dataChiusura > ?1 ORDER BY C.dataErogazione ASC")
	List<CoursesDao> findFirst5OngoingOrderByDataErogazioneAsc(Date date, Pageable page);
	
	Page<CoursesDao> findByTipoAndTrashed(Integer tipo, Date trashed, Pageable page);

	long countByTipo(Integer tipo);

	@Query("SELECT COUNT(*) FROM CoursesDao C WHERE "
			+ "(C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%) AND C.trashed IS NOT NULL")
	long countTrashedByField(Object field);
	
	@Query("SELECT C FROM CoursesDao C WHERE "
			+ "(C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%) AND C.trashed IS NOT NULL")
	Page<CoursesDao> findTrashedByField(Object field, Pageable page);

	@Query("SELECT COUNT(*) FROM CoursesDao C WHERE C.trashed IS NOT NULL")
	long countTrashedAll();
	
	@Query("SELECT COUNT(*) FROM CoursesDao C WHERE C.trashed IS NULL")
	long countNotTAll();
	
	@Query("SELECT C FROM CoursesDao C WHERE C.trashed IS NULL")
	Page<CoursesDao> findNotTAll(Pageable page);
	
	@Query("SELECT C FROM CoursesDao C WHERE C.trashed IS NOT NULL")
	Page<CoursesDao> findTrashedAll(Pageable page);
	
	@Query("SELECT C FROM CoursesDao C "
			+ "LEFT JOIN it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao MC ON C.corsoId = MC.corso "
			+ "WHERE MC.corso IS NULL AND "
			+ "C.dataChiusura <= ?1 "
			+ "ORDER BY C.dataChiusura DESC")
	Page<CoursesDao> findWithMissingMark(Date date, Pageable page);
	
	@Query("SELECT COUNT(*) FROM CoursesDao C "
			+ "LEFT JOIN it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao MC ON C.corsoId = MC.corso "
			+ "WHERE MC.corso IS NULL AND "
			+ "C.dataChiusura <= ?1 "
			+ "ORDER BY C.dataChiusura DESC")
	long countWithMissingMark(Date date);
	
	@Query("SELECT C FROM CoursesDao C "
			+ "LEFT JOIN it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao MC ON C.corsoId = MC.corso "
			+ "WHERE MC.corso IS NULL AND "
			+ "C.dataChiusura <= ?2 AND ("
			+ "C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%)"
			+ "ORDER BY C.dataChiusura DESC")
	Page<CoursesDao> searchWithMissingMark(Object field, Date date, Pageable page);
	
	@Query("SELECT COUNT(*) FROM CoursesDao C "
			+ "LEFT JOIN it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao MC ON C.corsoId = MC.corso "
			+ "WHERE MC.corso IS NULL AND "
			+ "C.dataChiusura <= ?2 AND ("
			+ "C.typesDao.descrizione LIKE %?1% OR "
			+ "C.typologiesDao.descrizione LIKE %?1% OR "
			+ "C.descrizione LIKE %?1% OR "
			+ "C.edizione LIKE %?1% OR "
			+ "C.durata LIKE %?1% OR "
			+ "C.note LIKE %?1% OR "
			+ "C.luogo LIKE %?1% OR "
			+ "C.ente LIKE %?1% OR "
			+ "C.dataErogazione LIKE %?1% OR "
			+ "C.dataChiusura LIKE %?1% OR "
			+ "C.dataCensimento LIKE %?1%)"
			+ "ORDER BY C.dataChiusura DESC")
	long searchCountWithMissingMark(Object field, Date date);
	
}
