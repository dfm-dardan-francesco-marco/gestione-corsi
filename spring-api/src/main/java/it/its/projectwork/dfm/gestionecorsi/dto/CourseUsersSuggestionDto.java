package it.its.projectwork.dfm.gestionecorsi.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseUsersSuggestionDto {

	@NotNull
	private Integer dipendente;
	
	@NotNull
	private String visualizza;
	
	@NotNull
	private Integer durata;
	
}
