package it.its.projectwork.dfm.gestionecorsi.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseTeachersSuggestionDto {

	private int docente;
	
	private int dipendente;
	
	@NotNull
	private String visualizza;
	
}
