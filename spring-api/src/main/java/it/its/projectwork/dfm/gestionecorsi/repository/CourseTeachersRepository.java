package it.its.projectwork.dfm.gestionecorsi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.its.projectwork.dfm.gestionecorsi.dao.CourseTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto;

public interface CourseTeachersRepository extends JpaRepository<CourseTeachersDao, Integer> {

	// ------------FIND BY CORSO------------
	Page<CourseTeachersDao> findByCorso(int course, Pageable page);

	// ------------DELETE BY CORSO------------
	void deleteByCorso(int course);
	
	// ------------COUNT BY CORSO------------
	long countByCorso(int course);
	
	// ------------FIND BY TEACHER------------
	Page<CourseTeachersDao> findByDocente(int teacher, Pageable page);
	
	// ------------FIND BY EMPLOYEE------------
	Page<CourseTeachersDao> findByInterno(int employee, Pageable page);
	
	@Query(value="SELECT DISTINCT interno FROM CourseTeachersDao WHERE interno <> null")
	List<Integer> findAllDipendenteDistinct(Pageable page);
	
	@Query(value="SELECT COUNT(DISTINCT interno) FROM CourseTeachersDao WHERE interno <> null")
	Integer countInternoDistinct();
	
	// ------------COUNT BY TEACHER------------
	long countByDocente(int teacher);
	
	// ------------COUNT BY EMPLOYEE------------
	long countByInterno(int employee);

	@Query(value = "SELECT new it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto(t.docenteId, 0, t.ragioneSociale) "
				 + "FROM it.its.projectwork.dfm.gestionecorsi.dao.TeachersDao t WHERE t.ragioneSociale LIKE ?1 ")
	List<CourseTeachersSuggestionDto> findTeachers(String value, Pageable page);

	@Query(value = "SELECT new it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto(0, e.dipendenteId, CONCAT(e.cognome, ' ', e.nome)) "
				 + "FROM it.its.projectwork.dfm.gestionecorsi.dao.EmployeesDao e WHERE e.nome LIKE ?1 OR e.cognome LIKE ?1 OR CONCAT(e.cognome, ' ', e.nome) LIKE ?1 ")
	List<CourseTeachersSuggestionDto> findEmployees(String value, Pageable page);
	
	@Query("SELECT COUNT(c) FROM CourseTeachersDao c WHERE c.corso = ?1 AND (c.docente = ?2 OR c.interno = ?3)")
	byte teacherPresent(int course, Integer teacher, Integer intern);
}
