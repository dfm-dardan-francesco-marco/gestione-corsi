package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeesWithStatisticsDto extends EmployeesDto {

	private Long ongoing; // Ongoing (present) courses
	
	private Long incoming; // Incoming (future) courses
	
	private Long finished; // Finished (past) courses
	
}
