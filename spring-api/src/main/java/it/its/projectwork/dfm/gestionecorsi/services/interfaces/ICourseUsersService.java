package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.ArrayList;
import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface ICourseUsersService {

	//---------INSERT NEW RECORD---------
	ArrayList<ErrorsDto> insert(int course, List<CourseUsersSuggestionDto> dtos);
	
	//---------GET ALL RECORDS---------
	PageableDto<List<CourseUsersDto>> getAll(int startPage, int resultsPerPage, Order order);
	
	//---------GET A RECORD BY ITS ID---------
	CourseUsersDto getById(int id);
	
	//---------GET A RECORD BY CORSO---------
	PageableDto<List<CourseUsersDto>> getByCorso(int corso, int page, int resultsPerPage, Order order, Object search);

	//---------GET A RECORD BY EMPLOYEE---------
	PageableDto<List<CourseUsersDto>> getByEmployee(int employee, Integer page, Integer resultsPerPage, Order order);
	
	//---------UPDATE A RECORD---------
	EServiceResponse update(CourseUsersDto dto, int id);
	
	//---------DELETE BY ID---------
	EServiceResponse deleteById(int id);
	
	//---------DELETE BY CORSO---------
	EServiceResponse deleteByCorso(int id);
	
	//---------TRANSCRIBE DAO INTO DTO---------
	void daoToDto(CourseUsersDao dao, CourseUsersDto dto);
		
	//---------TRANSCRIBE DTO INTO DAO---------
	void dtoToDao(CourseUsersDto dto, CourseUsersDao dao);
	
	long count();

	List<CourseUsersSuggestionDto> getEmployees(String value);

}
