package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.dao.CompaniesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CompaniesDto;
import it.its.projectwork.dfm.gestionecorsi.repository.CompaniesRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ICompaniesService;

@Service
public class CompaniesService implements ICompaniesService {
	
	@Autowired
	private CompaniesRepository companiesRepository;

	/**
	 * Get all companies
	 * @return all companies
	 */
	@Override
	public List<CompaniesDto> getAll() {
		try {
			List<CompaniesDto> companies = new ArrayList<>();
			List<CompaniesDao> companiesDao = companiesRepository.findAll();
			for (CompaniesDao dao : companiesDao) {
				CompaniesDto dto = new CompaniesDto();
				daoToDto(dao, dto);
				companies.add(dto);
			}
			return companies;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Get a company by ID
	 * @param id
	 * @return CompaniesDto
	 */
	public CompaniesDto getById(Integer id) {
		try {
			CompaniesDao dao = companiesRepository.findById(id).get();
			CompaniesDto dto = new CompaniesDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Transcribe dao into dto.
	 * In this way the final user has no way to access directly into database.
	 * @param CompaniesDao, CompaniesDto
	 */
	@Override
	public void daoToDto(CompaniesDao dao, CompaniesDto dto) {
		dto.setSocietaId(dao.getSocietaId());
		dto.setRagioneSociale(dao.getRagioneSociale());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setLocalita(dao.getLocalita());
		dto.setProvincia(dao.getProvincia());
		dto.setNazione(dao.getNazione());
		dto.setTelefono(dao.getTelefono());
		dto.setFax(dao.getFax());
		dto.setPartitaIva(dao.getPartitaIva());
	}

}
