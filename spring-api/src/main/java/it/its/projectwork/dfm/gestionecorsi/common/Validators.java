package it.its.projectwork.dfm.gestionecorsi.common;

public class Validators {

	public static String emptyStringToNull(String s) {
		return s.equals("") ? null : s;
	}
	
	public static String trimString(String s) {
		return s.trim();
	}
	
	public static boolean trimAndNotEmptyAndNotNull(String s) {
		return s != null && !Validators.trimString(s).equals("");
	}
	
	public static String batch(String s) {
		return Validators.batch(s, true, true);
	}

	public static String batch(String s, boolean toNull, boolean trim) {
		if (toNull) s = Validators.emptyStringToNull(s);
		if (trim) s = Validators.trimString(s);
				
		return s;
	}
	
}
