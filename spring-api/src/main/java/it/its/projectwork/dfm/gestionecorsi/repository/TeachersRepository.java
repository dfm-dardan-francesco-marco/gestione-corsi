package it.its.projectwork.dfm.gestionecorsi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.TeachersDao;

@Repository
public interface TeachersRepository extends JpaRepository<TeachersDao, Integer>{

	@Query("SELECT T FROM TeachersDao T WHERE "
			+ "(T.titolo LIKE %?1% OR "
			+ "T.ragioneSociale LIKE %?1% OR "
			+ "T.indirizzo LIKE %?1% OR "
			+ "T.localita LIKE %?1% OR "
			+ "T.provincia LIKE %?1% OR "
			+ "T.nazione LIKE %?1% OR "
			+ "T.telefono LIKE %?1% OR "
			+ "T.fax LIKE %?1% OR "
			+ "T.email LIKE %?1% OR "
			+ "T.noteGeneriche LIKE %?1% OR "
			+ "T.partitaIva LIKE %?1% OR "
			+ "T.referente LIKE %?1%) AND T.trashed IS NULL")
	Page<TeachersDao> findNotTByField(Object field, Pageable page);

	@Query("SELECT COUNT(*) FROM TeachersDao T WHERE "
			+ "(T.titolo LIKE %?1% OR "
			+ "T.ragioneSociale LIKE %?1% OR "
			+ "T.indirizzo LIKE %?1% OR "
			+ "T.localita LIKE %?1% OR "
			+ "T.provincia LIKE %?1% OR "
			+ "T.nazione LIKE %?1% OR "
			+ "T.telefono LIKE %?1% OR "
			+ "T.fax LIKE %?1% OR "
			+ "T.email LIKE %?1% OR "
			+ "T.noteGeneriche LIKE %?1% OR "
			+ "T.partitaIva LIKE %?1% OR "
			+ "T.referente LIKE %?1%) AND T.trashed IS NULL")
	long countNotTByField(Object field);
	
	@Query("SELECT T FROM TeachersDao T WHERE T.trashed IS NOT NULL")
	Page<TeachersDao> findTrashedAll(Pageable page);
	
	@Query("SELECT T FROM TeachersDao T WHERE T.trashed IS NULL")
	Page<TeachersDao> findNotTAll(Pageable page);

	@Query("SELECT COUNT(*) FROM TeachersDao T WHERE T.trashed IS NOT NULL")
	long countTrashedAll();
	
	@Query("SELECT COUNT(*) FROM TeachersDao T WHERE T.trashed IS NULL")
	long countNotTAll();
	
	@Query("SELECT COUNT(*) FROM TeachersDao T WHERE "
			+ "(T.titolo LIKE %?1% OR "
			+ "T.ragioneSociale LIKE %?1% OR "
			+ "T.indirizzo LIKE %?1% OR "
			+ "T.localita LIKE %?1% OR "
			+ "T.provincia LIKE %?1% OR "
			+ "T.nazione LIKE %?1% OR "
			+ "T.telefono LIKE %?1% OR "
			+ "T.fax LIKE %?1% OR "
			+ "T.email LIKE %?1% OR "
			+ "T.noteGeneriche LIKE %?1% OR "
			+ "T.partitaIva LIKE %?1% OR "
			+ "T.referente LIKE %?1%) AND T.trashed IS NOT NULL")
	long countTrashedByField(Object field);
	
	@Query("SELECT T FROM TeachersDao T WHERE "
			+ "(T.titolo LIKE %?1% OR "
			+ "T.ragioneSociale LIKE %?1% OR "
			+ "T.indirizzo LIKE %?1% OR "
			+ "T.localita LIKE %?1% OR "
			+ "T.provincia LIKE %?1% OR "
			+ "T.nazione LIKE %?1% OR "
			+ "T.telefono LIKE %?1% OR "
			+ "T.fax LIKE %?1% OR "
			+ "T.email LIKE %?1% OR "
			+ "T.noteGeneriche LIKE %?1% OR "
			+ "T.partitaIva LIKE %?1% OR "
			+ "T.referente LIKE %?1%) AND T.trashed IS NOT NULL")
	Page<TeachersDao> findTrashedByField(Object field, Pageable page);
	
}
