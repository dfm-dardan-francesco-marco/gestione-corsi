package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.common.Validators;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dao.CoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.TeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.TeachersDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.TeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ITeachersService;

@Service
public class TeachersService implements ITeachersService {

	@Autowired
	private TeachersRepository teachersRepository;

	@Autowired
	private CoursesRepository coursesRepository;

	@Autowired
	private CoursesService coursesService;

	@Autowired
	private CourseTeachersRepository coursesTeachersRepository;

	/**
	 * Insert a new teacher
	 * 
	 * @param TeacherDto
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse insert(TeachersDto dto) {
		if (dto == null)
			return EServiceResponse.EMPTY_DTO;
		try {
			TeachersDao dao = new TeachersDao();
			dtoToDao(dto, dao, false);
			teachersRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.CREATE_FAILED;
		}
	}

	/**
	 * Get all teachers
	 * 
	 * @param Page    to return
	 * @param Results per page
	 * @return all teachers
	 */
	@Override
	public PageableDto<List<TeachersDto>> getAll(int page, int resultsPerPage, Order order, boolean trashed) {
		try {
			List<TeachersDto> dtos = new ArrayList<>();
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make())
					: PageRequest.of(page, resultsPerPage);
			if (trashed) {
				Page<TeachersDao> teachersDao = teachersRepository.findTrashedAll(pageable);
				for (TeachersDao dao : teachersDao) {
					System.out.println(dao.getRagioneSociale());
					TeachersDto dto = new TeachersDto();
					daoToDto(dao, dto);
					dtos.add(dto);
				}
			} else {
				Page<TeachersDao> teachersDao = teachersRepository.findNotTAll(pageable);
				for (TeachersDao dao : teachersDao) {
					TeachersDto dto = new TeachersDto();
					daoToDto(dao, dto);
					dtos.add(dto);
				}
			}
			return new PageableFactory<List<TeachersDto>>().create(
					trashed ? teachersRepository.countTrashedAll() : teachersRepository.countNotTAll(), resultsPerPage,
					page, dtos);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * Get teacher by ID
	 * 
	 * @param id
	 * @return TeachersDto
	 */
	@Override
	public TeachersDto getById(int id) {
		try {
			TeachersDao dao = teachersRepository.findById(id).get();
			TeachersDto dto = new TeachersDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public ServiceResponse<PageableDto<List<CoursesDto>>> getByTeacher(Integer id, int page, int resultsPerPage,
			Order order) {
		try {
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make())
					: PageRequest.of(page, resultsPerPage);

			Page<CourseTeachersDao> daos = coursesTeachersRepository.findByDocente(id, pageable);
			List<CoursesDto> cDtos = new ArrayList<>();
			int count = 0;
			for (CourseTeachersDao dao : daos) {
				Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
				if (cDao.get().getTrashed() == null) {
					CoursesDto cDto = new CoursesDto();
					coursesService.daoToDto(cDao.get(), cDto);
					cDtos.add(cDto);
				} else {
					count++;
				}
			}
			return new ServiceResponse<PageableDto<List<CoursesDto>>>(EServiceResponse.OK,
					new PageableFactory<List<CoursesDto>>().create(coursesTeachersRepository.countByDocente(id) - count,
							resultsPerPage, page, cDtos));
		} catch (Exception e) {
			return new ServiceResponse<PageableDto<List<CoursesDto>>>(EServiceResponse.READ_FAILED);
		}
	}

	/**
	 * Get teacher by Ragione Sociale
	 * 
	 * @param Ragione Sociale
	 * @param Page    to return
	 * @param Results per page
	 * @return List<TeachersDto>
	 */
	@Override
	public PageableDto<List<TeachersDto>> getByField(Object field, int page, int resultsPerPage, Order order,
			boolean trashed) {
		try {
			List<TeachersDto> teachers = new ArrayList<>();
			List<TeachersDto> teachersTrashed = new ArrayList<>();
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make())
					: PageRequest.of(page, resultsPerPage);

			if (trashed) {
				Page<TeachersDao> teacherDao = teachersRepository.findTrashedByField(field, pageable);
				for (TeachersDao dao : teacherDao) {
					TeachersDto dto = new TeachersDto();
					daoToDto(dao, dto);
					teachersTrashed.add(dto);
				}
			} else {
				Page<TeachersDao> teacherDao = teachersRepository.findNotTByField(field, pageable);
				for (TeachersDao dao : teacherDao) {
					TeachersDto dto = new TeachersDto();
					daoToDto(dao, dto);
					teachers.add(dto);
				}
			}

			return new PageableFactory<List<TeachersDto>>().create(
					trashed ? teachersRepository.countTrashedByField(field) : teachersRepository.countNotTByField(field),
					resultsPerPage, page, trashed ? teachersTrashed : teachers);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * Update Teacher
	 * 
	 * @param TeachersDto, ID
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse update(TeachersDto dto, int id) {
		if (dto == null)
			return EServiceResponse.EMPTY_DTO;
		if (id == 0)
			return EServiceResponse.EMPTY_ID;
		try {
			TeachersDao dao = teachersRepository.getOne(id);
			dtoToDao(dto, dao, false);
			teachersRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	/**
	 * Delete a teacher
	 * 
	 * @param id_docente
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse delete(int id) {
		if (id == 0)
			return EServiceResponse.EMPTY_ID;
		try {
			teachersRepository.deleteById(id);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	/**
	 * Transcribe dao into dto. In this way the final user has no way to access
	 * directly into database.
	 * 
	 * @param TeachersDao, TeachersDto
	 */
	@Override
	public void daoToDto(TeachersDao dao, TeachersDto dto) {
		dto.setDocenteId(dao.getDocenteId());
		dto.setTitolo(dao.getTitolo());
		dto.setRagioneSociale(dao.getRagioneSociale());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setLocalita(dao.getLocalita());
		dto.setProvincia(dao.getProvincia());
		dto.setNazione(dao.getNazione());
		dto.setTelefono(dao.getTelefono());
		dto.setFax(dao.getFax());
		dto.setEmail(dao.getEmail());
		dto.setNoteGeneriche(dao.getNoteGeneriche());
		dto.setPartitaIva(dao.getPartitaIva());
		dto.setReferente(dao.getReferente());
		dto.setTrashed(dao.getTrashed());
	}

	/**
	 * Transcribe dto into dao. It's used to save data into database.
	 * 
	 * @param TeachersDao, TeachersDto
	 */
	@Override
	public void dtoToDao(TeachersDto dto, TeachersDao dao, boolean acceptMissing) {
		if (!acceptMissing) {
			dao.setTitolo(dto.getTitolo());
			dao.setRagioneSociale(Validators.batch(dto.getRagioneSociale()));
			dao.setIndirizzo(Validators.batch(dto.getIndirizzo()));
			dao.setLocalita(Validators.batch(dto.getLocalita()));
			dao.setProvincia(Validators.batch(dto.getProvincia()));
			dao.setNazione(Validators.batch(dto.getNazione()));
			dao.setTelefono(Validators.batch(dto.getTelefono()));
			dao.setFax(dto.getFax());
			dao.setEmail(Validators.batch(dto.getEmail()));
			dao.setNoteGeneriche(dto.getNoteGeneriche());
			dao.setPartitaIva(Validators.batch(dto.getPartitaIva()));
			dao.setReferente(Validators.batch(dto.getReferente()));
			dao.setTrashed(dto.getTrashed());
		} else {
			if (Validators.trimAndNotEmptyAndNotNull(dto.getTitolo()))
				dao.setTitolo(dto.getTitolo());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getRagioneSociale()))
				dao.setRagioneSociale(dto.getRagioneSociale());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getIndirizzo()))
				dao.setIndirizzo(dto.getIndirizzo());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getLocalita()))
				dao.setLocalita(dto.getLocalita());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getProvincia()))
				dao.setProvincia(dto.getProvincia());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getNazione()))
				dao.setNazione(dto.getNazione());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getTelefono()))
				dao.setTelefono(dto.getTelefono());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getFax()))
				dao.setFax(dto.getFax());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getEmail()))
				dao.setEmail(dto.getEmail());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getNoteGeneriche()))
				dao.setNoteGeneriche(dto.getNoteGeneriche());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getPartitaIva()))
				dao.setPartitaIva(dto.getPartitaIva());
			if (Validators.trimAndNotEmptyAndNotNull(dto.getReferente()))
				dao.setReferente(dto.getReferente());
		}
	}

	/**
	 * Trash or restore a teacher by ID
	 * 
	 * @param id, TeachersDto
	 * @return BaseResponseDto<ServiceResponse>
	 */
	@Override
	public EServiceResponse setTrashed(TeachersDto dto, int id, boolean trashed) {
		if (dto == null)
			return EServiceResponse.EMPTY_DTO;
		if (id == 0)
			return EServiceResponse.EMPTY_ID;
		try {
			TeachersDao dao = teachersRepository.getOne(id);
			if (trashed) {
				dto.setTrashed(new Date());
			} else {
				dto.setTrashed(null);
			}
			dtoToDao(dto, dao, false);
			teachersRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	public long count() {
		return teachersRepository.count();
	}

	public long countNotTAll() {
		return teachersRepository.countNotTAll();
	}

}
