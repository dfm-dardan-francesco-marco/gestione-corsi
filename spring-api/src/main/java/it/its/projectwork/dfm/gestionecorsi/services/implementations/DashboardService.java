package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.DashboardDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IDashboardService;

@Service
public class DashboardService implements IDashboardService {

	@Autowired
	private TeachersService teachersService;

	@Autowired
	private EmployeesService employeesService;

	@Autowired
	private CoursesService coursesService;

	@Autowired
	private MarksCoursesService marksCoursesService;
	
	@Override
	public ServiceResponse<DashboardDto> getCounters() {
		return new ServiceResponse<DashboardDto>(EServiceResponse.OK, new DashboardDto(teachersService.count(), employeesService.count(), coursesService.count(), marksCoursesService.countDistinct()));
	}

}
