package it.its.projectwork.dfm.gestionecorsi.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MarksUsersDto {
	
	@NotNull
	private Integer valutazioniUtentiId;
	
	@NotNull
	private Integer corso;
	
	@NotNull
	private Integer dipendente;
	
	@NotNull
	private String criterio;
	
	@NotNull
	private Integer valore;
}
