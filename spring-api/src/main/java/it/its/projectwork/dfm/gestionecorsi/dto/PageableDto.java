package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageableDto<T> {

	private long totalCount;
	
	private long resultsPerPage;
	
	private long pages;
	
	private long currentPage;
	
	T data;
	
}
