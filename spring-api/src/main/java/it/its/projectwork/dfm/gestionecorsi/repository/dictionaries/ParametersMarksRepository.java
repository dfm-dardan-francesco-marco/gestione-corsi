package it.its.projectwork.dfm.gestionecorsi.repository.dictionaries;


import org.springframework.data.jpa.repository.JpaRepository;

import it.its.projectwork.dfm.gestionecorsi.dao.ParametersMarksDao;

public interface ParametersMarksRepository extends JpaRepository<ParametersMarksDao, Integer> {

	long countByTipo(String tipo);
	
}
