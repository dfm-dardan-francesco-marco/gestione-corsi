package it.its.projectwork.dfm.gestionecorsi.common;

import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;

public class ResponseFactory<T> {

	public BaseResponseDto<ServiceResponse<T>> make(ServiceResponse<T> serResp) {
		BaseResponseDto<ServiceResponse<T>> response = new BaseResponseDto<ServiceResponse<T>>();
		
		response.setResponse(serResp);
		response.setSuccess(serResp.getResponse().value() >= 1);
		response.setError(serResp.getResponse().value() <= -1);
		
		return response;
	}
	
}
