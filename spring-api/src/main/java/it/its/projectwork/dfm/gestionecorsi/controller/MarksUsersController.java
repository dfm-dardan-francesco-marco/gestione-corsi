package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.MarksUsersService;

@RestController
@RequestMapping(value = "api/marks-users")
public class MarksUsersController {

	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private MarksUsersService marksUsersService;
	
	/**
	 * INSERT new record
	 * Try on POSTMAN: http://localhost:4000/api/marks-users
	 */
	@PostMapping( produces = "application/json")
	private BaseResponseDto<EServiceResponse> insertMarksUsers(@RequestBody(required = false) List<MarksUsersDto> marks_users) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksUsersService.insert(marks_users);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET by course or employee
	 * Try on POSTMAN: http://localhost:4000/api/marks-users
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<MarksUsersDto>>> getAllMarksUsers(
			@RequestParam(required = false) Integer course,
			@RequestParam(required = false) Integer employee,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<MarksUsersDto>>> result = new BaseResponseDto<>();
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		result.setResponse(
				course != null && employee != null ? marksUsersService.getByCourseEmployee(course, employee, page, resultsPerPage, order) :
				course != null ?
				marksUsersService.getByCourse(course, page, resultsPerPage, order) :
				employee != null ? 
						marksUsersService.getByEmployee(employee, page, resultsPerPage, order) :
							null
				);
		
		return result;
	}
	
	
	/**
	 * update a record
	 * Try on POSTMAN: http://localhost:4000/api/marks-users/{id}
	 */
	@PatchMapping(produces = "application/json")
	private BaseResponseDto<EServiceResponse> updateMarksUsers(@RequestBody(required = false) List<MarksUsersDto> marksUsersDto){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksUsersService.update(marksUsersDto);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * delete by corso and interno or docente
	 * Try on POSTMAN: http://localhost:4000/api/marks-users?corso=1&dipendente=1
	 */
	@DeleteMapping(produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteByCorsoAndDipendente(
			@RequestParam(required = false) Integer corso, @RequestParam(required = false) Integer dipendente) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksUsersService.deleteByCorso(corso, dipendente);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
}
