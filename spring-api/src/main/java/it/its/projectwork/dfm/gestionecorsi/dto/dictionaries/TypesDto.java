package it.its.projectwork.dfm.gestionecorsi.dto.dictionaries;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TypesDto {

	private Integer tipoId;
	private String descrizione;
	
}
