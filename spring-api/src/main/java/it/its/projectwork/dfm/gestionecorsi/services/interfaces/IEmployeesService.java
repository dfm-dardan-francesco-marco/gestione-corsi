package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.HashMap;
import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dao.EmployeesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesWithStatisticsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;

public interface IEmployeesService {
	
	
	// return the employees' list
	PageableDto<List<EmployeesDto>> getAll(int page, int resultsPerPage, Order order);
	
	// return the employee with the id that was passed to it
	EmployeesDto getById(int dipendente_id);
	
	// dao to dto to avoid that the end user has the possibility to access the database and tamper with the data
	void daoToDto(EmployeesDao dao, EmployeesDto dto);
	
	long count();

	ServiceResponse<PageableDto<List<CoursesDto>>> getCoursesByEmplyoeeId(Integer id, String type, int page, int resultsPerPage,
			Order order);
	
	ServiceResponse<EmployeesWithStatisticsDto> getByMatricola(String matricola);

	ServiceResponse<HashMap<String, List<CoursesDto>>> getCoursesByMatricola(String matricola);

	PageableDto<List<EmployeesDto>> getAllEmployeesTeachers(int page, int resultsPerPage, Order order);
}
