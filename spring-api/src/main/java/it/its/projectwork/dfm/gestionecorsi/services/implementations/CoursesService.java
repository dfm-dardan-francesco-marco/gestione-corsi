package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dao.CoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.MeasuresDao;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypologiesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.DictionariesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.MeasuresDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.TypesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.TypologiesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksCoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.MeasuresRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.ParametersMarksRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.TypesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.TypologiesRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ICoursesService;

@Service
public class CoursesService implements ICoursesService{
	
	@Autowired
	private CoursesRepository coursesRepository;
	
	@Autowired
	private CourseTeachersRepository courseTeachersRepository;
	
	@Autowired
	private CourseUsersRepository courseUsersRepository;
	
	@Autowired
	private MarksTeachersRepository marksTeachersRepository;
	
	@Autowired
	private MarksCoursesRepository marksCoursesRepository; 
	
	@Autowired
	private MarksUsersRepository marksUsersRepository; 
	
	@Autowired
	private ParametersMarksRepository parametersMarksRepository;
	
	@Autowired
	private CourseTeachersService courseTeachersService;
	
	@Autowired
	private CourseUsersService courseUsersService;
	
	@Autowired
	private MarksCoursesService marksCoursesService;
	
	@Autowired
	private MarksUsersService marksUsersService;
	
	@Autowired
	private MarksTeachersService marksTeachersService;
	
	@Autowired
	private TypologiesRepository typologiesRepository;
	
	@Autowired
	private TypesRepository typesRepository;
	
	@Autowired
	private MeasuresRepository measuresRepository;
	

	/**
	 * Get all courses
	 * @return all courses
	 */
	@Override
	public PageableDto<List<CoursesDto>> getAll(int page, int resultsPerPage, Order order, boolean trashed) {
		try {
			List<CoursesDto> courses = new ArrayList<>();
			List<CoursesDto> coursesTrashed = new ArrayList<>();
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make())
					: PageRequest.of(page, resultsPerPage);
			if (trashed) {
				Page<CoursesDao> coursesDao = coursesRepository.findTrashedAll(pageable);
				for (CoursesDao dao : coursesDao) {
						CoursesDto dto = new CoursesDto();
						daoToDto(dao, dto);
						coursesTrashed.add(dto);
				}
			} else {
				Page<CoursesDao> coursesDao = coursesRepository.findNotTAll(pageable);
				for (CoursesDao dao : coursesDao) {
						CoursesDto dto = new CoursesDto();
						daoToDto(dao, dto);
						courses.add(dto);
				}
			}
			return new PageableFactory<List<CoursesDto>>().create(
					trashed ? coursesRepository.countTrashedAll() : coursesRepository.countNotTAll(), 
					resultsPerPage, page, trashed ? coursesTrashed : courses);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @param field, page, resultsPerPage
	 * @return List<CoursesDto>
	 */
	@Override
	public PageableDto<List<CoursesDto>> getCoursesByField(Object field, int page, int resultsPerPage, Order order, boolean trashed) {
		try {
			List<CoursesDto> courses = new ArrayList<>();
			List<CoursesDto> coursesTrashed = new ArrayList<>();
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make()) : PageRequest.of(page, resultsPerPage);
			
			if (trashed) {
				Page<CoursesDao> coursesDao = coursesRepository.findTrashedByField(field, pageable);
				for (CoursesDao dao : coursesDao) {
					CoursesDto dto = new CoursesDto();
					daoToDto(dao, dto);
					coursesTrashed.add(dto);
				}
			} else {
				Page<CoursesDao> coursesDao = coursesRepository.findNotTByField(field, pageable);
				for (CoursesDao dao : coursesDao) {
					CoursesDto dto = new CoursesDto();
					daoToDto(dao, dto);
					courses.add(dto);
				}
			}
			return new PageableFactory<List<CoursesDto>>().create(
					trashed ? coursesRepository.countTrashedByField(field) : coursesRepository.countNotTByField(field), 
							resultsPerPage, page, trashed ? coursesTrashed : courses);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Get course by ID
	 * @param id
	 * @return Coursesdto
	 */
	@Override
	public CoursesDto getById(int id) {
		try {
			CoursesDto dto = new CoursesDto();
			CoursesDao dao = coursesRepository.findById(id).get();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Delete a course
	 * @param id
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse delete(int id) {
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			courseTeachersService.deleteByCorso(id);
			courseUsersService.deleteByCorso(id);
			marksCoursesService.deleteByCorso(id);
			marksTeachersService.deleteByCorso(id, 0, 0);
			marksUsersService.deleteByCorso(id, 0);
			coursesRepository.deleteById(id);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	/**
	 * Insert a new course
	 * @param CoursesDto
	 * @return ServiceResponse
	 */
	@Override
	public ServiceResponse<Integer> insert(CoursesDto dto) {
		if(dto == null) return new ServiceResponse<Integer>(EServiceResponse.EMPTY_DTO);
		try {
			CoursesDao dao = new CoursesDao();
			dtoToDao(dto, dao);
			dao = coursesRepository.save(dao);			
			return new ServiceResponse<Integer>(EServiceResponse.OK, dao.getCorsoId());
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return new ServiceResponse<Integer>(EServiceResponse.DATA_INTEGRITY_ERROR);
		} catch (Exception e) {
			return new ServiceResponse<Integer>(EServiceResponse.CREATE_FAILED);
		}
	}
	
	/**
	 * update a course
	 * @param CoursesDto, id
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse update(CoursesDto dto, Integer id) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			CoursesDao dao = coursesRepository.getOne(id);
			dtoToDao(dto, dao);
			coursesRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	private void dtoToDao(CoursesDto dto, CoursesDao dao) {
		dao.setTipo(dto.getTipo());
		dao.setTipologia(dto.getTipologia());
		dao.setDescrizione(dto.getDescrizione());
		dao.setEdizione(dto.getEdizione());
		dao.setPrevisto(dto.isPrevisto());
		dao.setErogato(dto.isErogato());
		dao.setDurata(dto.getDurata());
		dao.setMisura(dto.getMisura());
		dao.setNote(dto.getNote());
		dao.setLuogo(dto.getLuogo());
		dao.setEnte(dto.getEnte());
		dao.setSocieta(dto.getSocieta());
		dao.setDataErogazione(dto.getDataErogazione());
		dao.setDataChiusura(dto.getDataChiusura());
		dao.setDataCensimento(dto.getDataCensimento());
		dao.setInterno(dto.isInterno());
		dao.setTrashed(dto.getTrashed());
		dao.setValutazioni(dto.getValutazioni());
	}
	
	public void daoToDto(CoursesDao dao, CoursesDto dto) {
		dto.setCorsoId(dao.getCorsoId());
		dto.setTipo(dao.getTipo());
		dto.setTipologia(dao.getTipologia());
		dto.setDescrizione(dao.getDescrizione());
		dto.setEdizione(dao.getEdizione());
		dto.setPrevisto(dao.isPrevisto());
		dto.setErogato(dao.isErogato());
		dto.setDurata(dao.getDurata());
		dto.setMisura(dao.getMisura());
		dto.setNote(dao.getNote());
		dto.setLuogo(dao.getLuogo());
		dto.setEnte(dao.getEnte());
		dto.setSocieta(dao.getSocieta());
		dto.setDataErogazione(dao.getDataErogazione());
		dto.setDataChiusura(dao.getDataChiusura());
		dto.setDataCensimento(dao.getDataCensimento());
		dto.setInterno(dao.isInterno());
		dto.setTrashed(dao.getTrashed());
		dto.setValutazioni(dao.getValutazioni());
	}
	
	public long count() {
		return coursesRepository.count();
	}

	@Override
	public List<CoursesDto> getIncoming() {
		try {
			List<CoursesDto> coursesDto = new ArrayList<>();
			List<CoursesDao> coursesDao = coursesRepository.findFirst5ByDataErogazioneGreaterThanEqualOrderByDataErogazioneAsc(new Date());
			for (CoursesDao dao : coursesDao) {
				CoursesDto dto = new CoursesDto();
				daoToDto(dao, dto);
				coursesDto.add(dto);
			}
			return coursesDto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<CoursesDto> getOngoing() {
		try {
			List<CoursesDto> coursesDto = new ArrayList<>();
			List<CoursesDao> coursesDao = coursesRepository.findFirst5OngoingOrderByDataErogazioneAsc(new Date(), PageRequest.of(0, 5));
			for (CoursesDao dao : coursesDao) {
				CoursesDto dto = new CoursesDto();
				daoToDto(dao, dto);
				coursesDto.add(dto);
			}
			return coursesDto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<CoursesDto> getFinished() {
		try {
			List<CoursesDto> coursesDto = new ArrayList<>();
			List<CoursesDao> coursesDao = coursesRepository.findFirst5ByDataChiusuraLessThanEqualOrderByDataChiusuraDesc(new Date());
			for (CoursesDao dao : coursesDao) {
				CoursesDto dto = new CoursesDto();
				daoToDto(dao, dto);
				coursesDto.add(dto);
			}
			return coursesDto;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public ServiceResponse<DictionariesDto> getDictionaries() {
		try {
			List<TypologiesDao> typologiesDaos = typologiesRepository.findAllByOrderByDescrizione();
			List<TypologiesDto> typologiesDtos = new ArrayList<>();
			for (TypologiesDao dao : typologiesDaos) {
				typologiesDtos.add(new TypologiesDto(dao.getTipologiaId(), dao.getDescrizione()));
			}

			List<TypesDao> typesDaos = typesRepository.findAllByOrderByDescrizione();
			List<TypesDto> typesDtos = new ArrayList<>();
			for (TypesDao dao : typesDaos) {
				typesDtos.add(new TypesDto(dao.getTipoId(), dao.getDescrizione()));
			}

			List<MeasuresDao> measuresDaos = measuresRepository.findAllByOrderByDescrizione();
			List<MeasuresDto> measuresDtos = new ArrayList<>();
			for (MeasuresDao dao : measuresDaos) {
				measuresDtos.add(new MeasuresDto(dao.getMisuraId(), dao.getDescrizione()));
			}
			
			DictionariesDto dto = new DictionariesDto(typologiesDtos, typesDtos, measuresDtos);
			return new ServiceResponse<DictionariesDto>(EServiceResponse.OK, dto);
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return new ServiceResponse<DictionariesDto>(EServiceResponse.DATA_INTEGRITY_ERROR);
		} catch (Exception e) {
			return new ServiceResponse<DictionariesDto>(EServiceResponse.CREATE_FAILED);
		}
	}

	@Override
	public PageableDto<List<CoursesDto>> getCoursesByType(Integer tipo, Integer page, Integer resultsPerPage, Order order) {
		try {
			List<CoursesDto> coursesDto = new ArrayList<>();			
			Pageable pageable = order != null ? PageRequest.of(page, resultsPerPage, order.make()) : PageRequest.of(page, resultsPerPage);
			Page<CoursesDao> coursesDao = coursesRepository.findByTipoAndTrashed(tipo, null, pageable);
			for (CoursesDao dao : coursesDao) {
				CoursesDto dto = new CoursesDto();
				daoToDto(dao, dto);
				coursesDto.add(dto);
			}
			return new PageableFactory<List<CoursesDto>>().create(coursesRepository.countByTipo(tipo), resultsPerPage, page, coursesDto);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Trash or restore a course by ID
	 * @param id, CourseDto
	 * @return BaseResponseDto<EServiceResponse>
	 */
	@Override
	public EServiceResponse setTrashed(CoursesDto dto, int id, boolean trashed) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			CoursesDao dao = coursesRepository.getOne(id);
			if (trashed && dto.getTrashed()==null) {
				dto.setTrashed(new Date());
			}
			else if(!trashed){
				dto.setTrashed(null);
			}
			dtoToDao(dto, dao);
			coursesRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	public long countNotTAll() {
		return coursesRepository.countNotTAll();
	}

	@Override
	public EServiceResponse setMarks(CoursesDto dto, int id) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			if (dto.getValutazioni() > 3) {
				return EServiceResponse.UPDATE_FAILED;
			}
			Long cMT = marksTeachersRepository.countByCorso(id);
			Long cMU = marksUsersRepository.countByCorso(id);
			Long cCT = courseTeachersRepository.countByCorso(id);
			Long cCU = courseUsersRepository.countByCorso(id);
			Long cMC = marksCoursesRepository.countByCorso(id);
			Long cCP = parametersMarksRepository.countByTipo("corso");
			boolean cT = false;
			boolean cU = false;
			boolean cC = false;
			CoursesDao dao = coursesRepository.getOne(id);
			if (cMT == (cCT * parametersMarksRepository.countByTipo("docente")) && cMT != 0) cT = true; 
			if (cMU == (cCU * parametersMarksRepository.countByTipo("dipendente")) && cMU != 0) cU = true; 
			if (cMC == cCP) cC = true; 
			System.out.println(cMT + " = " + (cCT * parametersMarksRepository.countByTipo("docente")) + " " + 
			cMU + " = " + (cCU * parametersMarksRepository.countByTipo("dipendente")) + " " + 
			cMC + " = " + cCP);
			System.out.println(cT + " " + cU + " " + cC);
			if (cT && cU && cC) {
				dto.setValutazioni(3);
			} else if ((cT && cU) || (cT && cC) || (cC && cU)) {
				dto.setValutazioni(2);
			} else if (cT || cU || cC) {
				dto.setValutazioni(1);
			} else {
				dto.setValutazioni(0);
			}
			dtoToDao(dto, dao);
			coursesRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}
	
}
