package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ResponseFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.TeachersDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.TeachersService;

@RestController
@RequestMapping(value = "api/teachers")
public class TeachersController {
	
	public static final int RESULTS_PER_PAGE = 20;

	@Autowired
	private TeachersService teacherService;
	
	/**
	 * INSERT new teacher
	 * Try on POSTMAN: http://localhost:4000/api/teachers
	 */
	@PostMapping()
	private BaseResponseDto<EServiceResponse> insertTeacher(@RequestBody(required = false) TeachersDto teacher){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		
		EServiceResponse serviceStatus = teacherService.insert(teacher);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET all the teachers
	 * Try on POSTMAN: http://localhost:4000/api/teachers
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<TeachersDto>>> getAllTeachers(
			@RequestParam(required = false) Object field,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order,
			@RequestParam(required = false) boolean trashed
			){
		BaseResponseDto<PageableDto<List<TeachersDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(
				field == null ? 
				teacherService.getAll(page, resultsPerPage, order, trashed) : 
				teacherService.getByField(field, page, resultsPerPage, order, trashed)
			);
		
		return result;
	}
	
	/**
	 * GET a teacher by ID
	 * Try on POSTMAN: http://localhost:4000/api/teachers/get/{id}
	 * @return BaseResponseDto<TeachersDto>
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<TeachersDto> getTeacherByID(@PathVariable("id") Integer id){
		BaseResponseDto<TeachersDto> result = new BaseResponseDto<>();
		
		result.setResponse(teacherService.getById(id));
		return result;
	}
	
	/**
	 * GET courses by a teacher ID
	 * Try on POSTMAN: http://localhost:4000/api/teachers/{id}/courses
	 */
	@GetMapping(value = "{id}/courses", produces = "application/json")
	private BaseResponseDto<ServiceResponse<PageableDto<List<CoursesDto>>>> getCourseTeacherById(
			@PathVariable("id") Integer id,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		return new ResponseFactory<PageableDto<List<CoursesDto>>>().make(
				teacherService.getByTeacher(id, page, resultsPerPage, order)
				);
	}
	
	/**
	 * UPDATE a teacher
	 * Try on POSTMAN: http://localhost:4000/api/teachers/{id}
	 */
	@PatchMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> updateTeacher(@PathVariable("id") Integer id, @RequestBody(required = false) TeachersDto teacher){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		
		EServiceResponse serviceStatus = teacherService.update(teacher, id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * DELETE a teacher by ID
	 * Try on POSTMAN: http://localhost:4000/api/teachers/{id}
	 */
	@DeleteMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteTeacher(@PathVariable("id") Integer id){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		
		EServiceResponse serviceStatus = teacherService.delete(id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * Trash or restore a teacher by ID
	 * Try on POSTMAN: http://localhost:4000/api/teachers/setTrashed/{id}
	 */
	@PatchMapping(value = "/setTrashed/{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> setTrashed(
			@PathVariable("id") Integer id, 
			@RequestBody(required = false) TeachersDto teacher,
			@RequestParam(required = false) boolean trashed) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = teacherService.setTrashed(teacher, id, trashed);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
}
