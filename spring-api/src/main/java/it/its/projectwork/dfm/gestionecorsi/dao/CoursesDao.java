package it.its.projectwork.dfm.gestionecorsi.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypologiesDao;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "corsi")
@Data
public class CoursesDao {
	
	@Id
	@Column(name = "corso_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int corsoId;
	
	@Column(name = "tipo")
	private int tipo;
	
	@Column(name = "tipologia")
	private int tipologia;
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@Column(name = "edizione")
	private String edizione;
	
	@Column(name = "previsto")
	private boolean previsto;
	
	@Column(name = "erogato")
	private boolean erogato;
	
	@Column(name = "durata")
	private int durata;
	
	@Column(name = "misura")
	private int misura;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "luogo")
	private String luogo;
	
	@Column(name = "ente")
	private String ente;
	
	@Column(name = "societa")
	private int societa;
	
	@Column(name = "data_erogazione")
	private Date dataErogazione;
	
	@Column(name = "data_chiusura")
	private Date dataChiusura;
	
	@Column(name = "data_censimento")
	private Date dataCensimento;
	
	@Column(name = "interno")
	private boolean interno;
	
	@Column (name = "trashed")
	private Date trashed;
	
	@Column(name = "valutazioni")
	private int valutazioni;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipologia", referencedColumnName = "tipologia_id", insertable = false, updatable = false)
	@JsonBackReference
	private TypologiesDao typologiesDao;

	@ManyToOne
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "tipo", referencedColumnName = "tipo_id", insertable = false, updatable = false)
	@JsonBackReference
	private TypesDao typesDao;
}
