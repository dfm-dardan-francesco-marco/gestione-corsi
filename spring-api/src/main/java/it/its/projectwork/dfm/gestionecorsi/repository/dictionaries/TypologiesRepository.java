package it.its.projectwork.dfm.gestionecorsi.repository.dictionaries;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypologiesDao;

public interface TypologiesRepository extends JpaRepository<TypologiesDao, Integer>{

	List<TypologiesDao> findAllByOrderByDescrizione();
	
}
