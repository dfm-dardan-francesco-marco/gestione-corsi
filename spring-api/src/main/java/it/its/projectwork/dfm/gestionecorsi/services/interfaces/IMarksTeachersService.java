package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface IMarksTeachersService {

	// insert a new record
	EServiceResponse insert(List<MarksTeachersDto> dto);
	
	// get all records
	PageableDto<List<MarksTeachersDto>> getAll(int startPage, int resultsPerPage, Order order);
	
	// get a record by id
	MarksTeachersDto getById(int id);
	
	// get records by course
	PageableDto<List<MarksTeachersDto>> getByCourse(int course, int page, int resultsPerPage, Order order);
	
	// get records by teacher
	PageableDto<List<MarksTeachersDto>> getByTeacher(int course, int teacher, int page, int resultsPerPage, Order order);
	
	// get records by interno
	PageableDto<List<MarksTeachersDto>> getByInterno(int course, int interno, int page, int resultsPerPage, Order order);
		
	// delete a record by corso
	EServiceResponse deleteByCorso(int corso, int docente, int interno);
	
	// transcribe dao into dto
	void daoToDto(MarksTeachersDao dao, MarksTeachersDto dto) throws JsonMappingException, JsonProcessingException;;
	
	// transcribe dto into dao
	void dtoToDao(MarksTeachersDto dto, MarksTeachersDao dao);
	
	// count
	long count();
	
}
