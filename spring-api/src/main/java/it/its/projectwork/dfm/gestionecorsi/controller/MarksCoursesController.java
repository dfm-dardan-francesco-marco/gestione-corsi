package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksCoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ParametersMarksDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.MarksCoursesService;

@RestController
@RequestMapping(value = "api/marks-courses")
public class MarksCoursesController {

	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private MarksCoursesService marksCoursesService;
	
	/**
	 * INSERT new record
	 * Try on POSTMAN: http://localhost:4000/api/marks-courses
	 */
	@PostMapping(produces = "application/json")
	private BaseResponseDto<EServiceResponse> insertMarksCourses(@RequestBody(required = false) List<MarksCoursesDto> marks_courses) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksCoursesService.insert(marks_courses);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET by course
	 * Try on POSTMAN: http://localhost:4000/api/marks-courses
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<MarksCoursesDto>>> getAllMarksCourses(
			@RequestParam(required = false) Integer course,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<MarksCoursesDto>>> result = new BaseResponseDto<>();
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		result.setResponse(
				course != null ?
						marksCoursesService.getByCourse(course, page, resultsPerPage, order) :
				null
				);
		
		return result;
	}
	
	/**
	 * GET course that are MISSING a mark
	 * Try on POSTMAN: http://localhost:4000/api/marks-courses/missing
	 */
	@GetMapping(value = "/missing", produces = "application/json")
	private BaseResponseDto<PageableDto<List<CoursesDto>>> getCoursesWithMissingMark(
			@RequestParam(required = false) Object field,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<CoursesDto>>> result = new BaseResponseDto<>();
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		result.setResponse(marksCoursesService.getCoursesWithMissingMark(field, page, resultsPerPage, order));
		
		return result;
	}
	
	/**
	 * GET BY ID
	 * Try on POSTMAN: http://localhost:4000/api/marks-courses/{id}
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<MarksCoursesDto> getMarksCoursesById(@PathVariable("id") Integer id) {
		BaseResponseDto<MarksCoursesDto> result = new BaseResponseDto<>();
		result.setResponse(marksCoursesService.getById(id));
		return result;
	}
	
	/**
	 * GET all parameters of valutations by tipo
	 * Try on POSTMAN: http://localhost:4000/api/marks-courses/params
	 */
	@GetMapping(value = "/params", produces = "application/json")
	private BaseResponseDto<List<ParametersMarksDto>> getParametri(){
		BaseResponseDto<List<ParametersMarksDto>> result = new BaseResponseDto<>();
		result.setResponse(marksCoursesService.getAllParamsByTipo());
		return result;
	}

}
