package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.dto.thirdParty.CountryDto;

public interface IThirdPartyService {

	List<CountryDto> getCountries();
	
}
