package it.its.projectwork.dfm.gestionecorsi.repository.dictionaries;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.MeasuresDao;

public interface MeasuresRepository extends JpaRepository<MeasuresDao, Integer>{

	List<MeasuresDao> findAllByOrderByDescrizione();
	
}
