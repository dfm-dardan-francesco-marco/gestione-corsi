package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.TypesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.TypesService;

@RestController
@RequestMapping(value = "api/types")
public class TypesController {

	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private TypesService typesService;
	
	/**
	 * INSERT a type
	 * try on POSTMAN: http://localhost:4000/api/types
	 */
	@PostMapping()
	private BaseResponseDto<EServiceResponse> insertType(@RequestBody(required = false) TypesDto type) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = typesService.insert(type);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET all types
	 * try on POSTMAN: http://localhost:4000/api/types
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<TypesDto>>> getAllTypes(
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<TypesDto>>> result = new BaseResponseDto<>();
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		result.setResponse(typesService.getAll(page, resultsPerPage, order));
		return result;
	}
	
	/**
	 * GET by id
	 * try on POSTMAN: http://localhost:4000/api/types/{id}
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<TypesDto> getTypeById(@PathVariable("id") int id) {
		BaseResponseDto<TypesDto> result = new BaseResponseDto<>();
		result.setResponse(typesService.getById(id));
		return result;
	}
	
	/**
	 * UPDATE a type
	 * try on POSTMAN: http://localhost:4000/api/types/{id}
	 */
	@PatchMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> updateType(@PathVariable("id") Integer id, @RequestBody(required = false) TypesDto typesDto){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = typesService.update(typesDto, id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * DELETE by ID
	 * try on POSTMAN: http://localhost:4000/api/types/{id}
	 */
	@DeleteMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteTypeById(@PathVariable("id") Integer id) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = typesService.deleteById(id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
}
