package it.its.projectwork.dfm.gestionecorsi.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.CourseUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersSuggestionDto;

@Repository
public interface CourseUsersRepository extends JpaRepository<CourseUsersDao, Integer>{
	
	//------------FIND BY CORSO------------
	Page<CourseUsersDao> findByCorso(int course, Pageable page);
	
	//------------FIND BY CORSO AND SEARCH------------
	@Query("SELECT CU FROM CourseUsersDao CU WHERE "
			+ "CU.corso = :course AND "
			+ "(CU.employeesDao.cognome LIKE :search OR "
			+ "CU.employeesDao.nome LIKE :search OR "
			+ "CU.durata = :search)")
	Page<CourseUsersDao> findByCorsoAndSearch(@Param("course") int course, @Param("search") Object search, Pageable page);
	
	//------------COUNT FIND BY CORSO AND SEARCH------------
	@Query("SELECT COUNT(*) FROM CourseUsersDao CU WHERE "
			+ "CU.corso = :course AND "
			+ "(CU.employeesDao.cognome LIKE :search OR "
			+ "CU.employeesDao.nome LIKE :search OR "
			+ "CU.durata = :search)")
	long countByCorsoAndSearch(@Param("course") int course, @Param("search") Object search);
	
	//------------DELETE BY CORSO------------
	void deleteByCorso(int course);

	// ------------COUNT BY CORSO------------
	long countByCorso(int course);

	// ------------FIND BY DIPENDENTE------------
	Page<CourseUsersDao> findByDipendente(int employee, Pageable page);

	// ------------COUNT BY DIPENDENTE----------
	long countByDipendente(int employee);
	

	@Query(value = "SELECT new it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersSuggestionDto(e.dipendenteId, CONCAT(e.cognome, ' ', e.nome), 0) "
				 + "FROM it.its.projectwork.dfm.gestionecorsi.dao.EmployeesDao e WHERE e.nome LIKE ?1 OR e.cognome LIKE ?1 OR CONCAT(e.cognome, ' ', e.nome) LIKE ?1 ")
	List<CourseUsersSuggestionDto> findEmployees(String value, Pageable page);

	@Query("SELECT COUNT(c) FROM CourseUsersDao c WHERE c.corso = ?1 AND c.dipendente = ?2")
	byte userPresent(int course, Integer teacher);
}
