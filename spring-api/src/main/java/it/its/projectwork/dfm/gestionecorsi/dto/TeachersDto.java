package it.its.projectwork.dfm.gestionecorsi.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeachersDto {
	
	@NotNull
	private int docenteId;
	
	private String titolo;
	
	private String ragioneSociale;
	
	private String indirizzo;
	
	private String localita;
	
	private String provincia;
	
	private String nazione;
	
	private String telefono;
	
	private String fax;
	
	private String email;
	
	private String noteGeneriche;
	
	private String partitaIva;
	
	private String referente;
	
	private Date trashed;

}
