package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table (name = "valutazione_utenti")
@Data
public class MarksUsersDao {

	@Id
	@Column(name = "valutazioni_utenti_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer valutazioniUtentiId;
	
	@Column(name = "corso")
	private Integer corso;
	
	@Column(name = "dipendente")
	private Integer dipendente;
	
	@Column(name = "criterio")
	private String criterio;
	
	@Column(name = "valore")
	private Integer valore;
	
}
