package it.its.projectwork.dfm.gestionecorsi.dto;

import java.util.HashMap;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MarksTeachersDto {

	@NotNull
	private Integer valutazionidocentiId;
	
	@NotNull
	private Integer corso;
	
	private Integer docente;
	
	private Integer interno;
	
	@NotNull
	private Integer criterio;
	
	@NotNull
	private HashMap<String, String> valore;
}
