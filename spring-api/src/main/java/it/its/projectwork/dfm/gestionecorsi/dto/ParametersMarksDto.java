package it.its.projectwork.dfm.gestionecorsi.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParametersMarksDto {

	@NotNull
	private Integer idCriterio;
	
	private String descrizione;
	
	private String tipo;
}
