package it.its.projectwork.dfm.gestionecorsi.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table (name = "docenti")
@Data
public class TeachersDao {

	@Id
	@Column (name = "docente_id")
	private int docenteId;
	
	@Column (name = "titolo")
	private String titolo;
	
	@Column (name = "ragione_sociale")
	private String ragioneSociale;
	
	@Column (name = "indirizzo")
	private String indirizzo;
	
	@Column (name = "localita")
	private String localita;
	
	@Column (name = "provincia")
	private String provincia;

	@Column (name = "nazione")
	private String nazione;
	
	@Column (name = "telefono")
	private String telefono;
	
	@Column (name = "fax")
	private String fax;
	
	@Column (name = "email")
	private String email;
	
	@Column (name = "note_generiche")
	private String noteGeneriche;
	
	@Column (name = "partita_iva")
	private String partitaIva;
	
	@Column (name = "referente")
	private String referente;
	
	@Column (name = "trashed")
	private Date trashed;
	
}
