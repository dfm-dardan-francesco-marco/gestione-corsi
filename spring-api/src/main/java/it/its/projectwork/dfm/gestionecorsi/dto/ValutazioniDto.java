package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ValutazioniDto {

	private String id;
	
	private int valore;

}
