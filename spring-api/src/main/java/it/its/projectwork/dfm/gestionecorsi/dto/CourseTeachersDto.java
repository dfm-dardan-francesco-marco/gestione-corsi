package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseTeachersDto {
	
	public enum CourseTeacherType {
		TEACHER,
		EMPLOYEE
	}
	
	private int corsiDocentiId;
	
	private int corso;
	
	private int docenteOInternoId;
	
	private CourseTeacherType type;
	
	private String showedText;

}
