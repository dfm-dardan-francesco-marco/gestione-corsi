package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IMarksTeachersService;

@Service
public class MarksTeachersService implements IMarksTeachersService{

	@Autowired
	private MarksTeachersRepository marksTeachersRepository;
	
	// insert
	@Override
	public EServiceResponse insert(List<MarksTeachersDto> dtos) {
		if (dtos == null || dtos.size() == 0) return EServiceResponse.EMPTY_DTO;
		try {
			boolean updatedOne = false;
			for (MarksTeachersDto dto : dtos) {
				try {
					MarksTeachersDao updatableDto = dto.getDocente() != null ? marksTeachersRepository.findByCorsoAndDocenteAndCriterio(dto.getCorso(), dto.getDocente(), dto.getCriterio()) : marksTeachersRepository.findByCorsoAndInternoAndCriterio(dto.getCorso(), dto.getInterno(), dto.getCriterio());
					
					MarksTeachersDao dao = new MarksTeachersDao();
					dtoToDao(dto, dao);
					
					if (updatableDto != null) {
						dao.setValutazioniDocentiId(updatableDto.getValutazioniDocentiId());
						updatedOne = true;
					}
					marksTeachersRepository.save(dao);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return updatedOne ? EServiceResponse.UPDATE_OK : EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.CREATE_FAILED;
		}
	}

	// get all
	@Override
	public PageableDto<List<MarksTeachersDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<MarksTeachersDto> MarksTeachersDto = new ArrayList<>();
			Page<MarksTeachersDao> MarksTeachersDao = marksTeachersRepository.findAll(
					order != null ?
					PageRequest.of(startPage, resultsPerPage, order.make()) :
					PageRequest.of(startPage, resultsPerPage)
				);
			
			for (MarksTeachersDao dao : MarksTeachersDao) {
				MarksTeachersDto dto = new MarksTeachersDto();
				daoToDto(dao, dto);
				MarksTeachersDto.add(dto);
			}
			
			return new PageableFactory<List<MarksTeachersDto>>().create(count(), resultsPerPage, startPage, MarksTeachersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// get by id
	@Override
	public MarksTeachersDto getById(int id) {
		try {
			MarksTeachersDao dao = marksTeachersRepository.findById(id).get();
			MarksTeachersDto dto = new MarksTeachersDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	// get by course
	@Override
	public PageableDto<List<MarksTeachersDto>> getByCourse(int course, int page, int resultsPerPage, Order order) {
		try {
			List<MarksTeachersDto> MarksTeachersDto = new ArrayList<>();
			Page<MarksTeachersDao> MarksTeachersDao = marksTeachersRepository.findByCorso(course, 
					order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage)
				);
			for (MarksTeachersDao dao : MarksTeachersDao) {
				try {
					MarksTeachersDto dto = new MarksTeachersDto();
					daoToDto(dao, dto);
					MarksTeachersDto.add(dto);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return new PageableFactory<List<MarksTeachersDto>>().create(marksTeachersRepository.countByCorso(course), resultsPerPage, page, MarksTeachersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// get by teacher
	@Override
	public PageableDto<List<MarksTeachersDto>> getByTeacher(int course, int teacher, int page, int resultsPerPage, Order order) {
		try {
			List<MarksTeachersDto> MarksTeachersDto = new ArrayList<>();
			Page<MarksTeachersDao> MarksTeachersDao = marksTeachersRepository.findByCorsoAndDocente(course, teacher, 
					order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage)
				);
			
			for (MarksTeachersDao dao : MarksTeachersDao) {
				try {
					MarksTeachersDto dto = new MarksTeachersDto();
					daoToDto(dao, dto);
					MarksTeachersDto.add(dto);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			return new PageableFactory<List<MarksTeachersDto>>().create(marksTeachersRepository.countByDocenteAndCorso(teacher, course), resultsPerPage, page, MarksTeachersDto);
		} catch (Exception e) {
			return null;
		}
	}
	
	public Integer countByTeacher(int teacher) {
		return (int) marksTeachersRepository.countByDocente(teacher);
	}
	
	@Override
	public PageableDto<List<MarksTeachersDto>> getByInterno(int course, int interno, int page, int resultsPerPage, Order order) {
		try {
			List<MarksTeachersDto> MarksTeachersDto = new ArrayList<>();
			Page<MarksTeachersDao> MarksTeachersDao = marksTeachersRepository.findByCorsoAndInterno(course, interno, 
					order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage)
				);
			
			for (MarksTeachersDao dao : MarksTeachersDao) {
				MarksTeachersDto dto = new MarksTeachersDto();
				daoToDto(dao, dto);
				MarksTeachersDto.add(dto);
			}
			
			return new PageableFactory<List<MarksTeachersDto>>().create(marksTeachersRepository.countByInternoAndCorso(interno, course), resultsPerPage, page, MarksTeachersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Integer countByInterno(int interno) {
		return (int) marksTeachersRepository.countByInterno(interno);
	}

	// DELETE by corso
	@Override
	@Transactional
	public EServiceResponse deleteByCorso(int corso, int docente, int interno) {
		if (corso == 0) return EServiceResponse.EMPTY_ID;
		try {
			if (docente == 0 && interno == 0) {
				marksTeachersRepository.deleteByCorso(corso);
			} else if (docente == 0) {
				marksTeachersRepository.deleteByCorsoAndInterno(corso, interno);
			} else {
				marksTeachersRepository.deleteByCorsoAndDocente(corso, docente);
			}
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	// dao to dto
	@Override
	public void daoToDto(MarksTeachersDao dao, MarksTeachersDto dto) throws JsonMappingException, JsonProcessingException {
		dto.setValutazionidocentiId(dao.getValutazioniDocentiId());
		dto.setCorso(dao.getCorso());
		dto.setDocente(dao.getDocente());
		dto.setInterno(dao.getInterno());
		dto.setCriterio(dao.getCriterio());
		ObjectMapper mapper = new ObjectMapper();
		dto.setValore(mapper.readValue(dao.getValore(), HashMap.class));
	}

	// dto to dao
	@Override
	public void dtoToDao(MarksTeachersDto dto, MarksTeachersDao dao) {
		dao.setCorso(dto.getCorso());
		dao.setDocente(dto.getDocente());
		dao.setInterno(dto.getInterno());;
		dao.setCriterio(dto.getCriterio());
		dao.setValore(new JSONObject(dto.getValore()).toString());
	}

	// count
	@Override
	public long count() {
		return marksTeachersRepository.count();
	}

	

}
