package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.CourseUsersService;

@RestController
@RequestMapping(value = "api/course-users")
public class CourseUsersController {

	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private CourseUsersService CUservice;
	
	/**
	 * INSERT new record
	 * Try on POSTMAN: http://localhost:4000/api/course-users
	 */
	@PostMapping(value = "/{course}")
	private BaseResponseDto<ArrayList<ErrorsDto>> insertCourseUsers(@PathVariable("course") int course, @RequestBody(required = false) List<CourseUsersSuggestionDto> course_user){
		BaseResponseDto<ArrayList<ErrorsDto>> result = new BaseResponseDto<>();
		result.setResponse(CUservice.insert(course, course_user));
		return result;
	}
	
	/**
	 * GET by course or employee
	 * Try on POSTMAN: http://localhost:4000/api/course-users
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<CourseUsersDto>>> getAllCourseUsers(
			@RequestParam(required = false) Integer course,
			@RequestParam(required = false) Integer employee,
			@RequestParam(required = false) Object search,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			){
		BaseResponseDto<PageableDto<List<CourseUsersDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(
				course != null ?
				CUservice.getByCorso(course, page, resultsPerPage, order, search) :
				employee != null ? 
						CUservice.getByEmployee(employee, page, resultsPerPage, order) :
						null
				);
		
		return result;
	}
	
	
	/**
	 * Get by ID
	 * Try on POSTMAN: http://localhost:4000/api/course-users/{id}
	 */
	@GetMapping(value = "/{id}", produces = "application/json")
	private BaseResponseDto<CourseUsersDto> getCourseUserByID(@PathVariable("id") Integer id){
		BaseResponseDto<CourseUsersDto> result = new BaseResponseDto<>();
		
		result.setResponse(CUservice.getById(id));
		return result;
	}
	
	@PatchMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> getEmployees(@PathVariable(value = "id", required = false) Integer id, @RequestBody(required = false) CourseUsersDto dto) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		result.setResponse(CUservice.update(dto, id));		
		return result;
	}
	
	/**
	 * DELETE BY ID
	 * Try on POSTMAN: http://localhost:4000/api/course-users/{id}
	 */
	@DeleteMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteCourseUser(@PathVariable("id") Integer id){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		
		EServiceResponse serviceStatus = CUservice.deleteById(id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET EMPLOYEES
	 * Try on POSTMAN: http://localhost:4000/api/course-users/employees
	 */
	@GetMapping(value = "/employees", produces = "application/json")
	private BaseResponseDto<List<CourseUsersSuggestionDto>> getEmployees(@RequestParam(required = false) String value) {
		BaseResponseDto<List<CourseUsersSuggestionDto>> result = new BaseResponseDto<>();
		result.setResponse(CUservice.getEmployees(value));		
		return result;
	}
}
