package it.its.projectwork.dfm.gestionecorsi.common;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Sort;

public class Order {

	HashMap<String, Byte> sortProperties = new HashMap<>();
	
	public Order(String filter) {
		String[] filters = filter.split(";");
		
		for (String aFilter : filters) {
			try {
				String[] pair = aFilter.split(",");
				this.sortProperties.put(pair[0], Byte.parseByte(pair[1]));
			} catch (Exception e) {
				System.out.println("Cannot parse property");
				e.printStackTrace();
			}
		}
	}
	
	public void printAll() {
		for (Map.Entry<String, Byte> entry : sortProperties.entrySet()) {
		    String key = entry.getKey();
		    Byte value = entry.getValue();
		    System.out.println(key + " " + value);
		}
	}
	
	private Sort orderDirection(Sort sort, int direction) {
		if (direction == -1) {
			return sort.descending();
		} else {
			return sort.ascending();
		}
	}
	
	public Sort make() {
		if (sortProperties.size() > 0) {
			Sort sort = null;
			
			for (Map.Entry<String, Byte> sortProperty : sortProperties.entrySet()) {
			    String key = sortProperty.getKey();
			    Byte direction = sortProperty.getValue();
			    
			    if (sort == null) {
			    	sort = orderDirection(Sort.by(key), direction);
			    } else {
			    	Sort temp = orderDirection(Sort.by(key), direction);
			    	sort.and(temp);
			    }
			    
			}

			return sort;
		}
		
		return null;
	}
}
