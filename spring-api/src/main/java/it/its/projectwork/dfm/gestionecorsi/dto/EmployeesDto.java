package it.its.projectwork.dfm.gestionecorsi.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeesDto {

	private int dipendenteId;
	private String matricola;
	private int societa;
	private String cognome;
	private String nome;
	private char sesso;
	private Date dataNascita;
	private String luogoNascita;
	private String statoCivile;
	private String titoloStudio;
	private String conseguitoPresso;
	private int annoConseguimento;
	private String tipoDipendente;
	private String qualifica;
	private String livello;
	private Date dataAssunzione;
	private String responsabileRisorsa;
	private String responsabileArea;
	private Date dataFineRapporto;
	private Date dataScadenzaContratto;
	
}
