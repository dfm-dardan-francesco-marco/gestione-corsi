package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseUsersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ICourseUsersService;

@Service
public class CourseUsersService implements ICourseUsersService {
	
	@Autowired
	private CourseTeachersRepository CTrepository;
	
	@Autowired
	private CourseUsersRepository CUrepository;
	
	@Autowired
	private EmployeesService employeesService;
	
	//-----------------INSERT-----------------
	@Override
	public ArrayList<ErrorsDto> insert(int course, List<CourseUsersSuggestionDto> dtos) {
		ArrayList<ErrorsDto> errors = new ArrayList<>();
		ErrorsDto error = new ErrorsDto();
		if (course == 0) {
			error.setId(course);
			error.setDescrizione("EMPTY_ID");
			errors.add(error);
		}
		if (dtos == null) {
			error.setId(course);
			error.setDescrizione("EMPTY_DTO");
			errors.add(error);
		}
		try {
			List<CourseUsersDao> daos = new ArrayList<>();
			for (CourseUsersSuggestionDto dto : dtos) {
				error = new ErrorsDto();
				int countTeacher = CTrepository.teacherPresent(course, null, dto.getDipendente());
				int countUser = CUrepository.userPresent(course, dto.getDipendente());
				if (countTeacher == 0 && countUser == 0) {
					boolean ok = true;
					CourseUsersDao dao = new CourseUsersDao();
					dao.setCorso(course);
					dao.setDipendente(dto.getDipendente());
					dao.setDurata(dto.getDurata());
					if (ok) {
						daos.add(dao);
						CUrepository.saveAll(daos);
					}
				} else if (countTeacher == 0 && countUser == 1) {
					error.setId(dto.getDipendente());
					error.setDescrizione("è già stato inserito");
					errors.add(error);
				} else if (countTeacher == 1 && countUser == 0){
					error.setId(dto.getDipendente());
					error.setDescrizione("è già un docente");
					errors.add(error);
				}
			}
			return errors;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			error.setId(course);
			error.setDescrizione("DATA_INTEGRITY_ERROR");
			errors.add(error);
			return errors;
		} catch (Exception e) {
			error.setId(course);
			error.setDescrizione("CREATE_FAILED");
			errors.add(error);
			return errors;
		}
	}
	
	//-----------------GET ALL-----------------
	@Override
	public PageableDto<List<CourseUsersDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<CourseUsersDto> course_users = new ArrayList<>();
			Page<CourseUsersDao> CourseUsersDao = CUrepository.findAll(order != null ?
																			PageRequest.of(startPage, resultsPerPage, order.make()) :
																			PageRequest.of(startPage, resultsPerPage)
																	  );
			
			for (CourseUsersDao dao : CourseUsersDao) {
				CourseUsersDto dto = new CourseUsersDto();
				daoToDto(dao, dto);
				course_users.add(dto);
			}
			
			return new PageableFactory<List<CourseUsersDto>>().create(count(), resultsPerPage, startPage, course_users);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	//-----------------GET BY COURSE-----------------
	@Override
	public PageableDto<List<CourseUsersDto>> getByCorso(int corso, int page, int resultsPerPage, Order order, Object search) {
		try {
			List<CourseUsersDto> course_users = new ArrayList<>();
			Pageable pageable = order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage);
			Page<CourseUsersDao> CourseUsersDao = search == null ? CUrepository.findByCorso(corso, pageable) : CUrepository.findByCorsoAndSearch(corso, "%" + search + "%", pageable);
			
			for (CourseUsersDao dao : CourseUsersDao) {
				CourseUsersDto dto = new CourseUsersDto();
				daoToDto(dao, dto);
				course_users.add(dto);
			}
			
			return new PageableFactory<List<CourseUsersDto>>().create(search == null ? CUrepository.countByCorso(corso) : CUrepository.countByCorsoAndSearch(corso, "%" + search + "%"), resultsPerPage, page, course_users);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//----------------GET BY EMPLOYEE-----------------
	@Override
	public PageableDto<List<CourseUsersDto>> getByEmployee(int employee, Integer page, Integer resultsPerPage, Order order) {
		try {
			List<CourseUsersDto> course_users = new ArrayList<>();
			Page<CourseUsersDao> CourseUsersDao = CUrepository.findByDipendente(employee, order != null ?
																				PageRequest.of(page, resultsPerPage, order.make()) :
																				PageRequest.of(page, resultsPerPage)
																		  );
			
			for (CourseUsersDao dao : CourseUsersDao) {
				CourseUsersDto dto = new CourseUsersDto();
				daoToDto(dao, dto);
				course_users.add(dto);
			}
			
			return new PageableFactory<List<CourseUsersDto>>().create(CUrepository.countByDipendente(employee), resultsPerPage, page, course_users);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//-----------------GET BY ID-----------------
	@Override
	public CourseUsersDto getById(int id) {
		try {
			CourseUsersDao dao = CUrepository.findById(id).get();
			CourseUsersDto dto = new CourseUsersDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	//-----------------UPDATE-----------------
	@Override
	public EServiceResponse update(CourseUsersDto dto, int id) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID; 
		try {
			CourseUsersDao dao = CUrepository.getOne(id);
			dtoToDao(dto, dao);
			CUrepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	//-----------------DELETE BY ID-----------------
	@Override
	public EServiceResponse deleteById(int id) {
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			CUrepository.deleteById(id);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}
	
	//-----------------DELETE BY CORSO-----------------
	@Override
	@Transactional
	public EServiceResponse deleteByCorso(int corso) {
		if (corso == 0) return EServiceResponse.EMPTY_ID;
		try {
			CUrepository.deleteByCorso(corso);;
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	//-----------------daoToDto-----------------
	@Override
	public void daoToDto(CourseUsersDao dao, CourseUsersDto dto) {
		dto.setCorsiUtentiId(dao.getCorsiUtentiId());
		dto.setCorso(dao.getCorso());
		dto.setDipendente(dao.getDipendente());
		dto.setDurata(dao.getDurata());
		
		dto.setShowedText(dao.getEmployeesDao().getCognome() + " " + dao.getEmployeesDao().getNome());
	}

	//-----------------dtoToDao-----------------
	@Override
	public void dtoToDao(CourseUsersDto dto, CourseUsersDao dao) {
		if (dto.getCorso() != 0) dao.setCorso(dto.getCorso());
		if (dto.getDipendente() != 0) dao.setDipendente(dto.getDipendente());
		if (dto.getDurata() != 0) dao.setDurata(dto.getDurata());
	}
	
	public long count() {
		return CUrepository.count();
	}

	@Override
	public List<CourseUsersSuggestionDto> getEmployees(String value) {
		try {
			List<CourseUsersSuggestionDto> suggestions = CUrepository.findEmployees("%" + value + "%", PageRequest.of(0, 20));
			
			return suggestions;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
