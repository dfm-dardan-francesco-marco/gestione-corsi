package it.its.projectwork.dfm.gestionecorsi.dto.thirdParty;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDto {

	@NotNull
	private String name;
	
}
