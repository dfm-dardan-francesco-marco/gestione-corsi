package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.MarksTeachersService;

@RestController
@RequestMapping(value = "api/marks-teachers")
public class MarksTeachersController {

	public static final int RESULTS_PER_PAGE = 20;

	@Autowired
	private MarksTeachersService marksTeachersService;

	/**
	 * INSERT new record 
	 * Try on POSTMAN: http://localhost:4000/api/marks-teachers
	 */
	@PostMapping(produces = "application/json")
	private BaseResponseDto<EServiceResponse> insertMarksTeachers(
			@RequestBody(required = false) List<MarksTeachersDto> marks_teachers) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksTeachersService.insert(marks_teachers);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}

	/**
	 * GET by course or employee 
	 * Try on POSTMAN: http://localhost:4000/api/marks-teachers
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<MarksTeachersDto>>> getAllMarksTeachers(
			@RequestParam(required = false) Integer course, @RequestParam(required = false) Integer teacher,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order, @RequestParam(required = false) Integer interno) {
		BaseResponseDto<PageableDto<List<MarksTeachersDto>>> result = new BaseResponseDto<>();
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		if (course != null && teacher == null && interno == null) {
			result.setResponse(marksTeachersService.getByCourse(course, page, resultsPerPage, order));
			System.out.println("sono dentrooooooo");
		} else {
			result.setResponse(teacher != null ? marksTeachersService.getByTeacher(course, teacher, page, marksTeachersService.countByTeacher(teacher), order) 
					: interno != null ? marksTeachersService.getByInterno(course, interno, page, marksTeachersService.countByInterno(interno), order) : null);
		}
		

		return result;
	}

	/**
	 * GET by ID 
	 * Try on POSTMAN: http://localhost:4000/api/marks-teachers/{id}
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<MarksTeachersDto> getMarksTeachersById(@PathVariable("id") Integer id) {
		BaseResponseDto<MarksTeachersDto> result = new BaseResponseDto<>();
		result.setResponse(marksTeachersService.getById(id));
		return result;
	}
	
	/**
	 * delete by corso and interno or docente
	 * Try on POSTMAN: http://localhost:4000/api/marks-teachers?corso=1&docente=0&interno=1
	 */
	@DeleteMapping(produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteByCorsoAndDocenteOrInterno(
			@RequestParam(required = false) Integer corso, @RequestParam(required = false) Integer docente,
			@RequestParam(required = false) Integer interno) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = marksTeachersService.deleteByCorso(corso, docente, interno);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}

}
