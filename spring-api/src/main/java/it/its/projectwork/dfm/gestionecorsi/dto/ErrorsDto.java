package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorsDto {

	private int id;
	
	private int esterno;
	
	private String descrizione;
	
}
