package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.TypesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface ITypesService {

	// insert a new type
	EServiceResponse insert(TypesDto dto);
	
	// get all types
	PageableDto<List<TypesDto>> getAll(int startPage, int resultsPerPage, Order order);
	
	// get by id
	TypesDto getById(int id);
	
	// update type
	EServiceResponse update(TypesDto dto, int id);
	
	// delete type
	EServiceResponse deleteById(int id);
	
	// transcribe dao into dto
	void daoToDto(TypesDao dao, TypesDto dto);
	
	// transcribe dto into dao
	void dtoToDao(TypesDto dto, TypesDao dao);
	
	long count();
	
}
