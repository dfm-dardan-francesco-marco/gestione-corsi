package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table (name = "valutazioni_corsi")
@Data
public class MarksCoursesDao {

	@Id
	@Column(name = "valutazioni_corsi_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer valutazioniCorsiId;
	
	@Column(name = "corso")
	private Integer corso;
	
	@Column(name = "criterio")
	private Integer criterio;
	
	@Column(name = "valore")
	private String valore;	
	
}
