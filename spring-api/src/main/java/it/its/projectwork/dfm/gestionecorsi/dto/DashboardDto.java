package it.its.projectwork.dfm.gestionecorsi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DashboardDto {

	private long nTeachers;
	
	private long nEmployees;
	
	private long nCourses;
	
	private long nMarks;
	
}
