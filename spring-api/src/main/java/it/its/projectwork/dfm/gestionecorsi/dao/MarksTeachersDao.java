package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table (name = "valutazioni_docenti")
@Data
public class MarksTeachersDao {

	@Id
	@Column(name = "valutazioni_docenti_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer valutazioniDocentiId;
	
	@Column(name = "corso")
	private Integer corso;
	
	@Column(name = "docente")
	private Integer docente;
	
	@Column(name = "interno")
	private Integer interno;
	
	@Column(name = "criterio")
	private Integer criterio;
	
	@Column(name = "valore")
	private String valore;
	
}
