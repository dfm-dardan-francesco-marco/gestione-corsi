package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "corsi_utenti")
@Data
public class CourseUsersDao {
	
	@Id
	@Column(name = "corsi_utenti_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer corsiUtentiId;
	
	@Column(name = "corso")
	private int corso;
	
	@Column(name = "dipendente")
	private int dipendente;
	
	@Column(name = "durata")
	private int durata;

	@ManyToOne(optional=false)
	@EqualsAndHashCode.Exclude
	@JoinColumn(name = "dipendente", referencedColumnName = "dipendente_id", insertable = false, updatable = false)
	@JsonBackReference
	private EmployeesDao employeesDao;
}
