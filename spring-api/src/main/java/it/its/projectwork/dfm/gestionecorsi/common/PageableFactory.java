package it.its.projectwork.dfm.gestionecorsi.common;

import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;

public class PageableFactory<T> {

	public PageableDto<T> create(long count, long resultsPerPage, long currentPage, T data) {
		return new PageableDto<T>(count, resultsPerPage, (count / resultsPerPage) + 1, currentPage, data);
	}
	
}
