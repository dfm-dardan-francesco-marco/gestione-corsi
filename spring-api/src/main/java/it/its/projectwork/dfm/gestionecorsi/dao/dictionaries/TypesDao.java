package it.its.projectwork.dfm.gestionecorsi.dao.dictionaries;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tipi")
@Data
public class TypesDao {

	@Id
	@Column(name = "tipo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer tipoId;
	
	@Column(name = "descrizione")
	private String descrizione;
	
}
