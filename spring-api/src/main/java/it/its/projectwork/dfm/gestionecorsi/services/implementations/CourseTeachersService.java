package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.TeachersDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ICourseTeachersService;

@Service
public class CourseTeachersService implements ICourseTeachersService {

	@Autowired
	private CourseTeachersRepository CTrepository;
	
	@Autowired
	private CourseUsersRepository CUrepository;
	
	@Autowired
	private TeachersService teachersService;
	
	@Autowired
	private EmployeesService employeesService;
	
	//-----------------INSERT-----------------
	@Override
	public ArrayList<ErrorsDto> insert(int course, List<CourseTeachersSuggestionDto> dtos) {
		ArrayList<ErrorsDto> errors = new ArrayList<>();
		ErrorsDto error = new ErrorsDto();
		if (course == 0) {
			error.setId(course);
			error.setDescrizione("EMPTY_ID");
			errors.add(error);
		}
		if (dtos == null) {
			error.setId(course);
			error.setDescrizione("EMPTY_DTO");
			errors.add(error);
		}
		try {
			List<CourseTeachersDao> daos = new ArrayList<>();
			for (CourseTeachersSuggestionDto dto : dtos) {
				error = new ErrorsDto();
				int countTeacher = CTrepository.teacherPresent(course, dto.getDocente(), dto.getDipendente());
				int countUser = CUrepository.userPresent(course, dto.getDipendente());
				if (countTeacher == 0 && countUser == 0) {
					boolean ok = true;
					CourseTeachersDao dao = new CourseTeachersDao();
					dao.setCorso(course);
					if (dto.getDipendente() != 0) {
						dao.setInterno(dto.getDipendente());
					} else if (dto.getDocente() != 0) {
						dao.setDocente(dto.getDocente());
					} else {
						ok = false;
					}
					if (ok) {
						daos.add(dao);
						CTrepository.saveAll(daos);
					}
				} else if (countTeacher == 0 && countUser == 1) {
					error.setId(dto.getDipendente());
					error.setDescrizione("è già uno studente");
					errors.add(error);
				} else if (countTeacher == 1 && countUser == 0) {
					if (dto.getDipendente() != 0) {
						error.setId(dto.getDipendente());
					} else {
						error.setEsterno(dto.getDocente());
					}
					error.setDescrizione("è già stato inserito");
					errors.add(error);
				}
			}
			return errors;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			error.setId(course);
			error.setDescrizione("DATA_INTEGRITY_ERROR");
			errors.add(error);
			return errors;
		} catch (Exception e) {
			error.setId(course);
			error.setDescrizione("CREATE_FAILED");
			errors.add(error);
			return errors;
		}
	}

	//-----------------GET ALL-----------------
	@Override
	public PageableDto<List<CourseTeachersDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<CourseTeachersDto> course_teachers = new ArrayList<>();
			Page<CourseTeachersDao> CourseTeachersDao = CTrepository.findAll(order != null ?
																			PageRequest.of(startPage, resultsPerPage, order.make()) :
																			PageRequest.of(startPage, resultsPerPage)
																	  );
			
			for (CourseTeachersDao dao : CourseTeachersDao) {
				CourseTeachersDto dto = new CourseTeachersDto();
				daoToDto(dao, dto);
				course_teachers.add(dto);
			}
			
			return new PageableFactory<List<CourseTeachersDto>>().create(count(), resultsPerPage, startPage, course_teachers);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	//-----------------GET BY ID-----------------
	@Override
	public CourseTeachersDto getById(int id) {
		try {
			CourseTeachersDao dao = CTrepository.findById(id).get();
			CourseTeachersDto dto = new CourseTeachersDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//-----------------GET BY COURSE-----------------
	@Override
	public PageableDto<List<CourseTeachersDto>> getByCorso(int corso, int page, int resultsPerPage, Order order) {
		try {
			List<CourseTeachersDto> course_teachers = new ArrayList<>();
			Page<CourseTeachersDao> CourseTeachersDao = CTrepository.findByCorso(corso, order != null ?
																				PageRequest.of(page, resultsPerPage, order.make()) :
																				PageRequest.of(page, resultsPerPage)
																		  );
			
			for (CourseTeachersDao dao : CourseTeachersDao) {
				CourseTeachersDto dto = new CourseTeachersDto();
				daoToDto(dao, dto);
				course_teachers.add(dto);
			}
			
			return new PageableFactory<List<CourseTeachersDto>>().create(CTrepository.countByCorso(corso), resultsPerPage, page, course_teachers);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//----------------GET BY TEACHER-----------------
	@Override
	public PageableDto<List<CourseTeachersDto>> getByTeacher(int teacher, Integer page, Integer resultsPerPage,
			Order order) {
		try {
			List<CourseTeachersDto> course_teachers = new ArrayList<>();
			Page<CourseTeachersDao> CourseTeachersDao = CTrepository.findByDocente(teacher, order != null ?
																				PageRequest.of(page, resultsPerPage, order.make()) :
																				PageRequest.of(page, resultsPerPage)
																		  );
			
			for (CourseTeachersDao dao : CourseTeachersDao) {
				CourseTeachersDto dto = new CourseTeachersDto();
				daoToDto(dao, dto);
				course_teachers.add(dto);
			}
			
			return new PageableFactory<List<CourseTeachersDto>>().create(CTrepository.countByDocente(teacher), resultsPerPage, page, course_teachers);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//-----------------UPDATE-----------------
	@Override
	public EServiceResponse update(CourseTeachersDto dto, int id) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID; 
		try {
			CourseTeachersDao dao = CTrepository.getOne(id);
			dtoToDao(dto, dao);
			CTrepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	//-----------------DELETE BY ID-----------------
	@Override
	public EServiceResponse deleteById(int id) {
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			CTrepository.deleteById(id);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	//-----------------DELETE BY CORSO-----------------
	@Override
	@Transactional
	public EServiceResponse deleteByCorso(int corso) {
		if (corso == 0) return EServiceResponse.EMPTY_ID;
		try {
			CTrepository.deleteByCorso(corso);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	//-----------------daoToDto-----------------
	@Override
	public void daoToDto(CourseTeachersDao dao, CourseTeachersDto dto) {
		dto.setCorsiDocentiId(dao.getCorsiDocentiId());
		dto.setCorso(dao.getCorso());
		
		if (dao.getDocente() != null) {
			dto.setType(CourseTeachersDto.CourseTeacherType.TEACHER);
			TeachersDto tDto = teachersService.getById(dao.getDocente());
			dto.setShowedText(tDto.getRagioneSociale());
			dto.setDocenteOInternoId(tDto.getDocenteId());
		}
		if (dao.getInterno() != null) {
			dto.setType(CourseTeachersDto.CourseTeacherType.EMPLOYEE);
			EmployeesDto eDto = employeesService.getById(dao.getInterno());
			dto.setShowedText(eDto.getCognome() + " " + eDto.getNome());
			dto.setDocenteOInternoId(eDto.getDipendenteId());
		}
	}

	//-----------------dtoToDao-----------------
	@Override
	public void dtoToDao(CourseTeachersDto dto, CourseTeachersDao dao) {
		dao.setCorso(dto.getCorso());
		
		if (dto.getType() == CourseTeachersDto.CourseTeacherType.TEACHER) {
			dao.setDocente(dto.getDocenteOInternoId());
		}
		if (dto.getType() == CourseTeachersDto.CourseTeacherType.EMPLOYEE) {
			dao.setInterno(dto.getDocenteOInternoId());
		}
	}

	@Override
	public long count() {
		return CTrepository.count();
	}

	@Override
	public List<CourseTeachersSuggestionDto> getTeachersAndEmployees(String value) {
		try {
			List<CourseTeachersSuggestionDto> suggestions = new ArrayList<>(CTrepository.findTeachers("%" + value + "%", PageRequest.of(0, 20)));
			suggestions.addAll(CTrepository.findEmployees("%" + value + "%", PageRequest.of(0, 20)));
			
			Collections.sort(suggestions, 
                    (o1, o2) -> o1.getVisualizza().compareTo(o2.getVisualizza()));
			
			return suggestions;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
