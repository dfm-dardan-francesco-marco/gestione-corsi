package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.ArrayList;
import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface ICourseTeachersService {

	//---------INSERT NEW RECORD---------
	ArrayList<ErrorsDto> insert(int course, List<CourseTeachersSuggestionDto> dtos);
	
	//---------GET ALL RECORDS---------
	PageableDto<List<CourseTeachersDto>> getAll(int startPage, int resultsPerPage, Order order);

	// ---------GET A RECORD BY ITS ID---------
	CourseTeachersDto getById(int id);

	// ---------GET A RECORD BY CORSO---------
	PageableDto<List<CourseTeachersDto>> getByCorso(int corso, int page, int resultsPerPage, Order order);

	// ---------GET A RECORD BY TEACHER---------
	PageableDto<List<CourseTeachersDto>> getByTeacher(int teacher, Integer page, Integer resultsPerPage, Order order);

	// ---------DELETE BY CORSO---------
	EServiceResponse deleteByCorso(int id);

	// ---------UPDATE A RECORD---------
	EServiceResponse update(CourseTeachersDto dto, int id);

	// ---------DELETE BY ID---------
	EServiceResponse deleteById(int id);

	// ---------TRANSCRIBE DAO INTO DTO---------
	void daoToDto(CourseTeachersDao dao, CourseTeachersDto dto);

	// ---------TRANSCRIBE DTO INTO DAO---------
	void dtoToDao(CourseTeachersDto dto, CourseTeachersDao dao);

	long count();

	List<CourseTeachersSuggestionDto> getTeachersAndEmployees(String value);

}
