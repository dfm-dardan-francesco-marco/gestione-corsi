package it.its.projectwork.dfm.gestionecorsi.dto;

import java.util.HashMap;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MarksCoursesDto {

	@NotNull
	private Integer valutazioniCorsiId;
	
	@NotNull
	private Integer corso;

	@NotNull
	private Integer criterio;
	
	@NotNull
	private HashMap<String, String> valore;
	
}
