package it.its.projectwork.dfm.gestionecorsi.repository.dictionaries;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypesDao;

public interface TypesRepository extends JpaRepository<TypesDao, Integer>{

	List<TypesDao> findAllByOrderByDescrizione();
	
}
