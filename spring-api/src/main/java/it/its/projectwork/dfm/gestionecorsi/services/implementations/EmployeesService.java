package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseTeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dao.CourseUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dao.CoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.EmployeesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesWithStatisticsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseTeachersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CourseUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.CoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.EmployeesRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IEmployeesService;

@Service
public class EmployeesService implements IEmployeesService{
	
	@Autowired
	private EmployeesRepository employeesRepository;
	
	@Autowired
	private CourseUsersRepository coursesUsersRepository;
	
	@Autowired
	private CourseTeachersRepository coursesTeachersRepository;
	
	@Autowired
	private CoursesRepository coursesRepository;
	
	@Autowired
	private CoursesService coursesService;

	/**
	 * Get all employees
	 * @return all employees
	 */
	@Override
	public PageableDto<List<EmployeesDto>> getAll(int page, int resultsPerPage, Order order) {
		try {
			List<EmployeesDto> employeesDto = new ArrayList<>();
			Page<EmployeesDao> employeesDao = employeesRepository.findAll(order != null ?
																	PageRequest.of(page, resultsPerPage, order.make()) :
																	PageRequest.of(page, resultsPerPage)
															  );
			for (EmployeesDao dao : employeesDao) {
				EmployeesDto dto = new EmployeesDto();
				daoToDto(dao, dto);
				employeesDto.add(dto);
			}
			return new PageableFactory<List<EmployeesDto>>().create(count(), resultsPerPage, page, employeesDto);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Get employee that are teachers too
	 */
	@Override
	public PageableDto<List<EmployeesDto>> getAllEmployeesTeachers(int page, int resultsPerPage, Order order) {
		try {
			List<EmployeesDto> employeesDto = new ArrayList<>();
			List<Integer> CourseUsersDao = coursesTeachersRepository.findAllDipendenteDistinct(order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage)
			  );
			
			for (Integer value : CourseUsersDao) {
				EmployeesDto dto = new EmployeesDto();
				EmployeesDao dao = employeesRepository.findById(value).get();
				daoToDto(dao, dto);
				employeesDto.add(dto);
			}
			return new PageableFactory<List<EmployeesDto>>().create(coursesTeachersRepository.countInternoDistinct(), resultsPerPage, page, employeesDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public PageableDto<List<EmployeesDto>> getByField(Object field, Integer page, Integer resultsPerPage, Order order,
			boolean docente) {
		try {
			List<EmployeesDto> employees = new ArrayList<>();
			Pageable pageable = order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage);
			if (docente) {
				List<Integer> myList = coursesTeachersRepository.findAllDipendenteDistinct(pageable);
				List<EmployeesDao> employeesDao = employeesRepository.search(field, pageable);
				for (EmployeesDao dao : employeesDao) {
					if (myList.contains(dao.getDipendenteId()) == true) {
						EmployeesDto dto = new EmployeesDto();
						daoToDto(dao, dto);
						employees.add(dto);
					}
				}
			} else {
				List<EmployeesDao> employeesDao = employeesRepository.search(field, pageable);
				for (EmployeesDao dao : employeesDao) {
						EmployeesDto dto = new EmployeesDto();
						daoToDto(dao, dto);
						employees.add(dto);
				}
			}
			
			return new PageableFactory<List<EmployeesDto>>().create(employees.size(), resultsPerPage, page, employees);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get employee by Id
	 * @param id
	 * @return EmployeesDto
	 */
	@Override
	public EmployeesDto getById(int dipendenteId) {
		try {
			EmployeesDao dao = employeesRepository.findById(dipendenteId).get();
			EmployeesDto dto = new EmployeesDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public ServiceResponse<PageableDto<List<CoursesDto>>> getCoursesByEmplyoeeId(Integer id, String type, int page, int resultsPerPage, Order order) {
		try {
			Pageable pageable = order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage);

			List<CoursesDto> cDtos = new ArrayList<>();
			long count = 0;
			if (type.equalsIgnoreCase("tenuti")) {
				Page<CourseTeachersDao> daos = coursesTeachersRepository.findByInterno(id, pageable);
				int counter = 0;
				for (CourseTeachersDao dao : daos) {
					Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
					if (cDao.get().getTrashed() == null) {
						CoursesDto cDto = new CoursesDto();
						coursesService.daoToDto(cDao.get(), cDto);
						cDtos.add(cDto);
					}else {
						counter++;
					}
					
				}
				count = coursesTeachersRepository.countByInterno(id) - counter;
			} else if (type.equalsIgnoreCase("seguiti")) {
				Page<CourseUsersDao> daos = coursesUsersRepository.findByDipendente(id, pageable);
				int counter = 0;
				for (CourseUsersDao dao : daos) {
					Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
					if (cDao.get().getTrashed() == null) {
						CoursesDto cDto = new CoursesDto();
						coursesService.daoToDto(cDao.get(), cDto);
						cDtos.add(cDto);
					}else {
						counter++;
					}
				}
				count = coursesUsersRepository.countByDipendente(id) - counter;
			}
			
			return new ServiceResponse<PageableDto<List<CoursesDto>>>(EServiceResponse.OK, new PageableFactory<List<CoursesDto>>().create(count, resultsPerPage, page, cDtos));
		} catch(Exception e) {
			return new ServiceResponse<PageableDto<List<CoursesDto>>>(EServiceResponse.READ_FAILED);
		}
	}

	/**
	 * Transcribe dao into dto.
	 * In this way the final user has no way to access directly into database.
	 * @param EmployeesDao, EmployeesDto
	 */
	@Override
	public void daoToDto(EmployeesDao dao, EmployeesDto dto) {
		dto.setDipendenteId(dao.getDipendenteId());
		dto.setMatricola(dao.getMatricola());
		dto.setSocieta(dao.getSocieta());
		dto.setCognome(dao.getCognome());
		dto.setNome(dao.getNome());
		dto.setSesso(dao.getSesso());
		dto.setDataNascita(dao.getDataNascita());
		dto.setLuogoNascita(dao.getLuogoNascita());
		dto.setStatoCivile(dao.getStatoCivile());
		dto.setTitoloStudio(dao.getTitoloStudio());
		dto.setConseguitoPresso(dao.getConseguitoPresso());
		dto.setAnnoConseguimento(dao.getAnnoConseguimento());
		dto.setTipoDipendente(dao.getTipoDipendente());
		dto.setQualifica(dao.getQualifica());
		dto.setLivello(dao.getLivello());
		dto.setDataAssunzione(dao.getDataAssunzione());
		dto.setResponsabileRisorsa(dao.getResponsabileRisorsa());
		dto.setResponsabileArea(dao.getResponsabileArea());
		dto.setDataFineRapporto(dao.getDataFineRapporto());
		dto.setDataScadenzaContratto(dao.getDataScadenzaContratto());
	}

	public long count() {
		return employeesRepository.count();
	}
	

	@Override
	public ServiceResponse<EmployeesWithStatisticsDto> getByMatricola(String matricola) {
		try {
			EmployeesDao eDao = employeesRepository.findByMatricola(matricola);
			EmployeesWithStatisticsDto eDto = new EmployeesWithStatisticsDto();
			Date now = new Date();
			daoToDto(eDao, eDto);

			long cOngoing = 0;
			long cIncoming = 0;
			long cFinished = 0;
			
			Page<CourseTeachersDao> CTdaos = coursesTeachersRepository.findByInterno(eDto.getDipendenteId(), PageRequest.of(0, 100));
			for (CourseTeachersDao dao : CTdaos) {
				Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
				if (cDao.isPresent() && cDao.get().getDataErogazione() != null && cDao.get().getDataChiusura() != null) {
					if (cDao.get().getDataErogazione().compareTo(now) * now.compareTo(cDao.get().getDataChiusura()) >= 0) {
						cOngoing++;
					} else if (cDao.get().getDataErogazione().compareTo(now) < 0) {
						cFinished++;
					} else if (cDao.get().getDataChiusura().compareTo(now) > 0) {
						cIncoming++;
					}
				}
			}
			
			Page<CourseUsersDao> CUdaos = coursesUsersRepository.findByDipendente(eDto.getDipendenteId(), PageRequest.of(0, 100));
			for (CourseUsersDao dao : CUdaos) {
				Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
				if (cDao.isPresent() && cDao.get().getDataErogazione() != null && cDao.get().getDataChiusura() != null) {
					if (cDao.get().getDataErogazione().compareTo(now) * now.compareTo(cDao.get().getDataChiusura()) >= 0) {
						cOngoing++;
					} else if (cDao.get().getDataErogazione().compareTo(now) < 0) {
						cFinished++;
					} else if (cDao.get().getDataChiusura().compareTo(now) > 0) {
						cIncoming++;
					}
				}
			}

			eDto.setIncoming(cIncoming);
			eDto.setOngoing(cOngoing);
			eDto.setFinished(cFinished);
			
			return new ServiceResponse<EmployeesWithStatisticsDto>(EServiceResponse.OK, eDto);
		} catch(Exception e) {
			return new ServiceResponse<EmployeesWithStatisticsDto>(EServiceResponse.READ_FAILED);
		}
	}

	@Override
	public ServiceResponse<HashMap<String, List<CoursesDto>>> getCoursesByMatricola(String matricola) {
		try {
			HashMap<String, List<CoursesDto>> map = new HashMap<>();
			List<CoursesDto> ongoing = new ArrayList<>();
			List<CoursesDto> finished = new ArrayList<>();
			List<CoursesDto> incoming = new ArrayList<>();
			
			EmployeesDao eDao = employeesRepository.findByMatricola(matricola);
			Date now = new Date();
			
			Page<CourseTeachersDao> CTdaos = coursesTeachersRepository.findByInterno(eDao.getDipendenteId(), PageRequest.of(0, 100));
			for (CourseTeachersDao dao : CTdaos) {
				Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
				if (cDao.isPresent() && cDao.get().getDataErogazione() != null && cDao.get().getDataChiusura() != null) {
					CoursesDto cDto = new CoursesDto();
					coursesService.daoToDto(cDao.get(), cDto);
					cDto.setSeiDocente(true);
					
					if (cDao.get().getDataErogazione().compareTo(now) * now.compareTo(cDao.get().getDataChiusura()) >= 0) {
						ongoing.add(cDto);
					} else if (cDao.get().getDataErogazione().compareTo(now) < 0) {
						finished.add(cDto);
					} else if (cDao.get().getDataChiusura().compareTo(now) > 0) {
						incoming.add(cDto);
					}
				}
			}
			
			Page<CourseUsersDao> CUdaos = coursesUsersRepository.findByDipendente(eDao.getDipendenteId(), PageRequest.of(0, 100));
			for (CourseUsersDao dao : CUdaos) {
				Optional<CoursesDao> cDao = coursesRepository.findById(dao.getCorso());
				if (cDao.isPresent() && cDao.get().getDataErogazione() != null && cDao.get().getDataChiusura() != null) {
					CoursesDto cDto = new CoursesDto();
					coursesService.daoToDto(cDao.get(), cDto);
					cDto.setSeiDocente(false);
					
					if (cDao.get().getDataErogazione().compareTo(now) * now.compareTo(cDao.get().getDataChiusura()) >= 0) {
						ongoing.add(cDto);
					} else if (cDao.get().getDataErogazione().compareTo(now) < 0) {
						finished.add(cDto);
					} else if (cDao.get().getDataChiusura().compareTo(now) > 0) {
						incoming.add(cDto);
					}
				}
			}

			map.put("ongoing", ongoing);
			map.put("finished", finished);
			map.put("incoming", incoming);
			
			return new ServiceResponse<HashMap<String, List<CoursesDto>>>(EServiceResponse.OK, map);
		} catch(Exception e) {
			return new ServiceResponse<HashMap<String, List<CoursesDto>>>(EServiceResponse.READ_FAILED);
		}
	}

}
