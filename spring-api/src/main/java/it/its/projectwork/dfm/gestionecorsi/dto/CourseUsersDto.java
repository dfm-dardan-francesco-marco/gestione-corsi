package it.its.projectwork.dfm.gestionecorsi.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseUsersDto {
	
	@NotNull
	private int corsiUtentiId;
	
	@NotNull
	private int corso;
	
	@NotNull
	private int dipendente;
	
	@NotNull
	private int durata;
	
	@NotNull
	private String showedText;

}
