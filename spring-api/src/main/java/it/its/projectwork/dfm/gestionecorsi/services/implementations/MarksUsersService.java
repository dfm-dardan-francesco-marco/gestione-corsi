package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksUsersRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IMarksUsersService;

@Service
public class MarksUsersService implements IMarksUsersService {

	@Autowired
	private MarksUsersRepository marksUsersRepository;

	// insert
	@Override
	public EServiceResponse insert(List<MarksUsersDto> dtos) {
		if (dtos == null || dtos.size() == 0) return EServiceResponse.EMPTY_DTO;
		try {
			for (MarksUsersDto dto : dtos) {
				try {
					MarksUsersDao dao = new MarksUsersDao();
					dtoToDao(dto, dao);
					marksUsersRepository.save(dao);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.CREATE_FAILED;
		}
	}

	// get all
	@Override
	public PageableDto<List<MarksUsersDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<MarksUsersDto> MarksUsersDto = new ArrayList<>();
			Page<MarksUsersDao> MarksUsersDao = marksUsersRepository
					.findAll(order != null ? PageRequest.of(startPage, resultsPerPage, order.make())
							: PageRequest.of(startPage, resultsPerPage));

			for (MarksUsersDao dao : MarksUsersDao) {
				MarksUsersDto dto = new MarksUsersDto();
				daoToDto(dao, dto);
				MarksUsersDto.add(dto);
			}

			return new PageableFactory<List<MarksUsersDto>>().create(count(), resultsPerPage, startPage, MarksUsersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// get by course
	@Override
	public PageableDto<List<MarksUsersDto>> getByCourse(int course, int page, int resultsPerPage, Order order) {
		try {
			List<MarksUsersDto> MarksUsersDto = new ArrayList<>();
			Page<MarksUsersDao> MarksUsersDao = marksUsersRepository.findByCorso(course,
					order != null ? PageRequest.of(page, (int)marksUsersRepository.countByCorso(course), order.make())
							: PageRequest.of(page, (int)marksUsersRepository.countByCorso(course)));

			for (MarksUsersDao dao : MarksUsersDao) {
				MarksUsersDto dto = new MarksUsersDto();
				daoToDto(dao, dto);
				MarksUsersDto.add(dto);
			}

			return new PageableFactory<List<MarksUsersDto>>().create(marksUsersRepository.countByCorso(course),
					(int)marksUsersRepository.countByCorso(course), page, MarksUsersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// get by employee
	@Override
	public PageableDto<List<MarksUsersDto>> getByEmployee(int employee, int page, int resultsPerPage, Order order) {
		try {
			List<MarksUsersDto> MarksUsersDto = new ArrayList<>();
			Page<MarksUsersDao> MarksUsersDao = marksUsersRepository.findByDipendente(employee,
					order != null ? PageRequest.of(page, resultsPerPage, order.make())
							: PageRequest.of(page, resultsPerPage));

			for (MarksUsersDao dao : MarksUsersDao) {
				MarksUsersDto dto = new MarksUsersDto();
				daoToDto(dao, dto);
				MarksUsersDto.add(dto);
			}

			return new PageableFactory<List<MarksUsersDto>>().create(marksUsersRepository.countByDipendente(employee),
					resultsPerPage, page, MarksUsersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// get records by course and employee
	@Override
	public PageableDto<List<MarksUsersDto>> getByCourseEmployee(Integer course, Integer employee, Integer page,
			Integer resultsPerPage, Order order) {
		try {
			List<MarksUsersDto> MarksUsersDto = new ArrayList<>();
			Page<MarksUsersDao> MarksUsersDao = marksUsersRepository.findByCorsoAndDipendente(course, employee,
					order != null ? PageRequest.of(page, resultsPerPage, order.make())
							: PageRequest.of(page, resultsPerPage));

			for (MarksUsersDao dao : MarksUsersDao) {
				MarksUsersDto dto = new MarksUsersDto();
				daoToDto(dao, dto);
				MarksUsersDto.add(dto);
			}
			return new PageableFactory<List<MarksUsersDto>>().create(marksUsersRepository.countByCorsoAndDipendente(course, employee),
					resultsPerPage, page, MarksUsersDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// update
	@Override
	public EServiceResponse update(List<MarksUsersDto> dtos) {
		if (dtos == null || dtos.size() == 0) return EServiceResponse.EMPTY_DTO;
		try {
			for (MarksUsersDto dto : dtos) {
				try {
					MarksUsersDao dao = marksUsersRepository.getOne(dto.getValutazioniUtentiId());
					dtoToDao(dto, dao);
					marksUsersRepository.save(dao);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	// delete by corso
	@Override
	@Transactional
	public EServiceResponse deleteByCorso(int corso, int dipendente) {
		if (corso == 0)
			return EServiceResponse.EMPTY_ID;
		try {
			if (dipendente == 0) {
				marksUsersRepository.deleteByCorso(corso);
			} else {
				marksUsersRepository.deleteByCorsoAndDipendente(corso, dipendente);
			}
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	// dao to dto
	@Override
	public void daoToDto(MarksUsersDao dao, MarksUsersDto dto) {
		dto.setValutazioniUtentiId(dao.getValutazioniUtentiId());
		dto.setCorso(dao.getCorso());
		dto.setDipendente(dao.getDipendente());
		dto.setCriterio(dao.getCriterio());
		dto.setValore(dao.getValore());
	}

	// dto to dao
	@Override
	public void dtoToDao(MarksUsersDto dto, MarksUsersDao dao) {
		dao.setCorso(dto.getCorso());
		dao.setDipendente(dto.getDipendente());
		dao.setCriterio(dto.getCriterio());
		dao.setValore(dto.getValore() == null ? 0 : dto.getValore());
	}

	// count
	@Override
	public long count() {
		return marksUsersRepository.count();
	}

	public Integer countByCourse(Integer course) {
		
		return (int) marksUsersRepository.countByCorso(course);
	}

}
