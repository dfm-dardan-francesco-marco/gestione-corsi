package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.dao.CompaniesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CompaniesDto;

public interface ICompaniesService {

	// get all companies
	List<CompaniesDto> getAll();
	
	// transcribe dao into dto
	void daoToDto(CompaniesDao dao, CompaniesDto dto);

	// get a company by Id
	CompaniesDto getById(Integer id);

}
