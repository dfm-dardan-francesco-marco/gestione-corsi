package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksUsersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksUsersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface IMarksUsersService {

	// insert a new record
	EServiceResponse insert(List<MarksUsersDto> dto);
	
	// get all records
	PageableDto<List<MarksUsersDto>> getAll(int startPage, int resultsPerPage, Order order);
	
	// get records by course
	PageableDto<List<MarksUsersDto>> getByCourse(int course, int page, int resultsPerPage, Order order);
	
	// get records by employee
	PageableDto<List<MarksUsersDto>> getByEmployee(int employee, int page, int resultsPerPage, Order order);
	
	// get records by course and employee
	PageableDto<List<MarksUsersDto>> getByCourseEmployee(Integer course, Integer employee, Integer page,
			Integer resultsPerPage, Order order);
	
	// update a record
	EServiceResponse update(List<MarksUsersDto> dto);
		
	// delete a record by corso
	EServiceResponse deleteByCorso(int corso, int dipendente);
	
	// transcribe dao into dto
	void daoToDto(MarksUsersDao dao, MarksUsersDto dto);
	
	// transcribe dto into dao
	void dtoToDao(MarksUsersDto dto, MarksUsersDao dao);
	
	// count
	long count();
	
}
