package it.its.projectwork.dfm.gestionecorsi.common;

import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class ServiceResponse<T> {

	@NonNull
	EServiceResponse response;
	
	T data;
	
}
