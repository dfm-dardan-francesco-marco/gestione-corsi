package it.its.projectwork.dfm.gestionecorsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.its.projectwork.dfm.gestionecorsi.dao.CompaniesDao;

public interface CompaniesRepository extends JpaRepository<CompaniesDao, Integer>{
	
}
