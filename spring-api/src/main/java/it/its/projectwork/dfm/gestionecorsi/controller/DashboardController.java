package it.its.projectwork.dfm.gestionecorsi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.ResponseFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.DashboardDto;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.DashboardService;

@RestController()
@RequestMapping(value = "api/dashboard")
public class DashboardController {
	
	@Autowired
	private DashboardService dashboardService;
	
	/**
	 * Get all counters
	 * @return BaseResponseDto<DashboardDto>
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<ServiceResponse<DashboardDto>> getAllTeachers_Controller(){
		return new ResponseFactory<DashboardDto>().make(dashboardService.getCounters());
	}
}
