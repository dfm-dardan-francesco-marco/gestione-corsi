package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dao.TeachersDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.TeachersDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface ITeachersService {
	
	//---------INSERT NEW TEACHER---------
	EServiceResponse insert(TeachersDto dto);
	
	//---------GET ALL TEACHERS---------
	PageableDto<List<TeachersDto>> getAll(int startPage, int resultsPerPage, Order order, boolean trashed);
	
	//---------GET A TEACHER BY HIS ID---------
	TeachersDto getById(int id);
	
	//---------GET TEACHERS BY RagioneSociale---------
	PageableDto<List<TeachersDto>> getByField(Object field, int startPage, int resultsPerPage, Order order, boolean trashed);
	
	//---------UPDATE TEACHER---------
	EServiceResponse update(TeachersDto dto, int id);
	
	//---------DELETE TEACHER---------
	EServiceResponse delete(int id);

	//---------TRANSCRIBE DAO INTO DTO---------
	void daoToDto(TeachersDao dao, TeachersDto dto);
	
	//---------TRANSCRIBE DTO INTO DAO---------
	void dtoToDao(TeachersDto dto, TeachersDao dao, boolean acceptMissing);

	ServiceResponse<PageableDto<List<CoursesDto>>> getByTeacher(Integer id, int page, int resultsPerPage, Order order);
	
	// set trashed
	EServiceResponse setTrashed(TeachersDto dto, int id, boolean trashed);
	
}
