package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.CoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao;
import it.its.projectwork.dfm.gestionecorsi.dao.ParametersMarksDao;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.MarksCoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ParametersMarksDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.CoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.MarksCoursesRepository;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.ParametersMarksRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IMarksCoursesService;

@Service
public class MarksCoursesService implements IMarksCoursesService {

	@Autowired
	private MarksCoursesRepository marksCoursesRepository;
	
	@Autowired
	private ParametersMarksRepository paramRepo;

	@Autowired
	private CoursesRepository coursesRepository;

	@Autowired
	private CoursesService coursesService;
	
	@Override
	public EServiceResponse insert(List<MarksCoursesDto> dtos) {
		if (dtos == null || dtos.size() == 0) return EServiceResponse.EMPTY_DTO;
		try {
			boolean updatedOne = false;
			for (MarksCoursesDto dto : dtos) {
				try {
					MarksCoursesDao updatableDto = marksCoursesRepository.findByCorsoAndCriterio(dto.getCorso(), dto.getCriterio()); // Checks if an entity for corsoId and criterioId already exists
					
					MarksCoursesDao dao = new MarksCoursesDao();
					dtoToDao(dto, dao);
					
					if (updatableDto != null) { // If it exists, then update rather than create a new one
						dao.setValutazioniCorsiId(updatableDto.getValutazioniCorsiId());
						updatedOne = true;
					}
					marksCoursesRepository.save(dao);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return updatedOne ? EServiceResponse.UPDATE_OK : EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.CREATE_FAILED;
		}
	}

	@Override
	public PageableDto<List<MarksCoursesDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<MarksCoursesDto> MarksCoursesDto = new ArrayList<>();
			Page<MarksCoursesDao> MarksCoursesDao = marksCoursesRepository.findAll(
					order != null ?
					PageRequest.of(startPage, resultsPerPage, order.make()) :
					PageRequest.of(startPage, resultsPerPage)
				);
			
			for (MarksCoursesDao dao : MarksCoursesDao) {
				MarksCoursesDto dto = new MarksCoursesDto();
				daoToDto(dao, dto);
				MarksCoursesDto.add(dto);
			}
			
			return new PageableFactory<List<MarksCoursesDto>>().create(count(), resultsPerPage, startPage, MarksCoursesDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public PageableDto<List<CoursesDto>> getCoursesWithMissingMark(Object field, int startPage, int resultsPerPage, Order order) {
		try {
			Date currentDate = new Date();
			List<CoursesDto> dtos = new ArrayList<>();
			Page<CoursesDao> daos = null;
			
			if (field == null) {
				daos = coursesRepository.findWithMissingMark(
						currentDate,
						order != null ?
						PageRequest.of(startPage, resultsPerPage, order.make()) :
						PageRequest.of(startPage, resultsPerPage)
					);
			} else {
				daos = coursesRepository.searchWithMissingMark(
						field,
						currentDate,
						order != null ?
						PageRequest.of(startPage, resultsPerPage, order.make()) :
						PageRequest.of(startPage, resultsPerPage)
					);
			}

			for (CoursesDao dao : daos) {
				CoursesDto dto = new CoursesDto();
				coursesService.daoToDto(dao, dto);
				dtos.add(dto);
			}
			
			return new PageableFactory<List<CoursesDto>>().create(coursesRepository.countWithMissingMark(currentDate), resultsPerPage, startPage, dtos);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public MarksCoursesDto getById(int id) {
		try {
			MarksCoursesDao dao = marksCoursesRepository.findById(id).get();
			MarksCoursesDto dto = new MarksCoursesDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public PageableDto<List<MarksCoursesDto>> getByCourse(int course, int page, int resultsPerPage, Order order) {
		try {
			List<MarksCoursesDto> MarksCoursesDto = new ArrayList<>();
			Page<MarksCoursesDao> MarksCoursesDao = marksCoursesRepository.findByCorso(course, 
					order != null ?
					PageRequest.of(page, resultsPerPage, order.make()) :
					PageRequest.of(page, resultsPerPage)
				);
			
			for (MarksCoursesDao dao : MarksCoursesDao) {
				MarksCoursesDto dto = new MarksCoursesDto();
				daoToDto(dao, dto);
				MarksCoursesDto.add(dto);
			}
			
			return new PageableFactory<List<MarksCoursesDto>>().create(marksCoursesRepository.countByCorso(course), resultsPerPage, page, MarksCoursesDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public EServiceResponse deleteById(int id) {
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			marksCoursesRepository.deleteById(id);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	@Override
	@Transactional
	public EServiceResponse deleteByCorso(int corso) {
		if (corso == 0) return EServiceResponse.EMPTY_ID;
		try {
			marksCoursesRepository.deleteByCorso(corso);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	@Override
	public void daoToDto(MarksCoursesDao dao, MarksCoursesDto dto) throws JsonMappingException, JsonProcessingException {
		dto.setValutazioniCorsiId(dao.getValutazioniCorsiId());
		dto.setCorso(dao.getCorso());
		dto.setCriterio(dao.getCriterio());
	    ObjectMapper mapper = new ObjectMapper();
		dto.setValore(mapper.readValue(dao.getValore(), HashMap.class));
	}

	@Override
	public void dtoToDao(MarksCoursesDto dto, MarksCoursesDao dao) {
		dao.setCriterio(dto.getCriterio());
		dao.setCorso(dto.getCorso());
		dao.setValore(new JSONObject(dto.getValore()).toString());
	}

	@Override
	public long count() {
		return marksCoursesRepository.count();
	}

	@Override
	public long countDistinct() {
		return marksCoursesRepository.countDistinctCorso();
	}
	

	@Override
	public List<ParametersMarksDto> getAllParamsByTipo() {
		try {
			List<ParametersMarksDto> paramsDto = new ArrayList<>();
			List<ParametersMarksDao> paramsDao = paramRepo.findAll();
			for (ParametersMarksDao dao : paramsDao) {
				ParametersMarksDto dto = new ParametersMarksDto();
				daoAdto(dao, dto);
				paramsDto.add(dto);
			}
			return paramsDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void daoAdto(ParametersMarksDao dao, ParametersMarksDto dto) {
		dto.setIdCriterio(dao.getIdCriterio());
		dto.setDescrizione(dao.getDescrizione());
		dto.setTipo(dao.getTipo());
		
	}

}
