package it.its.projectwork.dfm.gestionecorsi.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.EmployeesDao;

@Repository
public interface EmployeesRepository extends JpaRepository<EmployeesDao, Integer> {

	@Query("SELECT E FROM EmployeesDao E WHERE "
			+ "E.matricola LIKE %?1% OR "
			+ "E.cognome LIKE %?1% OR "
			+ "E.nome LIKE %?1% OR "
			+ "E.sesso LIKE %?1% OR "
			+ "E.dataNascita LIKE %?1% OR "
			+ "E.luogoNascita LIKE %?1% OR "
			+ "E.statoCivile LIKE %?1% OR "
			+ "E.titoloStudio LIKE %?1% OR "
			+ "E.conseguitoPresso LIKE %?1% OR "
			+ "E.annoConseguimento LIKE %?1% OR "
			+ "E.tipoDipendente LIKE %?1% OR "
			+ "E.qualifica LIKE %?1% OR "
			+ "E.livello LIKE %?1% OR "
			+ "E.dataAssunzione LIKE %?1% OR "
			+ "E.responsabileRisorsa LIKE %?1% OR "
			+ "E.responsabileArea LIKE %?1% OR "
			+ "E.dataFineRapporto LIKE %?1% OR "
			+ "E.dataScadenzaContratto LIKE %?1%")
	List<EmployeesDao> search(Object field, Pageable page);

	@Query("SELECT COUNT(*) FROM EmployeesDao E WHERE "
			+ "E.matricola LIKE %?1% OR "
			+ "E.cognome LIKE %?1% OR "
			+ "E.nome LIKE %?1% OR "
			+ "E.sesso LIKE %?1% OR "
			+ "E.dataNascita LIKE %?1% OR "
			+ "E.luogoNascita LIKE %?1% OR "
			+ "E.statoCivile LIKE %?1% OR "
			+ "E.titoloStudio LIKE %?1% OR "
			+ "E.conseguitoPresso LIKE %?1% OR "
			+ "E.annoConseguimento LIKE %?1% OR "
			+ "E.tipoDipendente LIKE %?1% OR "
			+ "E.qualifica LIKE %?1% OR "
			+ "E.livello LIKE %?1% OR "
			+ "E.dataAssunzione LIKE %?1% OR "
			+ "E.responsabileRisorsa LIKE %?1% OR "
			+ "E.responsabileArea LIKE %?1% OR "
			+ "E.dataFineRapporto LIKE %?1% OR "
			+ "E.dataScadenzaContratto LIKE %?1%")
	long countSearch(Object field);

	EmployeesDao findByMatricola(String matricola);
}
