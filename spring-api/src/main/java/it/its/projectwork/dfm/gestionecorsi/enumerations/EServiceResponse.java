package it.its.projectwork.dfm.gestionecorsi.enumerations;

public enum EServiceResponse {

	UPDATE_OK(2),
	OK(1),
	CREATE_FAILED(-1),
	UPDATE_FAILED(-2),
	DELETE_FAILED(-3),
	DATA_INTEGRITY_ERROR(-4),
	EMPTY_DTO(-5),
	EMPTY_ID(-6),
	READ_FAILED(-7);

    private final int number;

    private EServiceResponse(int number) {
        this.number = number;
    }

    public int value() {
        return number;
    }
	
}
