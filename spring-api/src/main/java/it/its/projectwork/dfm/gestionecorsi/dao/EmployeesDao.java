package it.its.projectwork.dfm.gestionecorsi.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "dipendenti")
@Data
public class EmployeesDao {

	@Id
	@Column(name = "dipendente_id")
	private int dipendenteId;
	
	@Column(name = "matricola")
	private String matricola;
	
	@Column(name = "societa")
	private int societa;
	
	@Column(name = "cognome")
	private String cognome;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "sesso")
	private char sesso;
	
	@Column(name = "data_nascita")
	private Date dataNascita;
	
	@Column(name = "luogo_nascita")
	private String luogoNascita;
	
	@Column(name = "stato_civile")
	private String statoCivile;
	
	@Column(name = "titolo_studio")
	private String titoloStudio;
	
	@Column(name = "conseguito_presso")
	private String conseguitoPresso;
	
	@Column(name = "anno_conseguimento")
	private int annoConseguimento;
	
	@Column(name = "tipo_dipendente")
	private String tipoDipendente;
	
	@Column(name = "qualifica")
	private String qualifica;
	
	@Column(name = "livello")
	private String livello;
	
	@Column(name = "data_assunzione")
	private Date dataAssunzione;
	
	@Column(name = "responsabile_risorsa")
	private String responsabileRisorsa;
	
	@Column(name = "responsabile_area")
	private String responsabileArea;
	
	@Column(name = "data_fine_rapporto")
	private Date dataFineRapporto;
	
	@Column(name = "data_scadenza_contratto")
	private Date dataScadenzaContratto;
	
}
