package it.its.projectwork.dfm.gestionecorsi.dao.dictionaries;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "misure")
@Data
public class MeasuresDao {

	@Id
	@Column(name = "misura_id")
	private Integer misuraId;
	
	@Column(name = "descrizione")
	private String descrizione;
	
}
