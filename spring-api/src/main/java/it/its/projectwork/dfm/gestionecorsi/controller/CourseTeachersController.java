package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CourseTeachersSuggestionDto;
import it.its.projectwork.dfm.gestionecorsi.dto.ErrorsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.CourseTeachersService;

@RestController
@RequestMapping(value = "api/course-teachers")
public class CourseTeachersController {

	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private CourseTeachersService CTservice;
	
	/**
	 * Insert new record
	 * Try on POSTMAN: http://localhost:4000/api/course-teachers/{course}
	 */
	@PostMapping(value = "/{course}")
	private BaseResponseDto<ArrayList<ErrorsDto>> insertCourseUsers(@PathVariable("course") int course, @RequestBody(required = false) List<CourseTeachersSuggestionDto> course_teachers){
		BaseResponseDto<ArrayList<ErrorsDto>> result = new BaseResponseDto<>();
		result.setResponse(CTservice.insert(course, course_teachers));
		return result;
	}
	
	/**
	 * Get by course or teacher
	 * Try on POSTMAN: http://localhost:4000/api/course-teachers
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<CourseTeachersDto>>> getAllCourseUsers(
			@RequestParam(required = false) Integer course,
			@RequestParam(required = false) Integer teacher,
			@RequestParam(required = false) Object search,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			){
		BaseResponseDto<PageableDto<List<CourseTeachersDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(
				course != null ?
				CTservice.getByCorso(course, page, resultsPerPage, order) :
				teacher != null ? 
						CTservice.getByTeacher(teacher, page, resultsPerPage, order) :
						null
				);
		
		return result;
	}
	
	/**
	 * Get by ID
	 * Try on POSTMAN: http://localhost:4000/api/course-teachers/{id}
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<CourseTeachersDto> getCourseUserByID(@PathVariable("id") Integer id){
		BaseResponseDto<CourseTeachersDto> result = new BaseResponseDto<>();
		
		result.setResponse(CTservice.getById(id));
		return result;
	}
	
	/**
	 * Delete by ID
	 * Try on POSTMAN: http://localhost:4000/api/course-teachers/{id}
	 */
	@DeleteMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteCourseUser(@PathVariable("id") Integer id){
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		
		EServiceResponse serviceStatus = CTservice.deleteById(id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET TEACHERs-EMPLOYEES
	 * Try on POSTMAN: http://localhost:4000/api/course-teachers/teachers-employeess
	 */
	@GetMapping(value = "/teachers-employees", produces = "application/json")
	private BaseResponseDto<List<CourseTeachersSuggestionDto>> getTeachersAndEmployees(@RequestParam(required = false) String value) {
		BaseResponseDto<List<CourseTeachersSuggestionDto>> result = new BaseResponseDto<>();
		result.setResponse(CTservice.getTeachersAndEmployees(value));		
		return result;
	}
}
