package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.thirdParty.CountryDto;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.ThirdPartyService;

@RestController()
@RequestMapping(value = "api/third-party")
public class ThirdPartyController {
	
	@Autowired
	private ThirdPartyService thirdPartyService;
	
	/**
	 * Get all countries
	 * @return BaseResponseDto<CountriesDto>
	 */
	@GetMapping(value = "/countries", produces = "application/json")
	private BaseResponseDto<List<CountryDto>> getAllCountries(){
		BaseResponseDto<List<CountryDto>> result = new BaseResponseDto<>();
		
		result.setResponse(thirdPartyService.getCountries());
		return result;
	}
}
