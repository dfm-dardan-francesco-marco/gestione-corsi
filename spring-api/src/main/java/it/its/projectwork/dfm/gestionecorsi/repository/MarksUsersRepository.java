package it.its.projectwork.dfm.gestionecorsi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.MarksUsersDao;

@Repository
public interface MarksUsersRepository extends JpaRepository<MarksUsersDao, Integer>{

	// find by corso
	Page<MarksUsersDao> findByCorso(int course, Pageable page);
	
	// delete by corso
	void deleteByCorso(int course);
	
	// count by corso
	long countByCorso(int course);
	
	// find by dipendente
	Page<MarksUsersDao> findByDipendente(int employee, Pageable page);
	
	// find by dipendente e corso
	
	Page<MarksUsersDao> findByCorsoAndDipendente(int corso, int employee, Pageable page);
	
	// count by corso and dipendente
	long countByCorsoAndDipendente(int corso, int employee);
	
	// count by dipendente
	long countByDipendente(int employee);

	void deleteByCorsoAndDipendente(int corso, int dipendente);
	
}
