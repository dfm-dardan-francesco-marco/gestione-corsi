package it.its.projectwork.dfm.gestionecorsi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.MarksCoursesDao;

@Repository
public interface MarksCoursesRepository extends JpaRepository<MarksCoursesDao, Integer>{

	// find by corso
	Page<MarksCoursesDao> findByCorso(int course, Pageable page);
	
	// delete by corso
	void deleteByCorso(int course);
	
	// count by corso
	long countByCorso(int course);

	MarksCoursesDao findByCorsoAndCriterio(int course, int criterio);

	@Query("SELECT COUNT(DISTINCT mc.corso) FROM MarksCoursesDao mc")
	long countDistinctCorso();
}
