package it.its.projectwork.dfm.gestionecorsi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;

@RestController
@RequestMapping(value = "/ping")
public class PingController {
	
	@GetMapping(produces = "application/json")
	public BaseResponseDto<Boolean> ping() {
		BaseResponseDto<Boolean> response = new BaseResponseDto<>();
		response.setResponse(true);
		return response;
	}
}
