package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table (name = "corsi_docenti")
@Data
public class CourseTeachersDao {
	
	@Id
	@Column(name = "corsi_docenti_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer corsiDocentiId;
	
	@Column(name = "corso")
	private int corso;
	
	@Column(name = "docente")
	private Integer docente;
	
	@Column(name = "interno")
	private Integer interno;

}
