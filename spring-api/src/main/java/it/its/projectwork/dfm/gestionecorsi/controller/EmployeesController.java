package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ResponseFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.EmployeesWithStatisticsDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.EmployeesService;

@RestController
@RequestMapping(value = "api/employees")
public class EmployeesController {
	
	public static final int RESULTS_PER_PAGE = 20;

	@Autowired
	private EmployeesService employeesService;
	
	
	/**
	 * GET ALL the employees
	 * Try on POSTMAN: http://localhost:4000/api/employees
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<EmployeesDto>>> getAllEmployees(
			@RequestParam(required = false) Object field,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<EmployeesDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(
				field == null ? 
				employeesService.getAll(page, resultsPerPage, order) : 
				employeesService.getByField(field, page, resultsPerPage, order, false)
			);
		
		return result;
	}
	
	/**
	 * GET ALL the employees and teachers at the same time
	 * Try on POSTMAN: http://localhost:4000/api/employees
	 */
	@GetMapping(value = "/getEmployeesTeachers", produces = "application/json")
	private BaseResponseDto<PageableDto<List<EmployeesDto>>> getAllEmployeesTeachers(
			@RequestParam(required = true) Object field,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<EmployeesDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(field == null ? 
				employeesService.getAllEmployeesTeachers(page, resultsPerPage, order) :
				employeesService.getByField(field, page, resultsPerPage, order, true));
		
		return result;
	}
	
	/**
	 * GET ALL the employees that are Teachers
	 * Try on POSTMAN: http://localhost:4000/api/employees/teachers
	 */
	@GetMapping(value = "teachers", produces = "application/json")
	private BaseResponseDto<PageableDto<List<EmployeesDto>>> getAllEmployees(
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<EmployeesDto>>> result = new BaseResponseDto<>();

		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(employeesService.getAllEmployeesTeachers(page, resultsPerPage, order));
		
		return result;
	}
	
	/**
	 * GET an employee by Id
	 * Try on POSTMAN: http://localhost:4000/api/{id}
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EmployeesDto> getEmployeeById(@PathVariable("id") Integer id) {
		BaseResponseDto<EmployeesDto> result = new BaseResponseDto<>();
		result.setResponse(employeesService.getById(id));
		return result;
	}

	
	/**
	 * GET attended courses by a employees ID
	 * Try on POSTMAN: http://localhost:4000/api/employees/{id}/courses
	 */
	@GetMapping(value = "{id}/courses/{type}", produces = "application/json")
	private BaseResponseDto<ServiceResponse<PageableDto<List<CoursesDto>>>> getCourseByEmployeeId(
			@PathVariable("id") Integer id,
			@PathVariable("type") String type,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		return new ResponseFactory<PageableDto<List<CoursesDto>>>().make(
				employeesService.getCoursesByEmplyoeeId(id, type, page, resultsPerPage, order)
				);
	}

	/**
	 * GET employee by its matricola
	 * Try on POSTMAN: http://localhost:4000/api/employees/getByMatricola/{matricola}
	 */
	@GetMapping(value = "/getByMatricola/{matricola}", produces = "application/json")
	private BaseResponseDto<ServiceResponse<EmployeesWithStatisticsDto>> getByMatricola(
			@PathVariable("matricola") String matricola
			) {
		return new ResponseFactory<EmployeesWithStatisticsDto>().make(employeesService.getByMatricola(matricola));
	}

	/**
	 * Get courses by employee's matricola
	 * Try on POSTMAN: http://localhost:4000/api/employees/matricola/{matricola}
	 */
	@GetMapping(value = "/getCoursesByMatricola/{matricola}", produces = "application/json")
	private BaseResponseDto<ServiceResponse<HashMap<String, List<CoursesDto>>>> getCoursesByMatricola(
			@PathVariable("matricola") String matricola
			) {
		return new ResponseFactory<HashMap<String, List<CoursesDto>>>().make(employeesService.getCoursesByMatricola(matricola));
	}
}
