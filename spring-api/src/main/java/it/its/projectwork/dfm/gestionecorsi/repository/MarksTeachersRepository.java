package it.its.projectwork.dfm.gestionecorsi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.its.projectwork.dfm.gestionecorsi.dao.MarksTeachersDao;

@Repository
public interface MarksTeachersRepository extends JpaRepository<MarksTeachersDao, Integer>{

	// find by corso
	Page<MarksTeachersDao> findByCorso(int course, Pageable page);
	
	// delete by corso
	void deleteByCorso(int course);
	
	// count by corso
	long countByCorso(int course);
	
	// find by dipendente
	Page<MarksTeachersDao> findByCorsoAndDocente(int course, int teacher, Pageable page);
	
	// find by interno
	Page<MarksTeachersDao> findByCorsoAndInterno(int course, int interno, Pageable page);
	
	MarksTeachersDao findByCorsoAndDocenteAndCriterio(int course, int docente, int criterio);
	
	MarksTeachersDao findByCorsoAndInternoAndCriterio(int course, int interno, int criterio);
	
	// count by docente
	long countByDocenteAndCorso(int employee, int course);
	
	// count by dipendente
	long countByDocente(int employee);
	
	// count by interno
	long countByInterno(int interno);

	long countByInternoAndCorso(int interno, int course);

	void deleteByCorsoAndInterno(int corso, int interno);
	
	void deleteByCorsoAndDocente(int corso, int docente);
	
}
