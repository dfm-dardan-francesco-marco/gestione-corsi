package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.OutsideRest;
import it.its.projectwork.dfm.gestionecorsi.dto.thirdParty.CountryDto;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.IThirdPartyService;

@Service
public class ThirdPartyService implements IThirdPartyService {
	
	public static final String COUNTRIES_API = "https://restcountries.eu/rest/v2/all?fields=name";
	public static HashMap<String, String> REPLACE_COUNTRIES = null;

	@Override
	public List<CountryDto> getCountries() {
		if (REPLACE_COUNTRIES == null) {
			REPLACE_COUNTRIES = new HashMap<>();
			REPLACE_COUNTRIES.put("Italy", "Italia");
		}
		
		List<CountryDto> countries = new ArrayList<CountryDto>();
		
		OutsideRest.Response<JSONArray> outsideRest = OutsideRest.jsonArray(HttpMethod.GET, COUNTRIES_API, null);
	    
	    for (Object countryO : outsideRest.getResponse()) {
	    	JSONObject country = new JSONObject(countryO.toString());
	    	countries.add(new CountryDto(
	    			REPLACE_COUNTRIES.containsKey(country.getString("name")) ?
					REPLACE_COUNTRIES.get(country.getString("name")) : 
					country.getString("name"))
			);
	    }
	    
		return countries;
	}

}
