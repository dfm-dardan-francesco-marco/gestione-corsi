package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import java.util.List;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.DictionariesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;

public interface ICoursesService {
	
	// return the courses' list
	PageableDto<List<CoursesDto>> getAll(int page, int resultsPerPage, Order order, boolean trashed);

	// return the course with the id that was passed to it
	CoursesDto getById(int id);

	// delete a course
	EServiceResponse delete(int id);

	// insert a course
	ServiceResponse<Integer> insert(CoursesDto course);

	// update a course
	EServiceResponse update(CoursesDto dto, Integer id);

	List<CoursesDto> getIncoming();

	List<CoursesDto> getFinished();

	List<CoursesDto> getOngoing();

	ServiceResponse<DictionariesDto> getDictionaries();

	PageableDto<List<CoursesDto>> getCoursesByField(Object field, int page, int resultsPerPage, Order order, boolean trashed);
	
	PageableDto<List<CoursesDto>> getCoursesByType(Integer tipo, Integer page, Integer resultsPerPage, Order order);

	// set trashed
	EServiceResponse setTrashed(CoursesDto dto, int id, boolean trashed);
	
	// set number of marks
	EServiceResponse setMarks(CoursesDto dto, int id);
	
}
