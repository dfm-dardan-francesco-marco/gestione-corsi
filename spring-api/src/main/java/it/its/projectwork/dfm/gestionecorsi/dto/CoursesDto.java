package it.its.projectwork.dfm.gestionecorsi.dto;

import java.util.Date;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoursesDto {

	@NotNull
	private int corsoId;
	
	@NotNull
	private int tipo;
	
	@NotNull
	private int tipologia;
	
	private String descrizione;
	
	private String edizione;
	
	@NotNull
	private boolean previsto;
	
	@NotNull
	private boolean erogato;
	
	@NotNull
	private int durata;
	
	@NotNull
	private int misura;
	
	private String note;
	
	@NotNull
	private String luogo;
	
	@NotNull
	private String ente;
	
	@NotNull
	private int societa;
	
	private Date dataErogazione;
	
	private Date dataChiusura;
	
	private Date dataCensimento;
	
	@NotNull
	private boolean interno;
	
	private Boolean seiDocente;
	
	private Date trashed;
	
	@NotNull
	private int valutazioni;
	
}
