package it.its.projectwork.dfm.gestionecorsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.PageableFactory;
import it.its.projectwork.dfm.gestionecorsi.dao.dictionaries.TypesDao;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.TypesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.repository.dictionaries.TypesRepository;
import it.its.projectwork.dfm.gestionecorsi.services.interfaces.ITypesService;

@Service
public class TypesService implements ITypesService{

	@Autowired
	private TypesRepository typesRepository;
	
	/**
	 * insert a new type
	 * @param TypesDto
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse insert(TypesDto dto) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		try {
			TypesDao dao = new TypesDao();
			dtoToDao(dto, dao);
			typesRepository.save(dao);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			e.printStackTrace();
			return EServiceResponse.CREATE_FAILED;
		}
	}

	/**
	 * get all types
	 * @param startPage, resultsPerPage, order
	 * @return all types
	 */
	@Override
	public PageableDto<List<TypesDto>> getAll(int startPage, int resultsPerPage, Order order) {
		try {
			List<TypesDto> typesDto = new ArrayList<>();
			Page<TypesDao> typesDao = typesRepository.findAll(order != null ?
																			PageRequest.of(startPage, resultsPerPage, order.make()) :
																			PageRequest.of(startPage, resultsPerPage)
																	  );
			
			for (TypesDao dao : typesDao) {
				TypesDto dto = new TypesDto();
				daoToDto(dao, dto);
				typesDto.add(dto);
			}
			
			return new PageableFactory<List<TypesDto>>().create(count(), resultsPerPage, startPage, typesDto);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get by id
	 * @param id
	 * @return a type by id
	 */
	@Override
	public TypesDto getById(int id) {
		try {
			TypesDao dao = typesRepository.findById(id).get();
			TypesDto dto = new TypesDto();
			daoToDto(dao, dto);
			return dto;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * update a type
	 * @param TypesDto, id
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse update(TypesDto dto, int id) {
		if (dto == null) return EServiceResponse.EMPTY_DTO;
		if (id == 0) return EServiceResponse.EMPTY_ID; 
		try {
			TypesDao dao = typesRepository.getOne(id);
			dtoToDao(dto, dao);
			typesRepository.save(dao);
			return EServiceResponse.OK;
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.UPDATE_FAILED;
		}
	}

	/**
	 * delete a type
	 * @param id
	 * @return ServiceResponse
	 */
	@Override
	public EServiceResponse deleteById(int id) {
		if (id == 0) return EServiceResponse.EMPTY_ID;
		try {
			typesRepository.deleteById(id);
			return EServiceResponse.OK; 
		} catch (org.springframework.dao.DataIntegrityViolationException e) {
			return EServiceResponse.DATA_INTEGRITY_ERROR;
		} catch (Exception e) {
			return EServiceResponse.DELETE_FAILED;
		}
	}

	@Override
	public void daoToDto(TypesDao dao, TypesDto dto) {
		dto.setTipoId(dao.getTipoId());
		dto.setDescrizione(dao.getDescrizione());
	}

	@Override
	public void dtoToDao(TypesDto dto, TypesDao dao) {
		dao.setDescrizione(dto.getDescrizione());
	}

	@Override
	public long count() {
		return typesRepository.count();
	}

}
