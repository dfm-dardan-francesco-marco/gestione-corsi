package it.its.projectwork.dfm.gestionecorsi.dto;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompaniesDto {

	@NotNull
	private int societaId;
	
	@NotNull
	private String ragioneSociale;
	
	private String indirizzo;
	
	private String localita;
	
	private String provincia;
	
	private String nazione;
	
	private String telefono;
	
	private String fax;
	
	private String partitaIva;
	
}
