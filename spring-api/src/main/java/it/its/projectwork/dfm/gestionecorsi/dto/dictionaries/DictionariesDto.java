package it.its.projectwork.dfm.gestionecorsi.dto.dictionaries;

import java.util.List;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DictionariesDto {

	@NotNull
	private List<TypologiesDto> tipologie;

	@NotNull
	private List<TypesDto> tipi;

	@NotNull
	private List<MeasuresDto> misure;
	
}
