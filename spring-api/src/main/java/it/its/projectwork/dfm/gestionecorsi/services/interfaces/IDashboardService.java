package it.its.projectwork.dfm.gestionecorsi.services.interfaces;

import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.DashboardDto;

public interface IDashboardService {

	ServiceResponse<DashboardDto> getCounters();
	
}
