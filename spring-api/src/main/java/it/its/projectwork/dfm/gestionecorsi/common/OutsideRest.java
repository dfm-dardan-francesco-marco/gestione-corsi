package it.its.projectwork.dfm.gestionecorsi.common;

import org.json.JSONArray;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class OutsideRest {
	
	@AllArgsConstructor
	@Getter
	public static class Response<T> {
		public final HttpStatus status;
		public final T response;
	}

	public static Response<JSONArray> jsonArray(HttpMethod method, String url, HttpEntity<String> body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, method, body, String.class);
		
		JSONArray array;
		try {
			array = new JSONArray(responseEntity.getBody());
		} catch (Exception e) {
			array = null;
		}

		return new Response<JSONArray>(responseEntity.getStatusCode(), array);
	}
	
}
