package it.its.projectwork.dfm.gestionecorsi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.its.projectwork.dfm.gestionecorsi.common.Order;
import it.its.projectwork.dfm.gestionecorsi.common.ResponseFactory;
import it.its.projectwork.dfm.gestionecorsi.common.ServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.dto.BaseResponseDto;
import it.its.projectwork.dfm.gestionecorsi.dto.CoursesDto;
import it.its.projectwork.dfm.gestionecorsi.dto.PageableDto;
import it.its.projectwork.dfm.gestionecorsi.dto.dictionaries.DictionariesDto;
import it.its.projectwork.dfm.gestionecorsi.enumerations.EServiceResponse;
import it.its.projectwork.dfm.gestionecorsi.services.implementations.CoursesService;

@RestController
@RequestMapping(value = "api/courses")
public class CoursesController {
	
	public static final int RESULTS_PER_PAGE = 20;
	
	@Autowired
	private CoursesService coursesService;
	
	/**
	 * GET ALL
	 * Try on POSTMAN: http://localhost:4000/api/courses
	 */
	@GetMapping(produces = "application/json")
	private BaseResponseDto<PageableDto<List<CoursesDto>>> getAllCourses(
			@RequestParam(required = false) Object field,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order,
			@RequestParam(required = false) boolean trashed
			) {
		BaseResponseDto<PageableDto<List<CoursesDto>>> result = new BaseResponseDto<>();
		
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(
				field == null ?
				coursesService.getAll(page, resultsPerPage, order, trashed) :
				coursesService.getCoursesByField(field, page, resultsPerPage, order, trashed));
		return result;
	}
	
	
	/**
	 * GET BY ID
	 * Try on POSTMAN: http://localhost:4000/api/courses/{id}
	 * @return BaseResponse<CoursesDto>
	 */
	@GetMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<CoursesDto> getCourseById(@PathVariable("id") Integer id) {
		BaseResponseDto<CoursesDto> result = new BaseResponseDto<>();
		result.setResponse(coursesService.getById(id));
		return result;
	}
	
	/**
	 * Get incoming courses
	 * Try on POSTMAN: http://localhost:4000/api/courses/incoming
	 */
	@GetMapping(value = "/incoming", produces = "application/json")
	private BaseResponseDto<List<CoursesDto>> getIncoming() {
		BaseResponseDto<List<CoursesDto>> result = new BaseResponseDto<>();
		result.setResponse(coursesService.getIncoming());
		return result;
	}
	
	/**
	 * Get ongoing courses
	 * Try on POSTMAN: http://localhost:4000/api/courses/ongoing
	 */
	@GetMapping(value = "/ongoing", produces = "application/json")
	private BaseResponseDto<List<CoursesDto>> getOngoing() {
		BaseResponseDto<List<CoursesDto>> result = new BaseResponseDto<>();
		result.setResponse(coursesService.getOngoing());
		return result;
	}
	
	/**
	 * Get finished courses
	 * Try on POSTMAN: http://localhost:4000/api/courses/finished
	 */
	@GetMapping(value = "/finished", produces = "application/json")
	private BaseResponseDto<List<CoursesDto>> getFinished() {
		BaseResponseDto<List<CoursesDto>> result = new BaseResponseDto<>();
		result.setResponse(coursesService.getFinished());
		return result;
	}
	
	/**
	 * Delete a course by ID
	 * Try on POSTMAN: http://localhost:4000/api/courses/delete/{id}
	 */
	@DeleteMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> deleteCourse(@PathVariable("id") Integer id) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = coursesService.delete(id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * INSERT
	 * Try on POSTMAN: http://localhost:4000/api/courses
	 */
	@PostMapping()
	private BaseResponseDto<ServiceResponse<Integer>> insertCourse(@RequestBody(required = false) CoursesDto course) {
		return new ResponseFactory<Integer>().make(coursesService.insert(course));
	}
	
	/**
	 * Update a course
	 * Try on POSTMAN: http://localhost:4000/api/courses/{id}
	 */
	@PatchMapping(value = "{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> updateCourse(@PathVariable("id") Integer id, @RequestBody(required = false) CoursesDto course) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = coursesService.update(course, id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * GET DICTIONARIES
	 * Try on POSTMAN: http://localhost:4000/api/courses/dictionaries
	 */
	@GetMapping(value = "/dictionaries", produces = "application/json")
	private BaseResponseDto<ServiceResponse<DictionariesDto>> getDictionaries() {
		return new ResponseFactory<DictionariesDto>().make(coursesService.getDictionaries());
	}
	
	/**
	 * GET BY TIPO
	 * Try on POSTMAN: http://localhost:4000/api/courses/getByTipo
	 */
	@GetMapping(value = "/getByTipo", produces = "application/json")
	private BaseResponseDto<PageableDto<List<CoursesDto>>> getCourseByType(
			@RequestParam(required = false) Integer tipo,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer resultsPerPage,
			@RequestParam(required = false) Order order
			) {
		BaseResponseDto<PageableDto<List<CoursesDto>>> result = new BaseResponseDto<>();
		
		page = page != null ? page : 0;
		resultsPerPage = resultsPerPage != null ? resultsPerPage : RESULTS_PER_PAGE;
		
		result.setResponse(coursesService.getCoursesByType(tipo, page, resultsPerPage, order));
		return result; 
	}
	
	/**
	 * Trash or restore a course by ID
	 * Try on POSTMAN: http://localhost:4000/api/courses/setTrashed/{id}
	 */
	@PatchMapping(value = "/setTrashed/{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> setTrashed(
			@PathVariable("id") Integer id, 
			@RequestBody(required = false) CoursesDto course,
			@RequestParam(required = false) boolean trashed) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = coursesService.setTrashed(course, id, trashed);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
	/**
	 * Trash or restore a course by ID
	 * Try on POSTMAN: http://localhost:4000/api/courses/setTrashed/{id}
	 */
	@PatchMapping(value = "/setValutazioni/{id}", produces = "application/json")
	private BaseResponseDto<EServiceResponse> setMarks(
			@PathVariable("id") Integer id, 
			@RequestBody(required = false) CoursesDto course) {
		BaseResponseDto<EServiceResponse> result = new BaseResponseDto<>();
		EServiceResponse serviceStatus = coursesService.setMarks(course, id);
		result.setSuccess(serviceStatus.value() >= 1);
		result.setError(serviceStatus.value() <= -1);
		result.setResponse(serviceStatus);
		return result;
	}
	
}
