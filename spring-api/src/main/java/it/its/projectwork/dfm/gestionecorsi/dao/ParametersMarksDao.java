package it.its.projectwork.dfm.gestionecorsi.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "criteri_valutazione")
@Data
public class ParametersMarksDao {

	@Id
	@Column(name = "id_criterio")
	private Integer idCriterio;
	
	@Column(name = "descrizione")
	private String descrizione;

	@Column(name = "tipo")
	private String tipo;
}