DROP DATABASE IF EXISTS gestione_corsi;
CREATE DATABASE IF NOT EXISTS gestione_corsi;
USE gestione_corsi;

create table criteri_valutazione
(
    id_criterio int auto_increment
        primary key,
    descrizione varchar(300) not null,
    tipo        varchar(50)  not null
);

create table docenti
(
    docente_id      int auto_increment
        primary key,
    titolo          varchar(100) null,
    ragione_sociale varchar(100) not null,
    indirizzo       varchar(100) not null,
    localita        varchar(100) not null,
    provincia       varchar(100) not null,
    nazione         varchar(100) not null,
    telefono        varchar(100) not null,
    fax             varchar(100) null,
    email           varchar(100) not null,
    note_generiche  longtext     null,
    partita_iva     varchar(100) not null,
    referente       varchar(100) not null,
    trashed         timestamp    null,
    constraint email
        unique (email),
    constraint partita_iva
        unique (partita_iva)
);

create index docenti_index
    on docenti (ragione_sociale, partita_iva, referente);

create table misure
(
    misura_id   int auto_increment
        primary key,
    descrizione varchar(50) not null,
    constraint descrizione
        unique (descrizione)
);

create table societa
(
    societa_id      int auto_increment
        primary key,
    ragione_sociale varchar(100) not null,
    indirizzo       varchar(100) not null,
    localita        varchar(100) not null,
    provincia       varchar(100) not null,
    nazione         varchar(100) not null,
    telefono        varchar(100) not null,
    fax             varchar(100) null,
    partita_iva     varchar(100) not null,
    constraint partita_iva
        unique (partita_iva)
);

create table dipendenti
(
    dipendente_id           int auto_increment
        primary key,
    matricola               varchar(100) not null,
    societa                 int          not null,
    cognome                 varchar(100) not null,
    nome                    varchar(100) not null,
    sesso                   char         not null,
    data_nascita            date         not null,
    luogo_nascita           varchar(100) not null,
    stato_civile            varchar(100) null,
    titolo_studio           varchar(100) null,
    conseguito_presso       varchar(100) null,
    anno_conseguimento      int(4)       null,
    tipo_dipendente         varchar(100) null,
    qualifica               varchar(100) null,
    livello                 varchar(100) null,
    data_assunzione         date         null,
    responsabile_risorsa    varchar(100) null,
    responsabile_area       varchar(100) null,
    data_fine_rapporto      date         null,
    data_scadenza_contratto date         null,
    constraint matricola
        unique (matricola),
    constraint dipendenti_ibfk_1
        foreign key (societa) references societa (societa_id)
);

create index dipendenti_index
    on dipendenti (matricola, societa, nome, cognome);

create index societa
    on dipendenti (societa);

create index societa_index
    on societa (ragione_sociale, partita_iva);

create table tipi
(
    tipo_id     int auto_increment
        primary key,
    descrizione varchar(50) not null,
    constraint descrizione
        unique (descrizione)
);

create table tipologie
(
    tipologia_id int auto_increment
        primary key,
    descrizione  varchar(50) not null,
    constraint descrizione
        unique (descrizione)
);

create table corsi
(
    corso_id        int auto_increment
        primary key,
    tipo            int           not null,
    tipologia       int           not null,
    descrizione     varchar(200)  null,
    edizione        varchar(100)  null,
    previsto        tinyint(1)    not null,
    erogato         tinyint(1)    not null,
    durata          int(4)        not null,
    misura          int           not null,
    note            varchar(300)  null,
    luogo           varchar(30)   not null,
    ente            varchar(30)   not null,
    societa         int           not null,
    data_erogazione date          null,
    data_chiusura   date          null,
    data_censimento date          null,
    interno         tinyint(1)    not null,
    trashed         timestamp     null,
    valutazioni     int default 0 not null,
    constraint corsi_ibfk_1
        foreign key (tipo) references tipi (tipo_id),
    constraint corsi_ibfk_2
        foreign key (tipologia) references tipologie (tipologia_id),
    constraint corsi_ibfk_3
        foreign key (misura) references misure (misura_id),
    constraint corsi_ibfk_4
        foreign key (societa) references societa (societa_id)
);

create index corsi_index
    on corsi (descrizione, luogo, societa, interno);

create index misura
    on corsi (misura);

create index societa
    on corsi (societa);

create index tipo
    on corsi (tipo);

create index tipologia
    on corsi (tipologia);

create table corsi_docenti
(
    corsi_docenti_id int auto_increment
        primary key,
    corso            int not null,
    docente          int null,
    interno          int null,
    constraint corsi_docenti_ibfk_1
        foreign key (corso) references corsi (corso_id),
    constraint corsi_docenti_ibfk_2
        foreign key (docente) references docenti (docente_id),
    constraint corsi_docenti_ibfk_3
        foreign key (interno) references dipendenti (dipendente_id)
);

create index corsi_docenti_index
    on corsi_docenti (corso, docente);

create index docente
    on corsi_docenti (docente);

create index interno
    on corsi_docenti (interno);

create table corsi_utenti
(
    corsi_utenti_id int auto_increment
        primary key,
    corso           int not null,
    dipendente      int not null,
    durata          int not null,
    constraint corsi_utenti_ibfk_1
        foreign key (corso) references corsi (corso_id),
    constraint corsi_utenti_ibfk_2
        foreign key (dipendente) references dipendenti (dipendente_id)
);

create index corsi_utenti_index
    on corsi_utenti (corso, dipendente);

create index dipendente
    on corsi_utenti (dipendente);

create table valutazione_utenti
(
    valutazioni_utenti_id int auto_increment
        primary key,
    corso                 int    not null,
    dipendente            int    not null,
    criterio              int    not null,
    valore                int(1) not null,
    constraint valutazione_utenti_ibfk_1
        foreign key (criterio) references criteri_valutazione (id_criterio),
    constraint valutazione_utenti_ibfk_2
        foreign key (corso) references corsi (corso_id),
    constraint valutazione_utenti_ibfk_3
        foreign key (dipendente) references dipendenti (dipendente_id)
);

create index criterio
    on valutazione_utenti (criterio);

create index dipendente
    on valutazione_utenti (dipendente);

create index valutazioni_utenti_index
    on valutazione_utenti (corso, dipendente, criterio);

create table valutazioni_corsi
(
    valutazioni_corsi_id int auto_increment
        primary key,
    corso                int  not null,
    criterio             int  not null,
    valore               json not null,
    constraint valutazioni_corsi_ibfk_1
        foreign key (criterio) references criteri_valutazione (id_criterio),
    constraint valutazioni_corsi_ibfk_2
        foreign key (corso) references corsi (corso_id)
);

create index criterio
    on valutazioni_corsi (criterio);

create index valutazioni_corsi_index
    on valutazioni_corsi (corso, criterio);

create table valutazioni_docenti
(
    valutazioni_docenti_id int auto_increment
        primary key,
    corso                  int  not null,
    docente                int  null,
    interno                int  null,
    criterio               int  not null,
    valore                 json not null,
    constraint valutazioni_docenti_ibfk_1
        foreign key (criterio) references criteri_valutazione (id_criterio),
    constraint valutazioni_docenti_ibfk_2
        foreign key (corso) references corsi (corso_id),
    constraint valutazioni_docenti_ibfk_3
        foreign key (interno) references dipendenti (dipendente_id),
    constraint valutazioni_docenti_ibfk_4
        foreign key (docente) references docenti (docente_id)
);

create index criterio
    on valutazioni_docenti (criterio);

create index docente
    on valutazioni_docenti (docente);

create index interno
    on valutazioni_docenti (interno);

create index valutazioni_docenti_index
    on valutazioni_docenti (corso, docente, criterio);


INSERT INTO tipi (tipo_id, descrizione) VALUES
(NULL,'Java'),
(NULL,'Internet'),
(NULL,'Client-Server'),
(NULL,'Programmazione Mainframe'),
(NULL,'RPG'),
(NULL,'Cobol'),
(NULL,'Programmazione di Sistemi EDP'),
(NULL,'Object-Oriented'),
(NULL,'Sistema di Gestione Qualità'),
(NULL,'Privacy'),
(NULL,'Analisi e Programmazione'),
(NULL,'Programmazione PC'),
(NULL,'Project Management'),
(NULL,'Programmazione'),
(NULL,'CICS'),
(NULL,'DB2'),
(NULL,'Linguaggio C'),
(NULL,'Data Base'),
(NULL,'Introduzione all''Informatica'),
(NULL,'Office'),
(NULL,'JCL'),
(NULL,'Marketing'),
(NULL,'Oracle'),
(NULL,'Formazione Sicurezza (626/94; 81/2008)'),
(NULL,'Inglese'),
(NULL,'Amministrazione/Finanza'),
(NULL,'Windows'),
(NULL,'Word'),
(NULL,'Excel'),
(NULL,'Easytrieve'),
(NULL,'Comunicazione'),
(NULL,'Access'),
(NULL,'Formazione per Formatori'),
(NULL,'Tecnica Bancaria'),
(NULL,'Lotus Notes'),
(NULL,'Visualage'),
(NULL,'Websphere'),
(NULL,'Reti/Elettronica'),
(NULL,'SAS'),
(NULL,'UML'),
(NULL,'ViaSoft'),
(NULL,'Visual Basic'),
(NULL,'Sistemi Informativi'),
(NULL,'Cartografia Vettoriale'),
(NULL,'Cartografia Raster'),
(NULL,'Gestione Elettronica Documenti-Modulistica'),
(NULL,'Territorio e Ambiente'),
(NULL,'Sistemi Informativi Territoriali'),
(NULL,'Editoria'),
(NULL,'VARIE'),
(NULL,'AS/400'),
(NULL,'Gestione Progetti'),
(NULL,'Organizzazione'),
(NULL,'Funzionale Normativo'),
(NULL,'Funzionale'),
(NULL,'Manageriale'),
(NULL,'Metodologia'),
(NULL,'Prodotti'),
(NULL,'Business Object'),
(NULL,'Sistemi di Business Intelligence'),
(NULL,'Formazione Apprendisti'),
(NULL,'Primavera'),
(NULL,'XML'),
(NULL,'Cattolica – Servizi in ambito Sinistri'),
(NULL,'Ambito Assicurativo'),
(NULL,'Gestione della Sicurezza delle Informazioni (SGSI)'),
(NULL,'PHP'),
(NULL,'CMS/E-commerce'),
(NULL,'Microsoft Azure'),
(NULL,'Eurotech IoT'),
(NULL,'Infrastrutture'),
(NULL,'Android'),
(NULL,'SQL'),
(NULL,'Risorse Umane'),
(NULL,'Middleware - Sistemi Distribuiti'),
(NULL,'Blockchain'),
(NULL,'Angular'),
(NULL,'Soft Skill');


INSERT INTO tipologie (tipologia_id, descrizione) VALUES
(NULL,'Ingresso'),
(NULL,'Aggiornamento'),
(NULL,'Seminario');


INSERT INTO misure (misura_id, descrizione) VALUES
(NULL,'Ore'),
(NULL,'Giorni');


INSERT INTO criteri_valutazione VALUES
	(NULL, 'Utilità del corso per la crescita personale','corso'),
	(NULL, 'Utilità del corso nell''ambito lavorativo attuale','corso'),
	(NULL, 'Scelta degli argomenti specifici','corso'),
	(NULL, 'Livello di approfondimento degli argomenti specifici','corso'),
	(NULL, 'Durata del corso','corso'),
	(NULL, 'Corretta suddivisione tra teoria e pratica','corso'),
	(NULL, 'Efficacia delle esercitazioni','corso'),
	(NULL, 'Qualità del materiale didattico','corso'),
	(NULL, 'Valutazione Responsabile','corso');
INSERT INTO criteri_valutazione VALUES
	(NULL,'Impegno','dipendente'),
	(NULL,'Logica','dipendente'),
	(NULL,'Apprendimento','dipendente'),
	(NULL,'Velocità','dipendente'),
	(NULL,'Ordine e Precisione','dipendente'),
	(NULL,'Valutazione Responsabile','dipendente');
INSERT INTO criteri_valutazione VALUES
	(NULL,'Competenza','docente'),
	(NULL,'Chiarezza nell''esposizione','docente'),
	(NULL,'Disponibilità','docente'),
	(NULL,'Valutazione Resonsabile','docente');