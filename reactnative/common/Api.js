export const Api = (method, url, success, error, final = () => {}) => {
    fetch(url, {
        method,
    })
    .then((resp) => resp.json())
    .then(success)
    .catch(error)
    .finally(final);
};
