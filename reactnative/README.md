# Corvallis - Gestione Corsi
## Mobile app

This app has been developed to be used by employees to check in real-time their attendance to courses

## Compile instructions
### General
1. Install NodeJS and NPM
2. Install React Native
3. Install Android Studio/XCode
4. Run `npm i` to install all dependencies
5. Edit host in environments/environment.js to match your Spring Boot server IP address/domain

### Android
1. Enable USB Debug
2. Install ADB through Android Studio
3. Plug your smartphone into your PC
4. To run this app on your Android, execute `react-native run-android`


## Know issues
### Windows
If you're developing on Windows, you might face some issues while trying to compile or to develop with React Native. Those are problems bundled with React Native and it does not depend on me.

You may switch to Linux to avoid those problems or follow my instructions.

#### lmps permissions
To solve this, open the "android" folder with Android Studio as an existing Gradle project, then Build > Clean Project and Build > Rebuild Project

#### :appDebug
To solve this, open the "android" folder with Android Studio as an existing Gradle project, then Build > Clean Project and Build > Rebuild Project
