import React from 'react';
import { Layout, Text, Button, Input } from '@ui-kitten/components';
import FullWidthImage from 'react-native-fullwidth-image';
import {SafeAreaView} from 'react-native-safe-area-context';
import {environment} from '../environments/environment';
import {Api} from '../common/Api';

class Login extends React.Component {

    state = {
        matricola: {
            value: '',
            status: 'basic',
            caption: null,
        },
        login: {
            loading: false,
        },
    };

    constructor(props) {
        super(props);

        this.login = this.login.bind(this);
    }

    login = () => {
        const matricola = {...this.state.matricola};

        if (matricola.value.trim() === '') {
            this.setState(prevState => ({
                ...prevState,
                matricola: {
                    ...prevState.matricola,
                    status: 'danger',
                    caption: 'Inserisci una matricola',
                },
            }));
        } else {
            this.setState(prevState => ({
                ...prevState,
                matricola: {
                    ...prevState.matricola,
                    status: 'basic',
                    caption: 'Caricamento...',
                },
                login: {
                    ...prevState.login,
                    loading: true,
                },
            }));

            Api('GET', environment.host + environment.endpoint.employees +  '/getByMatricola/' + matricola.value.trim(), (rJson) => {
                let status = 'danger';
                let caption = 'Matricola non valida';

                if (rJson.status === 200 && rJson.success && rJson.response.response === 'OK') {
                    const employee = rJson.response.data;

                    status = 'success';
                    caption = 'Bentornato ' + employee.cognome + ' ' + employee.nome;
                    this.props.navigation.replace('Courses', {employee});
                }

                this.setState(prevState => ({
                    ...prevState,
                    matricola: {
                        ...prevState.matricola,
                        status,
                        caption,
                    },
                }));
            }, (e) => {
                console.log(e);
                this.setState(prevState => ({
                    ...prevState,
                    matricola: {
                        ...prevState.matricola,
                        status: 'danger',
                        caption: 'Errore di rete',
                    },
                }));
            }, () => {
                this.setState(prevState => ({
                    ...prevState,
                    login: {
                        ...prevState.login,
                        loading: false,
                    },
                }));
            });
        }
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 30}}>
                    <FullWidthImage source={require('../static/images/logo.png')} width={1133} height={305} style={{marginVertical: 20}} />
                    <Text category="h1">Gestione Corsi</Text>
                    <Text style={{textAlign: 'center', marginBottom: 10}}>Inserisci la tua matricola per vedere i corsi passati, presenti e futuri</Text>
                    <Input placeholder="Matricola" caption={this.state.matricola.caption} status={this.state.matricola.status} style={{textAlign: 'center'}} value={this.state.matricola.value} onChangeText={value => this.setState(prevState => ({...prevState, matricola: {...prevState.matricola, value}}))} />
                    <Button onPress={this.login} style={{alignSelf: 'stretch', marginTop: 10}} disabled={this.state.login.loading}>Accedi</Button>
                </Layout>
            </SafeAreaView>
        );
    }

}

export default Login;
