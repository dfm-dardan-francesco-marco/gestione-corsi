import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import { Layout, Text, BottomNavigation, BottomNavigationTab } from '@ui-kitten/components';
import FullWidthImage from 'react-native-fullwidth-image';
import moment from 'moment';
import {environment} from '../environments/environment';
import {Api} from '../common/Api';

const stylesheet = StyleSheet.create({
    dashboard: {
        padding: 20,
        width: 200,
        minWidth: 150,
        borderRadius: 10,
        margin: 5,
    },
    dashboardText: {
        color: '#FFFFFF',
    },
    courseItem: {
        padding: 20,
        margin: 20,
        backgroundColor: '#efefef',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
});

class Employee extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={{padding: 20}}>
                <Layout>
                    <Layout style={{margin: 40}}>
                        <FullWidthImage source={require('../static/images/logo.png')} width={1133} height={305} />
                    </Layout>
                    <Text category="h4" style={{textAlign: 'center'}}>Bentornato</Text>
                    <Text category="h1" style={{textAlign: 'center', marginBottom: 10}}>{this.props.employee.cognome} {this.props.employee.nome}</Text>
                    <Text style={{textAlign: 'center'}}>Lavori in Corvallis dal {moment(this.props.employee.dataAssunzione).format('DD/MM/YYYY')}</Text>
                </Layout>
                <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text category="h4" style={{marginBottom: 20}}>I tuoi corsi</Text>
                    <Layout style={[stylesheet.dashboard, {backgroundColor: '#4C9FFB'}]}>
                        <Text style={[stylesheet.dashboardText]}>In corso</Text>
                        <Text category="h2" style={[stylesheet.dashboardText]}>{this.props.employee.ongoing}</Text>
                    </Layout>
                    <Layout style={[stylesheet.dashboard, {backgroundColor: '#4DBD74'}]}>
                        <Text style={[stylesheet.dashboardText]}>Futuri</Text>
                        <Text category="h2" style={[stylesheet.dashboardText]}>{this.props.employee.incoming}</Text>
                    </Layout>
                    <Layout style={[stylesheet.dashboard, {backgroundColor: '#F86C6B'}]}>
                        <Text style={[stylesheet.dashboardText]}>Passati</Text>
                        <Text category="h2" style={[stylesheet.dashboardText]}>{this.props.employee.finished}</Text>
                    </Layout>
                </Layout>
            </ScrollView>
        );
    }
}

class CoursesList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={{flex: 1}}>
                {this.props.list.length === 0 && (<Text style={{flex: 1, justifyContent: 'center', alignItems: 'center', padding: 40}}>Nessun corso disponibile</Text>)}
                {this.props.list.map((item, index) => (
                    <Layout key={index} style={[stylesheet.courseItem]}>
                        <Text category="h4">{this.props.dictionaries.tipologie.find(tipologia => tipologia.tipologiaId === item.tipologia).descrizione} - {this.props.dictionaries.tipi.find(tipo => tipo.tipoId === item.tipo).descrizione}</Text>
                        <Text><Text style={{fontWeight: 'bold'}}>Data erogazione:</Text> {moment(item.dataErogazione).format('DD/MM/YYYY')}</Text>
                        <Text><Text style={{fontWeight: 'bold'}}>Data chiusura:</Text> {moment(item.dataChiusura).format('DD/MM/YYYY')}</Text>
                        <Text><Text style={{fontWeight: 'bold'}}>Luogo:</Text> {item.luogo}</Text>
                        {item.descrizione !== null && (<Text style={{marginTop: 10}}>{item.descrizione}</Text>)}
                    </Layout>
                ))}
            </ScrollView>
        );
    }
}

class Courses extends React.Component {

    state = {
        courses: {
            past: [],
            ongoing: [],
            incoming: [],
        },
        dictionaries: null,
        tabIndex: 0,
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Api('GET', environment.host + environment.endpoint.courses +  '/dictionaries', (dJson) => {
            if (dJson.status === 200 && dJson.success && dJson.response.response === 'OK') {
                this.setState(prevState => ({
                    ...prevState,
                    dictionaries: dJson.response.data,
                }));

                Api('GET', environment.host + environment.endpoint.employees +  '/getCoursesByMatricola/' + this.props.route.params.employee.matricola, (cJson) => {
                    if (cJson.status === 200 && cJson.success && cJson.response.response === 'OK') {
                        this.setState(prevState => ({
                            ...prevState,
                            courses: {
                                past: cJson.response.data.finished,
                                ongoing: cJson.response.data.ongoing,
                                incoming: cJson.response.data.incoming,
                            },
                        }));
                    }
                }, (e) => {
                    console.log(e);
                });
            }
        }, (e) => {
            console.log(e);
        });
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <Layout style={{flexGrow: 1}}>
                    {this.state.tabIndex === 0 && (<Employee employee={this.props.route.params.employee} />)}
                    {this.state.tabIndex === 1 && (<CoursesList list={this.state.courses.ongoing} dictionaries={this.state.dictionaries} />)}
                    {this.state.tabIndex === 2 && (<CoursesList list={this.state.courses.incoming} dictionaries={this.state.dictionaries} />)}
                    {this.state.tabIndex === 3 && (<CoursesList list={this.state.courses.past} dictionaries={this.state.dictionaries} />)}
                </Layout>
                <BottomNavigation
                    selectedIndex={this.state.tabIndex}
                    onSelect={index => this.setState(prevState => ({...prevState, tabIndex: index}))}>
                    <BottomNavigationTab title={(this.props.route.params.employee.cognome + ' ' + this.props.route.params.employee.nome.substring(0, 1) + '.').toUpperCase()}/>
                    <BottomNavigationTab title="IN CORSO"/>
                    <BottomNavigationTab title="FUTURI"/>
                    <BottomNavigationTab title="PASSATI"/>
                </BottomNavigation>
            </SafeAreaView>
        );
    }

}

export default Courses;
