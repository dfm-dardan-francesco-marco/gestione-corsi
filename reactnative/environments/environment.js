export const environment = {
    production: false,
    host: 'http://192.168.1.100:4000/api/',
    endpoint: {
        'employees': 'employees/',
        'courses': 'courses/',
    },
};
