import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './views/Login';
import Courses from './views/Courses';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
      <ApplicationProvider {...eva} theme={eva.light}>
        <StatusBar backgroundColor="#004899" />
          <NavigationContainer>
              <Stack.Navigator>
                  <Stack.Screen
                      name="Home"
                      component={Login}
                      options={{title: 'Accedi', headerShown: false}}
                  />
                  <Stack.Screen
                      name="Courses"
                      component={Courses}
                      options={{title: 'Lista corsi', headerShown: false}} />
              </Stack.Navigator>
          </NavigationContainer>
      </ApplicationProvider>
  );
};

export default App;
